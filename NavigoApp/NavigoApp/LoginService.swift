//
//  LoginService.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/21/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire

class LoginService:NSObject, BaseService {
    
    let userdefaults = NSUserDefaults()
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: BaseEntity?) -> Void) {
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)api/users/login", parameters: params as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            print(response)
            
          
            
            if response.result.isSuccess{
                
                let statusResponse = Response()
                    statusResponse.parseJson(response.result.value?.valueForKey(Constant.response) as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                   // print(response.result.value)
                
                    
                    
                    
                    let loginUser = LoginUser()
                    loginUser.parseJson(response.result.value as! NSDictionary)
                    
                    AppSingleton.sharedInstance.loginUserObj = loginUser
                    
                    if let userSessionString = AppSingleton.sharedInstance.dicToString(response.result.value as! NSDictionary){
                    
                        
                        self.userdefaults.setValue(userSessionString, forKey: "UserSessionJson")
                        
                    }
                    
                    
//                    let user = AppConfig.sharedInstance.getAppUser()
//                    
//                        user.user = loginUser
//                        user.creditCard = loginUser.creditCardObj
                    
                    
                    callback(errorMessage: nil, message: statusResponse.developerMessage,serviceResponse:loginUser)
                    
                }else if statusResponse.status == 2{
                    
                    callback(errorMessage: nil, message: statusResponse.developerMessage,serviceResponse:nil)

                
                }else{
                
                    callback(errorMessage: statusResponse.developerMessage, message: nil,serviceResponse: nil)

                }
                
            }else {
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message:nil,serviceResponse: nil)
            }
        }
        //

    }
    
//    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
//       
//        return nil
//    }
    
}