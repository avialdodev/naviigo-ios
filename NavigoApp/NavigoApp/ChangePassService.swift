//
//  ChangePassService.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 17/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire

class ChangePassService:BaseService {
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: BaseEntity?) -> Void) {
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)/api/users/changePassword", parameters: params as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            if response.result.isSuccess{
                
                let statusResponse = Response()
                statusResponse.parseJson(response.result.value as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                    callback(errorMessage: nil, message: statusResponse.developerMessage,serviceResponse:nil)
                    
                    
                }else{
                    
                    callback(errorMessage: statusResponse.developerMessage, message: nil,serviceResponse:nil)
                    
                }
                
            }else{
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil,serviceResponse:nil)
                
            }
            
            
        }
        
    }

}