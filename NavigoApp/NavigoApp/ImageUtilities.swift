//
//  ImageUtilities.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/14/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import UIKit

class ImageUtilities {
    static let walk_1 = UIImage(named: "walk-1")
    static let walk_2 = UIImage(named: "walk-2")
    static let walk_3 = UIImage(named: "walk-3")
    static let walk_4 = UIImage(named: "walk-4")
    static let walk_5 = UIImage(named: "walk-5")
    
    static let ic_email     = UIImage(named: "ic-email")
    static let ic_gender    = UIImage(named: "ic-gender")
    static let ic_password  = UIImage(named: "ic-password")
    static let ic_user      = UIImage(named: "ic-user")
    static let ic_camera    = UIImage(named: "ic-camera")
    static let ic_phone     = UIImage(named: "ic-phone")
    static let fullName    = UIImage(named: "fullName")
    static let usDollor    = UIImage(named:"US Dollar-100")
    static let date    = UIImage(named: "ic_date")
    static let world    = UIImage(named:"ic_world")
    static let rightArrow    = UIImage(named: "ic_right")
    static let whiteEmail    = UIImage(named:"ic_email")
    static let blueLocation    = UIImage(named:"locationBlue")
    static let warning          = UIImage(named: "warning")
    static let textbox_tick     = UIImage(named: "textbox_tick")

}