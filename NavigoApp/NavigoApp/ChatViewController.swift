//
//  ChatViewController.swift
//  UniversityApp
//
//  Created by Saad Umar on 9/1/15.
//  Copyright (c) 2015 Saad Umar. All rights reserved.


//  Complete interface(wrapper) for Objective-C Library 'JSQMessagesViewController', anything related to chat must be implemented here and here alone.

import UIKit
import JSQMessagesViewController
import Firebase
import SDWebImage

class ChatViewController: JSQMessagesViewController
{
    var presentingAncestor:UIViewController?
    var demoData:DemoModelData!
    
    var dateFormatter:NSDateFormatter = NSDateFormatter()
    
    var FriendID : String!
    var friendName : String!
    var friendPictureName : String!
    let myID = "\(AppSingleton.sharedInstance.loginUserObj!.iD!)"
    var myPictureName:String!
    let myName = AppSingleton.sharedInstance.loginUserObj?.name
    
//    var myImage = UIImage(named: "user")
//    var friendImage = UIImage(named: "user")
    
    var myImageView = UIImageView()
    var friendimgView = UIImageView()
    
    var user = AppSingleton.sharedInstance.loginUserObj!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        let lBbItem = UIBarButtonItem(image: UIImage(named:"back-arrow1"), style: .Plain, target: self, action: #selector(ChatViewController.goToBack))
        self.navigationItem.leftBarButtonItem = lBbItem
        
        let myImageUrl = NSURL(string: "\(Constant.image_Link)\(self.user.imageURL).jpeg")
        
        myImageView.sd_setImageWithURL(myImageUrl, placeholderImage: UIImage(named: "user")) { (image, error, cache, url) in
            
            
            if image != nil{
                
                self.myImageView.image = image
                
            }
            
            let friendImageUrl = NSURL(string: "\(Constant.image_Link)\(self.friendPictureName).jpeg")

            self.friendimgView.sd_setImageWithURL(friendImageUrl, placeholderImage:  UIImage(named: "user"), options: SDWebImageOptions.CacheMemoryOnly ,completed:{(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) in
                
                self.collectionView.reloadData()
                
                if image != nil{
                    
                    self.friendimgView.image = image
                    
                }
                
            })
            
        }
        

        
   

        
        self.navigationController?.navigationBar.barTintColor = ColorUtilities.buttonBackGroungBlueColor
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        let rightButton = UIBarButtonItem(image: UIImage(named:"add_transaction"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ChatViewController.toPaymentRequest))
        
        self.navigationItem.rightBarButtonItem = rightButton
        
        setupViews()
        setupChat()
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            self.listeningToConvos()
            
            
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
            }
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
     //   self.inputToolbar.contentView.leftBarButtonItem.hidden = true

        self.inputToolbar.contentView.leftBarButtonItem.enabled = false
        self.tabBarController?.tabBar.hidden = true
    }
    
    func goToBack(){
    
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = false

    }
    
    func toPaymentRequest(){
    
        self.performSegueWithIdentifier("toPaymentRequest", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let vc = segue.destinationViewController as! PaymentRequestVC
            vc.travelerName = self.friendName!
            vc.travellerId = Int(self.FriendID!)!
            vc.travellerImage = self.friendPictureName
      //  vc.isAcceptOrReject = false
    }
    
    //MARK:-
    //MARK: Overriding super
    //TODO:Goto jsqmessageviewcontroller and find todo 'redo this method'
    
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        
        
        //Play sound
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        
        self.finishSendingMessageAnimated(true)
        
        ////////Add object to firebase/////////
        
        let firebase = FIRDatabase.database().reference() // Firebase(url: Constants().Firebase_BASE_URL)
        
        //Creating convo
        let conversation = firebase.child("conversations").child("\(myID)-\(FriendID)")
        
        conversation.child("meta-data").child("username").setValue(friendName)
        conversation.child("meta-data").child("picture").setValue(friendPictureName)
        conversation.child("meta-data").child("last-message").setValue(text)
        conversation.child("meta-data").child("senderID").setValue(FriendID)
        conversation.child("meta-data").child("timeStamp").setValue(FIRServerValue.timestamp())
      //  FirebaseServerValue.timestamp()
       
    
        
        conversation.child("messages").childByAutoId().setValue(["senderID":"\(myID)","text":text,"timeStamp":FIRServerValue.timestamp(), "read":true])
        
        
        //inverse of the above
        
        let InverseConversation = firebase.child("conversations").child("\(FriendID)-\(myID)")
        
        InverseConversation.child("meta-data").child("username").setValue(myName)
        InverseConversation.child("meta-data").child("picture").setValue(myPictureName)
        InverseConversation.child("meta-data").child("last-message").setValue(text)
        InverseConversation.child("meta-data").child("senderID").setValue(myID)
        
        InverseConversation.child("meta-data").child("timeStamp").setValue(FIRServerValue.timestamp())
        
        InverseConversation.child("messages").childByAutoId().setValue(["senderID":"\(myID)","text":text,"timeStamp":FIRServerValue.timestamp(),"read":true])
    
        
        
        // Creating a thread if not exists for both the users
        
        //print("my friends id is " , FriendID)
        firebase.child("threads").child(myID).child(FriendID).setValue(FriendID)
        
        // inversing for friends
        
        firebase.child("threads").child(FriendID).child(myID).setValue(myID)
        
        
        //send notification service
        
        
        var unwrappedName = myName!
        
        let params:[String:AnyObject] = [Constant.userID:FriendID,
                                         Constant.message:"\(unwrappedName): \(text)"]
        
        
        
        let sendNotification = SendNotificationService()
        sendNotification.execute(params)
        
    }
    
    
    
    //MARK:-
    //MARK: Views
    func setupViews()
    {
        
        self.title = friendName
        self.demoData = DemoModelData()
        
        //Change this for show Earlier Messages Header
        self.showLoadEarlierMessagesHeader = false
        
        //Springy bubbles
        self.collectionView!.collectionViewLayout.springinessEnabled=false
        
    }
    
    //MARK:-
    //MARK: Chat Model
    func setupChat()
    {
        self.senderId = self.senderIdForDataSource()
        self.senderDisplayName = self.senderDisplayNameForDataSource()
    }
    
    func listeningToConvos() {
        let firebase = FIRDatabase.database().reference()
        
        // Listening to convos //
        
    
        let conversation = firebase.child("conversations")
       // FIRDatabase.database().reference().child("\(myID)-\(FriendID)").child("messages").observeEventType(.ChildAdded, withBlock: { snapshot in
        conversation.child("\(myID)-\(FriendID)").child("messages").queryOrderedByKey().observeEventType(.ChildAdded, withBlock: { snapshot in
            
            let myMessage : String = snapshot.value!.objectForKey("text") as! String
            let mySenderId : String = snapshot.value!.objectForKey("senderID") as! String
            
            let time_stamp = snapshot.value!.objectForKey("timeStamp")! as! NSNumber
            let myDate = NSDate(timeIntervalSince1970: time_stamp.doubleValue/1000)
            
        
            var message : JSQMessage!
            
            if self.myID == "\(mySenderId)"
            {
                message = JSQMessage(senderId: "\(mySenderId)", senderDisplayName: SenderName, date: myDate, text: myMessage)

            }
            else
            {
                message = JSQMessage(senderId: "\(mySenderId)", senderDisplayName: ReceiverName, date: myDate, text: myMessage)
            }
            
            self.demoData.messages.addObject(message)
            self.finishSendingMessageAnimated(true)
            
            }, withCancelBlock: { error in
                print(error.description)
        })
        
    }
    
    //MARK:-
    //MARK: Protocols
    func senderDisplayNameForDataSource() -> String! {
        return "Jess"
    }
    func senderIdForDataSource() -> String! {
        return "\(myID)"
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
    
        
        return nil
    }
    
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        return self.demoData.messages.objectAtIndex(indexPath.item) as! JSQMessageData
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message:JSQMessage=self.demoData.messages.objectAtIndex(indexPath.item) as! JSQMessage
        
        if(message.senderId==self.senderIdForDataSource()){
            return self.demoData.outgoingBubbleImageData
        }
        
        
        
        return self.demoData.incomingBubbleImageData
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        /**
        *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
        *  The other label text delegate methods should follow a similar pattern.
        *
        *  Show a timestamp for every 3rd message
        */
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let message:JSQMessage=self.demoData.messages.objectAtIndex(indexPath.item) as! JSQMessage
         print(message.date)
      

        if firstMessageOfTheDay(indexPath){
        
            let message:JSQMessage=self.demoData.messages.objectAtIndex(indexPath.item) as! JSQMessage
//            
//            JSQMessagesTimestampFormatter.attributedTimestampForDate(message.date)
            print(NSAttributedString(string: dateFormatter.stringFromDate(message.date)))
            return  NSAttributedString(string: dateFormatter.stringFromDate(message.date))
            
        }else{
        
            return nil
        }
        

    }
    
    func firstMessageOfTheDay(indexOfMessage: NSIndexPath) -> Bool {
        
        let message:JSQMessage = self.demoData.messages.objectAtIndex(indexOfMessage.item) as! JSQMessage
        let messageDate = message.date
        
        var previouseMessageDate:NSDate!
        
        
        if indexOfMessage.item > 0 {
        
            previouseMessageDate = (self.demoData.messages.objectAtIndex(indexOfMessage.item - 1) as? JSQMessage)?.date
        } else {
            
            return true // because there is no previous message so we need to show the date
        }
        
        let calendar = NSCalendar.currentCalendar()
        let day = calendar.components([.Day], fromDate: messageDate)
       
        let previouseMessageDay = calendar.components([.Day], fromDate: previouseMessageDate)
            if day == previouseMessageDay {
                return false
            } else {
            return true
        }
    
      
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        
        let message:JSQMessage=self.demoData.messages.objectAtIndex(indexPath.item) as! JSQMessage
        if(message.senderId==self.senderIdForDataSource()){
            return nil
        }
        
        if(indexPath.item-1>0){
            let previousMessage:JSQMessage=self.demoData.messages.objectAtIndex(indexPath.item-1) as! JSQMessage
            if(previousMessage.senderId==message.senderId){
                return nil
            }
        }
        
        return NSAttributedString(string:message.senderDisplayName)
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        return nil
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.demoData.messages.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        /**
         *  Configure almost *anything* on the cell
         *
         *  Text colors, label text, label colors, etc.
         *
         *
         *  DO NOT set `cell.textView.font` !
         *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
         *
         *
         *  DO NOT manipulate cell layout information!
         *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
         */
        
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! JSQMessagesCollectionViewCell
        
     print(self.friendPictureName)
     print(self.myPictureName)
        
        let message:JSQMessage = self.demoData.messages.objectAtIndex(indexPath.item) as! JSQMessage
        
        if(!message.isMediaMessage){
            
            cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.bounds.size.height/2
            cell.avatarImageView.clipsToBounds = true
            cell.avatarImageView.contentMode = .ScaleAspectFill
            
            if message.senderId == self.senderIdForDataSource() {
                cell.textView!.textColor = UIColor.blackColor()
                cell.avatarImageView.image = self.myImageView.image
                
            } else {
                cell.textView!.textColor = UIColor.whiteColor()
                cell.avatarImageView.image = self.friendimgView.image
            }
            
            //print(cell.textView?.linkTextAttributes)
            //let attributes : [NSObject:AnyObject] = [NSForegroundColorAttributeName:cell.textView!.textColor, NSUnderlineStyleAttributeName: 1] as [NSObject:AnyObject]
            //self.linkTextAttributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor],
            //NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
            //cell.textView!.linkTextAttributes = attributes
        }
        return cell
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat{
        
      
            return kJSQMessagesCollectionViewCellLabelHeightDefault
    
        
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        
        let currentMessage:JSQMessage = self.demoData.messages.objectAtIndex(indexPath.item) as! JSQMessage
        
        // Sent by me, skip
        if currentMessage.senderId == self.senderIdForDataSource() {
            return CGFloat(0.0);
        }
        
        if(indexPath.item-1>0){
            let previousMessage:JSQMessage = self.demoData.messages.objectAtIndex(indexPath.item-1) as! JSQMessage
            if(previousMessage.senderId==currentMessage.senderId){
                return CGFloat(0.0)
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return CGFloat(0.0)
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
        print("Load Earlier Messages")
    }
    
    
    
}
