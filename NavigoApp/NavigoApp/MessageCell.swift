//
//  NotificationCell.swift
//  UniversityApp
//
//  Created by Saad Umar on 8/27/15.
//  Copyright (c) 2015 Saad Umar. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    //Outlets
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!

    @IBOutlet weak var pictureIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setContents(forIndex forIndex:Int, profileImage:UIImage?, title:String, description:String,dateComponents:NSDateComponents) {
        //profileImg.image = profileImage
        //titleLbl.text = title
        //descriptionLbl.text = description
        
        //TODO: Break the components properly
        //dateLbl.text = "\(dateComponents.day)"
    }
    
}
