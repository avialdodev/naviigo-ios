//
//  CustomPageViewController.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/14/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class CustomPageViewController:BWWalkthroughPageViewController  {

    @IBOutlet weak var backgroudImageView: UIImageView!
    
    @IBOutlet weak var textLable: UILabel!
    var backImage:UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.backgroudImageView.image =  self.backImage
        self.view.bringSubviewToFront(textLable)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
