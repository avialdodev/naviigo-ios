//
//  UpdatePushIdService.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 03/06/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire

class UpdatePushIdService{
    
    func execute(params: NSDictionary) {
        
        print(params)
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)api/users/updatePushToken", parameters: params as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            print("response\(response)")
            
            if response.result.isSuccess {
                
             
                
            }
        }
        
        
    }
    
}
