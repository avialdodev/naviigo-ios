//
//  Guide.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 25/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation

class Guide: BaseEntity {

    var guideId = 0
    var iD:Int!
    var phone:String!
    var email:String!
    var name:String!
    var imageURL:String!
    var bio:String!
    var expertise:String!
    var city:String!
    var country:String!
    var lowestAmount:String!
    var rating:Int!
    
    
    
    override init(){
        
    }
    override func parseJson(response: NSDictionary) {
        
        self.iD                 =        response.valueForKey("ID") as! Int
        self.guideId            =        response.valueForKey("guideId") as? Int ?? 0
        self.phone              =        response.valueForKey("phone") as? String
        self.email              =        response.valueForKey("email") as? String
        self.name               =        response.valueForKey("name") as? String
        self.imageURL           =        response.valueForKey("imageURL") as? String
        self.bio                =        response.valueForKey("bio") as? String
        self.expertise          =        response.valueForKey("expertise") as? String
        self.city               =        response.valueForKey("city") as? String
        self.country            =        response.valueForKey("country") as? String
        self.lowestAmount       =        response.valueForKey("lowestAmount") as? String
        self.rating             =        response.valueForKey("rating") as? Int ?? 0
        
        
        
        
    }
}