//
//  EmailSendPopUpVC.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 17/06/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class EmailSendPopUpVC: UIViewController {

    @IBOutlet weak var popUpText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func okAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(false) { 
            
              AppSingleton.sharedInstance.okBtnClickOnEmailPopUp?.okBtnClick()
            
        }
      
       
        
    }
 

}
