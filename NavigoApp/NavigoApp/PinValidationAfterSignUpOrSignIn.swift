//
//  PinValidationAfterSignUpOrSignIn.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/17/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class PinValidationAfterSignUpOrSignIn: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var confirmButton: CustomButton!
    @IBOutlet weak var pinCodeView: TextFieldView!
    
    let saveUserDefaults = NSUserDefaults()

    var email:String?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setInitialViewRepresentation()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setInitialViewRepresentation(){
    
        self.pinCodeView.setPlaceHolderOFField("Enter Pin Code")
        self.pinCodeView.setTextFieldIconImage(ImageUtilities.ic_gender!)
        self.pinCodeView.textField.delegate = self
        
            self.confirmButton.setBacColor(ColorUtilities.buttonBackGroungBlueColor)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.pinCodeView.textField.resignFirstResponder()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func confirmAction(sender: AnyObject) {
        self.pinCodeView.textField.resignFirstResponder()
        
        if self.pinCodeView.textField.text != ""{
        
            Utilities.sharedInstance.disableScreen(self.view)
            
            var refreshedToken =  self.saveUserDefaults.objectForKey("FIRInstanceIDToken")
            if refreshedToken == nil {
                
                refreshedToken = "pushID"
            }
            
            let params:[String:AnyObject] = [Constant.token:self.pinCodeView.textField.text!,
                Constant.email:self.email!,
                Constant.pushID:"\(refreshedToken!)"
                ]

            let pinValidationService = PinValidationService()
                pinValidationService.execute(params, callback: { (errorMessage, message, serviceResponse) -> Void in
                    
                      Utilities.sharedInstance.enableScreeen()
                    
                    if errorMessage == nil{
                        
                        let getNotificationS = GetNotificationCount()
 
                        
                        print(serviceResponse)
                        let stb = UIStoryboard(name: "AppFlow", bundle: nil)
                        let vc = stb.instantiateViewControllerWithIdentifier("TabsViewController") as! TabsViewController
                        self.presentViewController(vc, animated: false, completion: nil)
                        
                    }else{
                        
                        
                        Utilities.makeToastOfString("\(errorMessage!)", onView: self.view)
                    }
                    
                })
            
       
        
        }else{
        
            self.pinCodeView.setTextFieldActionImage(ImageUtilities.warning)
        }
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            
            return true
        }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
            return newLength <= 10
            
       
        
    }
   
    @IBAction func backAction(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
