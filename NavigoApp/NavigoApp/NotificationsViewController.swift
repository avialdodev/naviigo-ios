//
//  NotificationsViewController.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 09/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var backImageView: UIImageView!
    var notifications:[Notification] = []

    @IBOutlet weak var notificationTable: UITableView!
    
    var user:LoginUser!
    var toDetailTrip:Trip? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user = AppSingleton.sharedInstance.loginUserObj

        let lBbItem = UIBarButtonItem(image: UIImage(named:"back-arrow1"), style: .Plain, target: self, action: #selector(NotificationsViewController.goToBack))
        self.navigationItem.leftBarButtonItem = lBbItem
        
        self.navigationController?.navigationBar.barTintColor = ColorUtilities.buttonBackGroungBlueColor
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
      //  self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        
        self.title = "Notifications"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = true
        AppSingleton.sharedInstance.notificationCount = 0
        self.getNotifications()
    }
    
  
    private func getNotifications(){
    
        Utilities.sharedInstance.disableScreen(self.view)
        
        let params =  ["userID":self.user.iD]
        let notificationS = GetNotificationsService()
            notificationS.execute(params) { (errorMessage, message, serviceResponse) in
                
                
                Utilities.sharedInstance.enableScreeen()
                
                if errorMessage == nil{
                    
                    self.notifications = serviceResponse!
                    self.notificationTable.reloadData()
                    
                    if self.notifications.count == 0{
                        
                    self.backImageView.image = UIImage(named:"notification_empty")
                        self.notificationTable.hidden = true
                       
                    
                    }else{
                        
                        self.backImageView.image = nil
                        
                        self.notificationTable.hidden = false
                    
                    }
                    
                
                }else{
                    
                    Utilities.makeToastOfString(errorMessage!, onView: self.view)
                
                }
        }
    
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if self.notifications[indexPath.row].type == 1{
            
            self.callGetTripDetailS(userId:self.user.iD, tripId: self.notifications[indexPath.row].tripId)

        
        }
        
        //self.callGetTripDetailS(userId: 16, tripId: 14)
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("NotificationsTableViewCell") as! NotificationsTableViewCell
            cell.title.text = self.notifications[indexPath.row].title!
            cell.notificationDescription.text = self.notifications[indexPath.row].description
            cell.dateTime.text = self.notifications[indexPath.row].createdAt
        
        
        if self.notifications[indexPath.row].isRead == 0{
        
            cell.backgroundColor = UIColor.whiteColor()
        }else{
        
            cell.backgroundColor =  ColorUtilities.lightGray
        }
        
        
        print("N-userId ===== \(self.notifications[indexPath.row].userId)&&N-Id ===== \(self.notifications[indexPath.row].tripId)")
        
        return cell
    }

  
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 95
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.notifications.count
    }
    
    private func callGetTripDetailS(userId userId:Int, tripId:String){
    
        let params:[String:AnyObject] = ["userID":userId,
                                         "tripID":tripId]
        
        
        Utilities.sharedInstance.disableScreen(self.view)
        
        let getTripDS = GetTripDetainServic()
            getTripDS.execute(params) { (errorMessage, message, trip) in
                
                
                Utilities.sharedInstance.enableScreeen()
                if errorMessage == nil{
                    
                    self.toDetailTrip = trip!
                    
                    let story = UIStoryboard(name: "AppFlow", bundle: nil)
                    let vc    = story.instantiateViewControllerWithIdentifier("PaymentRequestVC") as! PaymentRequestVC
                    
                        vc.trip = self.toDetailTrip!
                    
                    if let guide = self.user.asGuide{
                    
                        if guide.iD == self.toDetailTrip!.guideId{
                        
                        
                            vc.isGuide = true
                            
                        }else{
                        
                            vc.isGuide = false
                            
                        }
                    
                    }else{
                        
                        vc.isGuide = false
                        
                    }
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                
                }else{
                
                    Utilities.makeToastOfString(errorMessage!, onView: self.view)
                
                }
        }
        
    }
    
    private func goToPayentRequestS(){
    

        
    }
    
    
    func goToBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
