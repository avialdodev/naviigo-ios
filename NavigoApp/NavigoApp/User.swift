//
//  User.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/18/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation

class User:BaseEntity{
    
    var iD:Int!
    var phone:String!
    var email:String!
    var name:String!
    var imageURL:String!
    var emailVerified:Int!
    var timeStamp:String!
    var hasCreditCard:Int!
    var isGuide:Int!

    var asGuide:UserGuide? = nil
    
    override init(){
    
    }
    

      override func parseJson(response:NSDictionary) {
        
        self.iD               =        response.valueForKey("ID") as! Int
        self.phone            =        response.valueForKey("phone") as? String
        self.emailVerified    =        response.valueForKey("emailVerified") as? Int
        self.hasCreditCard    =        response.valueForKey("hasCreditCard") as? Int
        self.timeStamp        =        response.valueForKey("timestamp") as? String
        self.email            =        response.valueForKey("email") as? String
        self.imageURL         =        response.valueForKey("imageURL") as? String
        self.name             =        response.valueForKey("name") as? String

        self.isGuide             =        response.valueForKey("isGuide") as! Int
        
        if isGuide == 1{
        
            self.asGuide = UserGuide()
            self.asGuide!.parseJson(response.valueForKey("guideObj") as? NSDictionary)
        
        }
        
        
    }

    
    
    

}