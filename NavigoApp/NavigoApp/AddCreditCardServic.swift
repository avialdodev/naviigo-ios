//
//  AddCreditCardServic.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 18/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire

class AddCreditCardServic:BaseService {

    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: BaseEntity?) -> Void) {
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)api/creditCard/add", parameters: params as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            print(response)
            if response.result.isSuccess{
                
                let statusResponse = Response()
                statusResponse.parseJson(response.result.value?.valueForKey(Constant.response) as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                    let creditCardObj = CreditCard()
                        creditCardObj.parseJson(response.result.value as? NSDictionary)
                    
                 //   AppSingleton.sharedInstance.loginUserObj?.creditCardObj = creditCardObj
                     callback(errorMessage: nil, message:statusResponse.developerMessage,serviceResponse: creditCardObj)
                
                }else{
                
                    callback(errorMessage: statusResponse.developerMessage, message:nil,serviceResponse: nil)
                    
                }
                
            
            }else{
                
                 callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message:nil,serviceResponse: nil)
            
            }
            
        }
    }

}