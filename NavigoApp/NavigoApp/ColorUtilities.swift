//
//  ColorUtilities.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/14/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import UIKit

class ColorUtilities {
    
    static let placeHolderColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 0.5)
    
    static let borderColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 0.4)
    static let buttonBackGroungBlueColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
    static let lightBlueColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 0.6)

     static let redColor = UIColor(red: 255/255, green: 120/255, blue: 141/255, alpha: 1)
     static let lightGray = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 0.2)
    
    
   
    static let status_Cancel_Color = UIColor.darkGrayColor()
    static let status_Pending_Color = UIColor(red: 255/255, green: 207/255, blue: 53/255, alpha: 1)
    static let status_Accept_Color  = UIColor(red: 113/255, green: 189/255, blue: 159/255, alpha: 1)
    static let status_Complete_Color  = UIColor.darkGrayColor()
    
    static let taxtFBackColor = UIColor(red: 249/255, green: 251/255, blue: 252/255, alpha: 1.0)

}