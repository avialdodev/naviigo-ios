//
//  GetTripRequestService.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 14/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire

class GetTripRequestService {
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: [Trip]?) -> Void) {
        
        print(params)
        
        Alamofire.request(.GET, params["serviceLink"] as! String, parameters: nil, encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
      
            print(response)
            if response.result.isSuccess{
                
              print("response\(response.result.value)")
                let statusResponse = Response()
                statusResponse.parseJson(response.result.value?.valueForKey(Constant.response) as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                    var trips:[Trip] = []
                    
                    let responseTrips = response.result.value!.valueForKey("tripList") as? NSArray
                    
                    for responseTrip in responseTrips!{
                        
                        let tripModel = Trip()
                        tripModel.parseJson((responseTrip as! NSDictionary))
                        
                        trips.append(tripModel)
                        
                    }
                    
                    print(trips)
                    
                    callback(errorMessage: nil, message: statusResponse.developerMessage, serviceResponse: trips)
                    
                }else{
                    
                    callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
                    
                }
                
                
            }else{
                
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
            }
            
        }
        
    }
}

