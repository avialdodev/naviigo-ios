//
//  getFeedbackService.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 09/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire


class GetFeedbackService {
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: [Review]?) -> Void) {
        
        Alamofire.request(.GET, "\(Constant.SERVICE_MAIN_LINK)api/guide/getReviews?guideID=\(params["guideID"]!)", parameters: nil, encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            print(response.result.value)
            if response.result.isSuccess{
            
                var responseReviews:[Review] = []
                
                if let reviewsDicValues = response.result.value as? NSDictionary{
                    
                    let reviews = reviewsDicValues.valueForKey("reviewList") as! NSArray
                    
                    for reviewDic in reviews{
                    
                        let reviewTemp = Review()
                            reviewTemp.parseJson((reviewDic as! NSDictionary))
                        
                           responseReviews.append(reviewTemp)
                        
                    }
                    
                    callback(errorMessage: nil, message: nil, serviceResponse: responseReviews)
                    
                }else{
                
                
                    callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
                }
                
            }else{
            
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)

            
            }
            
            
        }
        
    }
}