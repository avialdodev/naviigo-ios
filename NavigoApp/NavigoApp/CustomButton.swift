//
//  CustomButton.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/11/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class CustomButton: UIButton {


    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
        self.layer.cornerRadius = 5
        self.clipsToBounds      = true
    //    self.backgroundColor    = ColorUtilities.buttonBackGroungBlueColor

    }
    
    func setBacColor(color:UIColor){
          self.backgroundColor = color
    }
    
        func makeViewBorder(){
    
            self.layer.borderColor = ColorUtilities.borderColor.CGColor
            self.layer.borderWidth = 1
         
    
        }

    
}

