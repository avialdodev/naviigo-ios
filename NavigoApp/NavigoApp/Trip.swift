//
//  Trip.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 14/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import SDWebImage


class Trip:BaseEntity {

    
    var id:Int!
    var travlerId:Int!
    var guideId:Int!
    var transactionId:Int!
    var amount:String!
    var guideShare:String!
    var startDate:String!
    var endDate:String!
    var cancelledBy:Int!
    var createdAt:String!
    var status:Int!
    var imageName:String!
    var name:String!
    var image:UIImage? = nil
    var feedback:String!
    var rating:Int!
    var description:String!
    
    
    
    override func parseJson(dic: NSDictionary) {
        
        self.id             = dic.valueForKey("id") as! Int
        self.travlerId      = dic.valueForKey("travlerID") as! Int
        self.guideId        = dic.valueForKey("guideID") as! Int
        self.transactionId  = dic.valueForKey("transactionID") as! Int
        self.amount         = (dic.valueForKey("amount") as? String) ?? ""
        self.guideShare     = (dic.valueForKey("guideShare") as? String) ?? ""
        self.startDate      = (dic.valueForKey("startDate") as? String) ?? ""
        self.endDate        = (dic.valueForKey("endDate") as? String) ?? ""
        self.cancelledBy    = dic.valueForKey("cancelledBy") as? Int ?? 0
        self.createdAt      = (dic.valueForKey("createdAt") as? String) ?? ""
        self.imageName      = (dic.valueForKey("image") as? String) ?? ""
        self.name           = (dic.valueForKey("name") as? String) ?? ""
        self.status         = (dic.valueForKey("status") as? Int) ?? 0
        self.feedback       = (dic.valueForKey("feedback") as? String) ?? ""
        self.rating         = (dic.valueForKey("rating") as? Int) ?? 0
        self.description    = (dic.valueForKey("description") as? String) ?? ""
        
        
        
    }
    
    
}