//
//  MainViewController.swift
//  TB_Walkthrough
//
//  Created by Yari D'areglia on 12/03/16.
//  Copyright © 2016 Bitwaker. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, BWWalkthroughViewControllerDelegate {

    @IBOutlet weak var backImageV: UIImageView!
    var needWalkthrough:Bool = true
    var walkthrough:BWWalkthroughViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if needWalkthrough {

        let stb = UIStoryboard(name: "Walkthrough", bundle: nil)
        walkthrough = stb.instantiateViewControllerWithIdentifier("container") as! BWWalkthroughViewController
            
        let page_one = stb.instantiateViewControllerWithIdentifier("page") as! CustomPageViewController
            page_one.backImage = ImageUtilities.walk_1
          
            

        
            
        let page_three = stb.instantiateViewControllerWithIdentifier("page") as! CustomPageViewController
            page_three.backImage = ImageUtilities.walk_3
    
            
        let page_four = stb.instantiateViewControllerWithIdentifier("page") as! CustomPageViewController
            page_four.backImage = ImageUtilities.walk_4
            
        let page_five = stb.instantiateViewControllerWithIdentifier("page") as! CustomPageViewController
            page_five.backImage = ImageUtilities.walk_5
          
        
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.addViewController(page_one)
        walkthrough.addViewController(page_three)
        walkthrough.addViewController(page_four)
        walkthrough.addViewController(page_five)
        self.presentViewController(walkthrough, animated: false) {
            ()->() in
            page_one.textLable.text = "Search For Your Destination"
            page_three.textLable.text = "Choose Your Guide"
            page_four.textLable.text = "Plan Any Itinerary"
            page_five.textLable.text = "Enjoy New Experiences"

            self.needWalkthrough = false
        }
            
        }else{
//        
            let stb = UIStoryboard(name: "Main", bundle: nil)
            let vc = stb.instantiateViewControllerWithIdentifier("register") as! SignUpViewController
            presentViewController(vc, animated: false, completion: nil)
        }
 
    }

    @IBAction func presentWalkthrough(){
        
    
    }
}


extension MainViewController{
    
    func walkthroughCloseButtonPressed() {
        
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    func walkthroughPageDidChange(pageNumber: Int) {
        if (self.walkthrough.numberOfPages - 1) == pageNumber{
           // self.walkthrough.closeButton?.hidden = false
            self.backImageV.image = UIImage(named: "signup-screen")
        }else{
            self.backImageV.image = UIImage(named: "walk-1")
            //self.walkthrough.closeButton?.hidden = true
        }
    }

    
}