//
//  SignViewController.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/11/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class SignViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var passwordFieldView: TextFieldView!
    @IBOutlet weak var emailFeildView: TextFieldView!
    
    @IBOutlet weak var signInButton: CustomButton!
    
    
    let saveUserDefaults = NSUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()


        
        self.initialValueSet()
        self.setInitialViewRepresentation()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {

        self.resignResponders()
        self.view.viewMoveToDown()
    }
    
    private func resignResponders(){
        self.emailFeildView.textField.resignFirstResponder()
        self.passwordFieldView.textField.resignFirstResponder()
        
        self.view.viewMoveToDown()

    }
    private func setInitialViewRepresentation(){
    
        self.emailFeildView.setTextFieldIconImage(ImageUtilities.ic_email!)
        self.emailFeildView.setPlaceHolderOFField("Email")
        self.emailFeildView.textField.keyboardType = UIKeyboardType.EmailAddress
        self.emailFeildView.setTextFieldActionImage(nil)
        self.emailFeildView.textField.delegate = self
        
        self.passwordFieldView.setTextFieldIconImage(ImageUtilities.ic_password!)
        self.passwordFieldView.textField.secureTextEntry = true
        self.passwordFieldView.setPlaceHolderOFField("Password")
        self.passwordFieldView.setTextFieldActionImage(nil)
        self.passwordFieldView.textField.delegate = self
        
   
        self.signInButton.setBacColor(ColorUtilities.buttonBackGroungBlueColor)
        
        self.logoImg.layer.cornerRadius = self.logoImg.bounds.size.height/2
    
    }

    private func initialValueSet(){
    
        self.emailFeildView.textField.delegate      = self
        self.passwordFieldView.textField.delegate   = self
    
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        self.view.viewMoveToUp()
    }
    
    private func setImageOnTextFieldAction(image:UIImage){
        
        self.passwordFieldView.setTextFieldActionImage(image)
        self.emailFeildView.setTextFieldActionImage(image)
    }
    @IBAction func forgetPassAction(sender: AnyObject) {
        
        self.resignResponders()
        
          self.performSegueWithIdentifier("toForgetPass", sender:self )
      
//        if self.emailFeildView.textField.isFirstResponder() || self.passwordFieldView.textField.isFirstResponder(){
//        
//            self.performSelector("toForget", withObject: nil, afterDelay: 2)
//
//        }else{
//        
//            self.performSegueWithIdentifier("toForgetPass", sender:self )
//        }
    }
    
    func toForget(){
        
        
    }
    
    @IBAction func createAction(sender: AnyObject) {
        
        self.resignResponders()
        
        let stb = UIStoryboard(name: "Walkthrough", bundle: nil)
        let vc = stb.instantiateViewControllerWithIdentifier("mainview") as! MainViewController
       // self.navigationController?.pushViewController(vc, animated: true)
       presentViewController(vc, animated: false, completion: nil)
        
    }
    @IBAction func signInAction(sender: AnyObject) {
        self.resignResponders()
        self.view.viewMoveToDown()
        
        
        if self.emailFeildView.textField.text != "" && self.passwordFieldView.textField.text != ""{
            
         if Utilities.isValidEmail(self.emailFeildView.textField.text!) {
            
            
            Utilities.sharedInstance.disableScreen(self.view)
            
            self.setImageOnTextFieldAction(ImageUtilities.textbox_tick!)
            
            var refreshedToken =  self.saveUserDefaults.objectForKey("FIRInstanceIDToken")
            print(refreshedToken)
            if refreshedToken == nil {
                
                refreshedToken = "pushID"
            }

            
            let params:[String:AnyObject] = [Constant.email:self.emailFeildView.textField.text!,
                Constant.password:self.passwordFieldView.textField.text!,
                Constant.pushID:"\(refreshedToken!)"
            ]

            let login = LoginService()
            login.execute(params, callback: { (errorMessage, message, response) -> Void in
                
                Utilities.sharedInstance.enableScreeen()
                
                if errorMessage != nil{
                    
                
                    Utilities.makeToastOfString("\(errorMessage!)", onView: self.view)
                   //for check
                
     
                    
                }else if errorMessage == nil && response == nil{
                    
                    Utilities.makeToastOfString("\(message!)", onView: self.view)
                    
                    let stb = UIStoryboard(name: "Main", bundle: nil)
                    let vc = stb.instantiateViewControllerWithIdentifier("pinVerify") as! PinValidationAfterSignUpOrSignIn
                  //  self.navigationController?.pushViewController(vc, animated: true)
                    self.presentViewController(vc, animated: false, completion: nil)
                    
                } else{
                    
  
                    
                    let stb = UIStoryboard(name: "AppFlow", bundle: nil)
                    let vc = stb.instantiateViewControllerWithIdentifier("TabsViewController") as! TabsViewController
                    self.presentViewController(vc, animated: false, completion: nil)
                    
                    
                    //self.performSegueWithIdentifier("signIn", sender: self)
                    
                }
                
                
            })
            
         }else{
            
            self.emailFeildView.setTextFieldActionImage(ImageUtilities.warning)
            
            }

            
        
        }else{
            
            if self.emailFeildView.textField.text == ""{
            
                self.emailFeildView.setTextFieldActionImage(ImageUtilities.warning)
                
            }
            
            if self.passwordFieldView.textField.text == "" {
            
                self.passwordFieldView.setTextFieldActionImage(ImageUtilities.warning)
            }
        
        }
        
        
       
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            
            return true
        }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        
        if textField == self.emailFeildView.textField{
            
            return newLength <= 50
            
        }else{
            
            return newLength <= 35
        }
        
        
        
        
        
    }


}
