//
//  AppSingleton.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/18/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation

class AppSingleton {
    
 let userdefaults = NSUserDefaults()
    
    class var sharedInstance : AppSingleton{
        
        struct Static {
            
            static let instance = AppSingleton()
        }
        
        return Static.instance
        
    }
    
    var loginUserObj:LoginUser?
    var notificationCount:Int = 0
    
    
    
    var isComingFromFDeleget:ComingFromFeedback?
    var getNotificationCountDeleget:GetNotificationCountDeleget?
    var okBtnClickOnEmailPopUp:getOkActionOfEmailSendDeleget?

    
    

    func loginWithSavedCredentialsSuccessfull() -> Bool {
     
        if let dataString = userdefaults.valueForKey("UserSessionJson"){
        
            if let userDic = stringToDic(dataString as! NSString){
//            
             
              
                let loginUser = LoginUser()
                loginUser.parseJson(userDic)
                
                AppSingleton.sharedInstance.loginUserObj = loginUser
                return true
            }
         
            
            
        }
        
      return false
    }
    
    
    func stringToDic(dataString:NSString)->[String:AnyObject]?{
    
        if let data = dataString.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                
                let mydic = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject]
                
                return mydic!
                
                
            } catch let error as NSError {
                print(error)
                return nil
                
            }
        }
        
      return nil
    
    }
    
    func dicToString(dic:NSDictionary)->NSString?{
    
    
        do {
            let data = try NSJSONSerialization.dataWithJSONObject(dic, options:[])
            let dataString = NSString(data: data, encoding: NSUTF8StringEncoding)!
            
            
            return dataString
            // do other stuff on success
            
        } catch {
            print("JSON serialization failed:  \(error)")
        }
        
        
        return nil
    }
    
}

protocol ComingFromFeedback {
    
    func isComingFromFeedback(isBool:Bool)
    
}
protocol GetNotificationCountDeleget{


    func setNotificationCount()
}

protocol getOkActionOfEmailSendDeleget{

    func okBtnClick()
}

