//
//  ResetPassViewController.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/11/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class ResetPassViewController: UIViewController,UITextFieldDelegate {

  
    @IBOutlet weak var emailView: TextFieldView!
    
    @IBOutlet weak var resetButton: CustomButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setInitialValue()
        self.setInitialViewRepresentation()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.emailView.textField.resignFirstResponder()
       // self.view.viewMoveToDown()
    }
    
    private func setInitialValue(){
    
       
    }
    
    private func setInitialViewRepresentation(){
    
        self.emailView.setTextFieldIconImage(ImageUtilities.ic_email!)
        self.emailView.setPlaceHolderOFField("Email")
        self.emailView.textField.keyboardType = UIKeyboardType.EmailAddress
        self.emailView.setTextFieldActionImage(nil)
        self.emailView.textField.delegate = self
        
        self.resetButton.setBacColor(ColorUtilities.buttonBackGroungBlueColor)
    
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func resetAction(sender: AnyObject) {
        
        self.emailView.textField.resignFirstResponder()
        
        if self.emailView.textField.text != ""{
            
            if Utilities.isValidEmail(self.emailView.textField.text!){
                
                Utilities.sharedInstance.disableScreen(self.view)
                
                
                
                
                let params:[String:AnyObject] = [
                    Constant.email:self.emailView.textField.text!
                ]
            
            
            let forgetPassService = ForgetPasswordService()
                forgetPassService.execute(params, callback: { (errorMessage, message, serviceResponse) -> Void in
                    
                    Utilities.sharedInstance.enableScreeen()
                    
                    if errorMessage == nil {
                        
                        self.performSegueWithIdentifier("toChangePass", sender: self)
                        
                    }else{
                        
                        Utilities.makeToastOfString("\(errorMessage!)", onView: self.view)
                    }
                    
                })
             
            }else{
            
                    self.emailView.setTextFieldActionImage(ImageUtilities.warning)
            }
        
        }else{
        
            self.emailView.setTextFieldActionImage(ImageUtilities.warning)
        }
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            
            return true
        }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
            return newLength <= 50
            
        
        
        
        
        
    }
    
    @IBAction func backAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toChangePass"{
            
            let vc = segue.destinationViewController as! ResetPassAndValidationViewController
                vc.email = self.emailView.textField.text
        
        }
    }
}
