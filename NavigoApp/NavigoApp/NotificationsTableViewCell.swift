//
//  NotificationsTableViewCell.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 09/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var notificationDescription: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var viewImg: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.viewImg.layer.cornerRadius = 25
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
