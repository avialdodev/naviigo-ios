//
//  UpdateGuidService.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 09/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//


import Alamofire

class UpdateGuidService: BaseService {
    
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: BaseEntity?) -> Void) {
        
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)/api/users/updateGuide", parameters: params as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            if response.result.isSuccess{
                
                
                let statusResponse = Response()
                statusResponse.parseJson(response.result.value?.valueForKey(Constant.response) as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                    let userGuide = UserGuide()
                    userGuide.parseJson(response.result.value?.valueForKey(Constant.GuideObj) as? NSDictionary)
                    
                    AppSingleton.sharedInstance.loginUserObj?.asGuide = userGuide
                    
                    
                    callback(errorMessage: nil, message: statusResponse.developerMessage, serviceResponse: userGuide)
                    
                }else{
                    
                    callback(errorMessage: statusResponse.developerMessage, message: nil, serviceResponse: nil)
                    
                }
                
                
            }else{
                
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
            }
            
            
        }
        
    }
}

