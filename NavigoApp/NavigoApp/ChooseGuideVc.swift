//
//  ChooseGuideVc.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 13/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import STRatingControl

class ChooseGuideVc: UIViewController,UITableViewDelegate,UITableViewDataSource,STRatingControlDelegate {

    @IBOutlet weak var guideTV: UITableView!
    
    
    @IBOutlet weak var backImagev: UIImageView!
    
    var guides:[Guide]  = []
    
    var city    = ""
    var country = ""
    
    var toGuideDetail:Guide? = nil
    var user:LoginUser!
    var unUsedImageView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

 
        
        self.user = AppSingleton.sharedInstance.loginUserObj
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
          let lBbItem = UIBarButtonItem(image: UIImage(named:"back-arrow1"), style: .Plain, target: self, action: #selector(ChooseGuideVc.goToBack))
        self.navigationItem.leftBarButtonItem = lBbItem

        
        self.guideTV.tableFooterView = UIView()

        
        self.callGetGuideRequest()
 
        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = true
    }
    
    func goToBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    private func callGetGuideRequest(){
    
        
        let params:[String:AnyObject] = [Constant.city:city,
                                         Constant.country:country,
                                         Constant.id:self.user.iD]
        
      
        Utilities.sharedInstance.disableScreen(self.view)
        
        let getGuide = GetGuideService()
        getGuide.execute(params) { (errorMessage, message, serviceResponse) in
            
           Utilities.sharedInstance.enableScreeen()
            
            if serviceResponse != nil{
                
               self.guides = serviceResponse!
                
                if self.guides.count == 0{
                
                    self.guideTV.hidden = true
            self.backImagev.image = UIImage(named:"no_guide_found")

                
                }else{
                
                    self.backImagev.image = nil
                    self.guideTV.hidden = false
  
                }
                self.guideTV.reloadData()                
                
            }else{
                
                Utilities.makeToastOfString(errorMessage!, onView: self.view)
            }
            
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.guides.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("guideCell") as! ChooseGuideCell
            cell.name.text = self.guides[indexPath.row].name
            cell.ratingView.rating = self.guides[indexPath.row].rating
        
        let url = NSURL(string : "\(Constant.image_Link)\(self.guides[indexPath.row].imageURL).jpeg")
    
        //let url = NSURL(string : "http://lorempixel.com/400/200")
        cell.imageViewG.sd_setImageWithURL(url, placeholderImage: UIImage(named: "user")) { (image, error, cache, url) in
            
            
            if image != nil{
                
               cell.imageViewG.image = image!
                
            }else{
                
                cell.imageViewG.image = UIImage(named: "user")
            }
        }
            cell.cityAndCountry.text = "\(self.guides[indexPath.row].city), \(self.guides[indexPath.row].country)"
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print()
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.toGuideDetail = self.guides[indexPath.row]
        self.performSegueWithIdentifier("toGuideDetail", sender: self)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 95
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toGuideDetail"{
            
            let vc = segue.destinationViewController as! GuideDetailViewController
            print(self.toGuideDetail)
                vc.guid = self.toGuideDetail
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
