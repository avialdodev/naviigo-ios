//
//  PaymentRequestVC.swift
//  NavigoApp
//
//  Created by Salman Raza on 2/4/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class PaymentRequestVC: UIViewController,UITextFieldDelegate,UITextViewDelegate,UIPopoverPresentationControllerDelegate,ComingFromFeedback {


    @IBOutlet weak var priceFieldV: TextFieldView!
    @IBOutlet weak var youGetFV: TextFieldView!
    @IBOutlet weak var startDateFV: TextFieldView!
    @IBOutlet weak var endDateFieldV: TextFieldView!
    
    @IBOutlet weak var descriptionV: UITextView!
    @IBOutlet weak var otherPersonName: UILabel!
    @IBOutlet weak var guidname: UILabel!
    @IBOutlet weak var rejectButton: CustomButton!
    
    @IBOutlet weak var travellerImageView: UIImageView!
    @IBOutlet weak var accptButton: CustomButton!
    
    @IBOutlet weak var cancelTripButton: CustomButton!
  
    var dateFormatter = NSDateFormatter()
    var unUsedImageView = UIImageView()
    
    var trip:Trip? = nil
    var isGuide:Bool!
    
    var backView = UIView()
    var currentWindow = UIApplication.sharedApplication().keyWindow
    
    
    var tempStartDate:String? = nil
    var selectedStartDate:String = ""
    var startDateForRestriction:NSDate!
    var temStartDateForRestriction:NSDate!
    
    
    var tempEndDate:String? = nil
    var selectedEndDate:String = ""
    var endDateForRestriction:NSDate!
    var temEndDateForRestriction:NSDate!
    
    var rightButon:UIBarButtonItem!

    let errorImage = UIImage(named: "warning")
    let textBox_Tick = UIImage(named: "textbox_tick")
    
    var travellerImage:String?
    
    var user:LoginUser!

    var travelerName:String!
    var travellerId:Int     = 0
    
    
    var tripFeedBack:String!
    var tripStars:Int!
    
    var imageUrl:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user = AppSingleton.sharedInstance.loginUserObj!
          backView.frame = self.view.frame
        
       // dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
       // dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        dateFormatter.dateFormat = "yyy-MM-dd h:mm a"

        self.title = "Payment Request"
        self.setPriceFieldView()
        self.setStartDateFieldView()
        self.setEndDateFieldView()
        self.setYouGetFieldView()
        
        
        self.descriptionV.layer.borderWidth = 1
        self.descriptionV.layer.borderColor = ColorUtilities.borderColor.CGColor
        
        
         rightButon = UIBarButtonItem(title: "Send", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(PaymentRequestVC.save))
        

        self.accptButton.setBacColor(ColorUtilities.buttonBackGroungBlueColor)
        self.rejectButton.setBacColor(ColorUtilities.redColor)
        self.cancelTripButton.setBacColor(ColorUtilities.redColor)

        self.travellerImageView.layer.cornerRadius = 35
        self.travellerImageView.clipsToBounds     = true
        
   

        AppSingleton.sharedInstance.isComingFromFDeleget = self
      
        
        
            // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.setViewAccordingToUser()
        
        self.tabBarController?.tabBar.hidden = true
        
        self.priceFieldV.textField.delegate   = self
        self.youGetFV.textField.delegate = self
        self.startDateFV.textField.delegate = self
        self.endDateFieldV.textField.delegate = self
        self.descriptionV.delegate = self
        
        let lBbItem = UIBarButtonItem(image: UIImage(named:"back-arrow1"), style: .Plain, target: self, action: #selector(PaymentRequestVC.goToBack))
        self.navigationItem.leftBarButtonItem = lBbItem
        
        if self.user.isGuide == 1{
            
            
            if self.trip == nil {
            
                 self.navigationItem.rightBarButtonItem = self.rightButon
            }else{
            
                self.navigationItem.rightBarButtonItem = nil
                
            }
           
            
        }else{
        
             self.navigationItem.rightBarButtonItem = nil
        }
        

        
    }
    
    
    
    override func viewDidAppear(animated: Bool) {
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = false

    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.priceFieldV.textField.resignFirstResponder()
        self.view.viewMoveToDown()
    }
    
    func goToBack(){
    
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    private func setViewAccordingToUser(){
    

        
        
        print("trip-----ID = \(self.trip?.id)")
        if self.trip != nil && self.isGuide != nil{
            
            self.userInteractionHandel(false)

        
            self.setTripValues()
            
            if self.isGuide!{
                
                self.guidname.text        = self.user.name
                self.otherPersonName.text = self.trip!.name!
                self.imageUrl             = self.trip!.imageName
                
                
                self.accptButton.hidden = true
                self.rejectButton.hidden = true
                self.cancelTripButton.hidden = true
                self.userInteractionHandel(false)

                
                switch self.trip!.status!{
                
 

                case 1:
                    self.cancelTripButton.hidden = false
                    self.cancelTripButton.setTitle("Cancel Trip", forState: UIControlState.Normal)

                
                case 3:
                    self.cancelTripButton.hidden = false
                    self.tripStars = self.trip!.rating
                    self.tripFeedBack = self.trip!.feedback
                    self.cancelTripButton.backgroundColor = ColorUtilities.buttonBackGroungBlueColor
                    self.cancelTripButton.setTitle("Show Feedback", forState: UIControlState.Normal)
   
                default:
                    self.accptButton.hidden = true
                    self.rejectButton.hidden = true
                    self.cancelTripButton.hidden = true
                    self.userInteractionHandel(false)
                
                }
            
            }else{
                
                self.guidname.text        = self.trip!.name!
                self.otherPersonName.text = self.user.name
                self.imageUrl             = self.user.imageURL!
            
            
                self.userInteractionHandel(false)
                self.cancelTripButton.hidden = true
                self.accptButton.hidden = false
                self.rejectButton.hidden = false
                
                
                switch self.trip!.status!{
                    
                    
                    
                case 0:

                    self.accptButton.setTitle("Accept Trip", forState: UIControlState.Normal)
                    self.rejectButton.setTitle("Reject Trip", forState: UIControlState.Normal)
                    
                case 1:
                    
                    self.accptButton.setTitle("Complete Trip", forState: UIControlState.Normal)
                    self.rejectButton.setTitle("Cancel Trip", forState: UIControlState.Normal)
                    
                case 3:
                    
                    self.cancelTripButton.hidden = false
                    self.accptButton.hidden = true
                    self.rejectButton.hidden = true
                    
                    self.tripStars = self.trip!.rating
                    self.tripFeedBack = self.trip!.feedback
                    self.cancelTripButton.backgroundColor = ColorUtilities.buttonBackGroungBlueColor
                    self.cancelTripButton.setTitle("Show Feedback", forState: UIControlState.Normal)
                    
                default:
                    self.accptButton.hidden = true
                    self.rejectButton.hidden = true
                    self.cancelTripButton.hidden = true
                    
                }
            }
            
            
        }else{
            
            
            if let travellerName =  self.travelerName {
            
                 self.otherPersonName.text = travellerName
            }
            
            if let travellerImg = self.travellerImage {
            
                self.imageUrl = travellerImg
            
            }
            
            self.guidname.text        = self.user.name
           
        
            self.userInteractionHandel(true)
            self.accptButton.hidden = true
            self.rejectButton.hidden = true
            self.cancelTripButton.hidden = true
        
        }
        
        self.setImageOfTraveller()
        
    }
    
    private func setImageOfTraveller(){
    
 
        
        print(imageUrl)
        if imageUrl != nil{
            
            
            let url = NSURL(string : "\(Constant.image_Link)\(imageUrl!).jpeg")
            
            
            self.unUsedImageView.sd_setImageWithURL(url, placeholderImage: UIImage(named: "user")) { (image, error, cache, url) in
                
                
                if image != nil{
                    
                    self.travellerImageView.image = image!
                    
                }else{
                    
                    self.travellerImageView.image = UIImage(named: "user")
                }
            }
            
        }else{
            
            self.travellerImageView.image = UIImage(named: "user")
            
        }

    }
    
    private func setTripValues(){
    
        self.startDateFV.textField.text = self.trip!.startDate!
        self.endDateFieldV.textField.text = self.trip!.endDate
        self.descriptionV.text            = self.trip!.description
        self.priceFieldV.textField.text   = "\(self.trip!.amount!)"
        print()
        self.youGetFV.textField.text      = "\(self.trip!.guideShare!)"
        
    }
    
    private func userInteractionHandel(bool:Bool){
    
        self.priceFieldV.userInteractionEnabled = bool
        self.youGetFV.userInteractionEnabled    = bool
        self.startDateFV.userInteractionEnabled = bool
        self.endDateFieldV.userInteractionEnabled = bool
        self.descriptionV.userInteractionEnabled  = bool
    
    }

    private func setPriceFieldView(){
        
        self.priceFieldV.textField.setPlaceHolderOFField("00000")
        self.priceFieldV.setTextFieldIconImage(ImageUtilities.usDollor)
        self.priceFieldV.increasAreaOfIcon()
        self.priceFieldV.setimageVTintColor()
         self.priceFieldV.textField.backgroundColor = ColorUtilities.taxtFBackColor
        self.priceFieldV.setTextFieldActionImage(nil)
        self.priceFieldV.setBorderColor()
        self.priceFieldV.textField.textColor = ColorUtilities.buttonBackGroungBlueColor
        self.priceFieldV.textField.keyboardType = .NumberPad
        
        self.priceFieldV.textField.delegate = self
       

    
    }
    
    private func setYouGetFieldView(){
        
        self.youGetFV.textField.setPlaceHolderOFField("00000")
       self.youGetFV.setTextFieldIconImage(ImageUtilities.usDollor)
        self.youGetFV.setimageVTintColor()
        self.youGetFV.increasAreaOfIcon()
        self.youGetFV.textField.backgroundColor = ColorUtilities.taxtFBackColor
        self.youGetFV.setTextFieldActionImage(nil)
        self.youGetFV.setBorderColor()
        self.youGetFV.textField.textColor = ColorUtilities.buttonBackGroungBlueColor
        self.youGetFV.textField.delegate = self
        self.youGetFV.textField.userInteractionEnabled = false

    }
    
    private func setStartDateFieldView(){
        
        self.startDateFV.setPlaceHolderOFField("Start Date")
        self.startDateFV.setBorderColor()
        self.startDateFV.setTextFieldActionImage(nil)
        self.startDateFV.setTextFieldIconImage(ImageUtilities.date)
        self.startDateFV.increasAreaOfIcon()
        self.startDateFV.setimageVTintColor()
        self.startDateFV.textField.backgroundColor = ColorUtilities.taxtFBackColor
        self.startDateFV.textField.delegate = self
        
    }
    
    private func setEndDateFieldView(){
        
        self.endDateFieldV.setPlaceHolderOFField("End Date")
        self.endDateFieldV.setBorderColor()
        self.endDateFieldV.setTextFieldActionImage(nil)
        self.endDateFieldV.setTextFieldIconImage(ImageUtilities.date)
        self.endDateFieldV.increasAreaOfIcon()
        self.endDateFieldV.setimageVTintColor()
        self.endDateFieldV.textField.backgroundColor = ColorUtilities.taxtFBackColor
        self.endDateFieldV.textField.delegate = self

    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func goBack(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        
        self.view.viewMoveToDown()
        textField.resignFirstResponder()
        
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n") {
            descriptionV.resignFirstResponder()
            self.view.viewMoveToDown()
            return false
        }
        
        let newText = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        return numberOfChars <= 200
      
  
    }
    func textViewDidBeginEditing(textView: UITextView) {
        
        self.view.viewMoveToUp()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField == self.priceFieldV.textField{
            
            if let value = Double(textField.text!) {
            
                
                
                self.youGetFV.textField.text = String(format: "%.2f", Float(value * 0.8))
            }
        
        
        }
    }
    
        func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
                guard let text = textField.text else {
        
                    return true
                }
        
                let newLength = text.characters.count + string.characters.count - range.length
        
            
            if textField == self.priceFieldV.textField{
            
                return newLength <= 4
            
            }else{
                
                return newLength <= 7
            
            }
            
                    
        
                
                
            }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
       
        
        if textField == self.startDateFV.textField{
            
            startDateAction(textField)
            self.view.viewMoveToUp()
            
            
        }else if textField == self.endDateFieldV.textField{
        
        
            endDateAction(textField)
            self.view.viewMoveToUp()
            
            
        }else if textField == self.priceFieldV.textField {
            
            self.addReturnButtonOnKeyboard(textField)
        
        }
        
        
    }
    
    private func addReturnButtonOnKeyboard(sender:UITextField){
        
        let toolBar = UIToolbar()
        let returnButton = UIBarButtonItem(title: "Return", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(PaymentRequestVC.resignOnReturn))
        
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        
        toolBar.barStyle = UIBarStyle.Default
        //datePickerToolBar.translucent = true
        toolBar.backgroundColor = UIColor.whiteColor()
        toolBar.tintColor = UIColor(red: 0/255, green: 140/255, blue: 220/255, alpha: 1)
        toolBar.sizeToFit()
        toolBar.setItems([spaceButton,returnButton], animated: false)
        toolBar.userInteractionEnabled = true
        
        
        sender.inputAccessoryView = toolBar

        
    
    }
    
    func resignOnReturn(){
    
        self.priceFieldV.textField.resignFirstResponder()
        self.youGetFV.textField.resignFirstResponder()
    }
    
    
    
    private func endDateAction(sender:UITextField){
        
        let datePickerView = UIDatePicker()
        
        let date = NSDate()
        
//        if self.startDateFV.textField.text != "" {
//            
//            let tomorrow = NSCalendar.currentCalendar()
//                .dateByAddingUnit(
//                    .Day,
//                    value: 1,
//                    toDate: self.startDateForRestriction,
//                    options: []
//            )
//            
//            
//           
//           
//            let a = NSCalendar.currentCalendar().dateBySettingHour(0, minute: 1, second: 0, ofDate: self.startDateForRestriction, options: [])
//            
//             datePickerView.minimumDate = a
//           
//        }else{
        
            let tomorrow = NSCalendar.currentCalendar()
                .dateByAddingUnit(
                    .Day,
                    value: 1,
                    toDate: date,
                    options: []
            )
          //  datePickerView.minimumDate = tomorrow
            
            
     // }
        
        datePickerView.minimumDate = NSDate()
        
        let datePickerToolBar = UIToolbar()
        let doneButtonDate = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(PaymentRequestVC.donePickerEndDate))
        
        let cancelButtonDate = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(PaymentRequestVC.CancelPickerEndDate))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        
        datePickerToolBar.barStyle = UIBarStyle.Default
        //datePickerToolBar.translucent = true
        datePickerToolBar.backgroundColor = UIColor.whiteColor()
        datePickerToolBar.tintColor = UIColor(red: 0/255, green: 140/255, blue: 220/255, alpha: 1)
        datePickerToolBar.sizeToFit()
        datePickerToolBar.setItems([cancelButtonDate,spaceButton,doneButtonDate], animated: false)
        datePickerToolBar.userInteractionEnabled = true
        
        
        datePickerView.datePickerMode = UIDatePickerMode.DateAndTime
        
        datePickerView .addTarget(self, action: #selector(PaymentRequestVC.endDatePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
        
        
        
        sender.inputView = datePickerView
        sender.inputAccessoryView = datePickerToolBar
        
        self.tempEndDate = dateFormatter.stringFromDate(datePickerView.date)
        self.endDateFieldV.textField.text = self.tempEndDate
        self.temEndDateForRestriction = datePickerView.date
        currentWindow?.addSubview(self.backView)
        
    }
    
    
    func endDatePickerValueChanged(sender:UIDatePicker) {
        
        print(sender.date)
        self.temEndDateForRestriction = sender.date
        
        let date = dateFormatter.stringFromDate(sender.date)
        self.tempEndDate = date
        self.endDateFieldV.textField.text = self.tempEndDate!
        
    }
    
    func donePickerEndDate(){
        
         self.view.viewMoveToDown()
        self.endDateForRestriction = self.temEndDateForRestriction
        
        if self.tempEndDate != nil{
            
            self.selectedEndDate = self.tempEndDate!
            self.endDateFieldV.textField.text = self.selectedEndDate
            
        }
        
        self.endDateFieldV.textField.resignFirstResponder()
        self.backView.removeFromSuperview()
        
    }
    
    func CancelPickerEndDate(){
        
        self.view.viewMoveToDown()
        self.temEndDateForRestriction = self.endDateForRestriction
        
        self.endDateFieldV.textField.text = self.selectedEndDate
        self.endDateFieldV.textField.resignFirstResponder()
        
        self.backView.removeFromSuperview()
        
    }
    
    
    
    private func startDateAction(sender:UITextField){
                
        let datePickerView = UIDatePicker()
        
//        if self.endDateFieldV.textField.text != "" {
//            
//            let yesturday = NSCalendar.currentCalendar()
//                .dateByAddingUnit(
//                    .Day,
//                    value: -1,
//                    toDate: self.endDateForRestriction,
//                    options: []
//            )
//            datePickerView.maximumDate = yesturday
//            
//        }
        
        datePickerView.minimumDate = NSDate()
        
        
        let datePickerToolBar = UIToolbar()
        let doneButtonDate = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(PaymentRequestVC.donePickerStartDate))
        
        let cancelButtonDate = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(PaymentRequestVC.cancelPickerStartDate))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        
        datePickerToolBar.barStyle = UIBarStyle.Default
        //datePickerToolBar.translucent = true
        datePickerToolBar.backgroundColor = UIColor.whiteColor()
        datePickerToolBar.tintColor = UIColor(red: 0/255, green: 140/255, blue: 220/255, alpha: 1)
        datePickerToolBar.sizeToFit()
        datePickerToolBar.setItems([cancelButtonDate,spaceButton,doneButtonDate], animated: false)
        datePickerToolBar.userInteractionEnabled = true
        
        
        datePickerView.datePickerMode = UIDatePickerMode.DateAndTime
        datePickerView .addTarget(self, action: #selector(PaymentRequestVC.startDatePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
        
        
        
        sender.inputView = datePickerView
        sender.inputAccessoryView = datePickerToolBar
        
        self.tempStartDate = dateFormatter.stringFromDate(datePickerView.date)
        self.startDateFV.textField.text = self.tempStartDate
        self.temStartDateForRestriction =  datePickerView.date
        
        currentWindow?.addSubview(self.backView)
        
    }
    
    
    func startDatePickerValueChanged(sender:UIDatePicker) {
        
        let date = dateFormatter.stringFromDate(sender.date)
        self.tempStartDate = date
        self.startDateFV.textField.text = self.tempStartDate!
        
        self.temStartDateForRestriction = sender.date
        
    }
    
    func donePickerStartDate(){
        
        
        self.view.viewMoveToDown()
        if self.tempStartDate != nil{
            
            self.selectedStartDate = self.tempStartDate!
            self.startDateFV.textField.text = self.selectedStartDate
            
            self.startDateForRestriction = self.temStartDateForRestriction
                }
        
        self.startDateFV.textField.resignFirstResponder()
        self.backView.removeFromSuperview()
        
    }
    
    func cancelPickerStartDate(){
        
        self.view.viewMoveToDown()
        self.temStartDateForRestriction = self.startDateForRestriction
        self.startDateFV.textField.text = self.selectedStartDate
        self.startDateFV.textField.resignFirstResponder()

        self.backView.removeFromSuperview()
        
    }
    
//    func getDateInOurFormat(date:NSDate)->NSDate{
//
//        let dateInString   = self.dateFormatter.stringFromDate(date)
//        let ourDate = dateFormatter.dateFromString(dateInString)
//
//        return ourDate!
//        
//    }
    
    private func checkWhoseFieldIsEmpty(){
        
        
        self.checkTextFieldASetImage(self.startDateFV)
      self.checkTextFieldASetImage(self.endDateFieldV)
        self.checkTextFieldASetImage(self.priceFieldV)
        self.checkTextFieldASetImage(self.youGetFV)
        
        if self.descriptionV.text == ""{
            
            Utilities.showAlertwithTitle("Alert", description: "Description is empty", parentViewController: self)
            
        }
        
    
        
        
    }
    
    private func checkTextFieldASetImage(textFieldV:TextFieldView){
        
        if textFieldV.textField.text == ""{
            
            textFieldV.setTextFieldActionImage(errorImage)
            
        }else{
            
            textFieldV.setTextFieldActionImage(textBox_Tick)
        }
        
    }
    
    func secondBetweenDates(startDate: NSDate, endDate: NSDate) -> Int
    {
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Second], fromDate: startDate, toDate: endDate, options: [])
        return components.second
    }
    
   func save(){
    
        if self.descriptionV.text != "" && self.priceFieldV.textField.text != "" && self.youGetFV.textField.text != "" && self.selectedStartDate != "" && self.selectedEndDate != "" {
            
            if self.startDateForRestriction != nil && self.endDateForRestriction != nil{
                
                if  self.startDateForRestriction.compare(self.endDateForRestriction) == .OrderedAscending{
                    
                    if secondBetweenDates(self.startDateForRestriction, endDate: self.endDateForRestriction) >= Constant.limitForEndDate{
                        
                        Utilities.sharedInstance.disableScreen(self.view)
                        
                        let params:[String:AnyObject] = [  "travlerID": self.travellerId,
                                                           "guideID": self.user.asGuide!.iD!,
                                                           "amount": Int(self.priceFieldV.textField.text!)!,
                                                           "guideShare": Float(self.youGetFV.textField.text!)!,
                                                           "startDate": self.startDateFV.textField.text!,
                                                           "endDate": self.endDateFieldV.textField.text!,
                                                           "description": self.descriptionV.text!]
                        
                        
                        let sendRequestS = SendTripRequest()
                        sendRequestS.execute(params, callback: { (errorMessage, message, serviceResponse) in
                            
                            
                            Utilities.sharedInstance.enableScreeen()
                            
                            if errorMessage == nil{
                                
                                print(message)
                                
                                self.navigationController?.popViewControllerAnimated(true)
                                
                            }else{
                                
                                Utilities.makeToastOfString(errorMessage!, onView: self.view)
                            }
                        })
                    }else{
                        if self.descriptionV.text != ""{
                            Utilities.showAlertwithTitle("", description: Constant.endDateGreater, parentViewController: self)
                        }
                        self.startDateFV.setTextFieldActionImage(errorImage)
                        self.endDateFieldV.setTextFieldActionImage(errorImage)
                    }
                }else{
                    if self.descriptionV.text != ""{
                        Utilities.showAlertwithTitle("", description: Constant.endDateGreater, parentViewController: self)
                    }
                    self.startDateFV.setTextFieldActionImage(errorImage)
                    self.endDateFieldV.setTextFieldActionImage(errorImage)
                }
            }else{
                self.startDateFV.setTextFieldActionImage(errorImage)
                self.endDateFieldV.setTextFieldActionImage(errorImage)
            }
            
        }else{
    
            checkWhoseFieldIsEmpty()
        }
    }
    @IBAction func acceptAction(sender: AnyObject) {
        
        
       if  self.trip!.status == 0{
        
            if self.user.hasCreditCard == 1{
            
                self.acceptCall()
        
            }else{
        
                Utilities.showAlertwithTitle("Alert", description: Constant.pleaseAddCreditCard, parentViewController: self)
            }
        
        
       }else if self.trip!.status == 1{
        
           self.performSegueWithIdentifier("toRate", sender: self)
        
        }
 
    }

    @IBAction func rejectAction(sender: AnyObject) {
        
        if  self.trip!.status == 0{
            
            self.rejectCall()
            
        }else if self.trip!.status == 1{
            
            self.cancelTripCall()
        }
        
    }
    @IBAction func cancelTripAction(sender: AnyObject) {
        
        if self.trip!.status == 1{
            
             self.cancelTripCall()
        
        }else if self.trip!.status == 3{
            
            self.performSegueWithIdentifier("toRate", sender: self)
           
        }
        
    }
    
    private func cancelTripCall(){
    
        Utilities.sharedInstance.disableScreen(self.view)
        
        
        var cancelId:Int!
        
        if self.isGuide!{
            
            cancelId = self.user.asGuide!.iD
        
        }else{
        
            cancelId = self.user.iD
        }
        
        let aceptRTripS = AcceptRejectCancelTripS()
        aceptRTripS.execute([   "userID": cancelId, "tripID": self.trip!.id,"status": 4,"serviceLink":"\(Constant.SERVICE_MAIN_LINK)api/trip/cancelTrip"]) { (errorMessage, message, serviceResponse) in
            
            Utilities.sharedInstance.enableScreeen()
            if errorMessage == nil{
                
                self.navigationController?.popViewControllerAnimated(true)
            }else{
                
                Utilities.makeToastOfString(errorMessage!, onView: self.view)
            }
        }
        

    }

    
    private func rejectCall(){
    
        Utilities.sharedInstance.disableScreen(self.view)
        
        let aceptRTripS = AcceptRejectCancelTripS()
        aceptRTripS.execute([   "userID": self.user.iD!, "tripID": self.trip!.id!,"status": 2,"serviceLink":"\(Constant.SERVICE_MAIN_LINK)api/trip/acceptDeclineTrip"]) { (errorMessage, message, serviceResponse) in
            Utilities.sharedInstance.enableScreeen()
            
            if errorMessage == nil{
                
                self.navigationController?.popViewControllerAnimated(true)
                
            }else{
                
                Utilities.makeToastOfString(errorMessage!, onView: self.view)
            }
        }

    
    }
    
    private func acceptCall(){
    
        
        Utilities.sharedInstance.disableScreen(self.view)
        
        let aceptRTripS = AcceptRejectCancelTripS()
        aceptRTripS.execute(["userID": self.user.iD!, "tripID": self.trip!.id,"status": 1,"serviceLink":"\(Constant.SERVICE_MAIN_LINK)api/trip/acceptDeclineTrip"]) { (errorMessage, message, serviceResponse) in
            
            Utilities.sharedInstance.enableScreeen()
            
            if errorMessage == nil{
                
                self.navigationController?.popViewControllerAnimated(true)
            }else{
                
                Utilities.makeToastOfString(errorMessage!, onView: self.view)
                
                print(errorMessage!)
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if segue.identifier == "toRate"{
            
            
            let  filterController = segue.destinationViewController as! RatingViewController
                 filterController.trip = self.trip
            
            if self.tripFeedBack != nil && self.tripStars != nil{
            
                filterController.previousFeedback = self.tripFeedBack
                filterController.previousStars    = self.tripStars
                
            }
            
   
            filterController.preferredContentSize = CGSize(width: 280 , height: 320)
            let controller = filterController.popoverPresentationController
            
            if controller != nil{
                
                controller?.delegate = self
                controller?.sourceView = self.view
                controller?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                controller?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds),0,0)
            }
            
            
            
        }
        
    }
    
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        
        
    }
    
    func popoverPresentationControllerShouldDismissPopover(popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    
    
    func isComingFromFeedback(isBool: Bool) {
        
        if isBool{
        
            self.navigationController?.popViewControllerAnimated(true)
        }
    }


}
