//
//  ProfileTableViewController.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC on 24/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import STRatingControl
import SDWebImage

class ProfileTableViewController: UITableViewController,UIPopoverPresentationControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,STRatingControlDelegate,UITextFieldDelegate,GetNotificationCountDeleget{

    @IBOutlet weak var nameField: CustomTextFields!
    @IBOutlet weak var editPencil: UIImageView!
   // @IBOutlet weak var editImageB: UIButton!
    @IBOutlet weak var editImageView: UIImageView!
    @IBOutlet weak var ratintView: STRatingControl!
    @IBOutlet weak var phoneNumber: CustomTextFields!
    @IBOutlet weak var becomeAGuideLable: UILabel!
    
    @IBOutlet weak var creditCardLable: UILabel!
    
     let userdefaults = NSUserDefaults()
    
    var isEdit = false
    var imageview = UIImageView()
        var user:LoginUser!
    
    var selectedImageInBase64:String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.setEditModeView()
       self.user = AppSingleton.sharedInstance.loginUserObj
 
        self.getUserImage()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileTableViewController.changeImage))
        self.editImageView.userInteractionEnabled = true
        self.editImageView.addGestureRecognizer(tapGestureRecognizer)
 
    
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        AppSingleton.sharedInstance.getNotificationCountDeleget = self
        
        let getNotificationS = GetNotificationCount()
        getNotificationS.execute(AppSingleton.sharedInstance.loginUserObj!.iD)
        
        setInitialViewRepresentation()
        
        
        self.tabBarController?.tabBar.hidden = false
    }

    
    override func viewWillDisappear(animated: Bool) {
        
       // AppSingleton.sharedInstance.getNotificationCountDeleget = nil
    }
    private func setInitialViewRepresentation(){
        
        self.navigationController?.navigationBar.barTintColor = ColorUtilities.buttonBackGroungBlueColor
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
     //   self.title = "Profile"
        

        editImageView.contentMode = .ScaleAspectFill
        self.tableView.tableFooterView = UIView()
        self.editImageView.layer.cornerRadius = self.editImageView.bounds.size.width/2
        self.editImageView.clipsToBounds = true
        
        if self.user.hasCreditCard == 1{
            self.creditCardLable.text =  "Update Credit Card"
        }else{
            self.creditCardLable.text =  "Add Credit Card"
        }
        
        self.nameField.text = self.user.name
        self.ratintView.sizeToFit()
        
        if self.user.isGuide == 1{
            
            self.ratintView.rating = self.user.asGuide!.rating
            self.becomeAGuideLable.text = "Update Guide Profile"
        }else{
        
            self.ratintView.rating = 0
             self.becomeAGuideLable.text = "Become a Guide"
        }
        
     //   self.ratintView.rating =
        self.phoneNumber.text = self.user.phone
    }
    // MARK: - Table view data source

    func setEditModeView(){
    
        self.nameField.layer.cornerRadius = 5
        self.phoneNumber.layer.cornerRadius = 5
        self.nameField.delegate = self
        self.phoneNumber.delegate = self
        
        self.editPencil.hidden = true
        self.editImageView.userInteractionEnabled = false
    }
    
    private func getUserImage(){
    
        
        let url = NSURL(string : "\(Constant.image_Link)\(self.user.imageURL).jpeg")
        // let url = NSURL(string : "http://lorempixel.com/400/200")
        
        imageview.sd_setImageWithURL(url, placeholderImage: UIImage(named: "user")) { (image, error, cache, url) in
            
            
            if image != nil{
                
                self.editImageView.image = image
                
                if let image = image{
                    
                    if  let compressImageData  =  image.jpeg(.lowest){
                        
                        self.selectedImageInBase64 = compressImageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                        
                        
                        
                    }
                    
                    
                }
                
            }else{
                
                self.editImageView.image = UIImage(named: "user")
            }
        }
        
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 6
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
        
            return 220
        }else {
        
        
            return 50
        }
    }
    
  override  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
      self.nameField.resignFirstResponder()
        self.phoneNumber.resignFirstResponder()
    
      if indexPath.row == 1{
        
        performSegueWithIdentifier("toUpatePAss", sender: self)
    
      } else if indexPath.row == 2{
            
            performSegueWithIdentifier("toCreditCard", sender: self)
        
        }else if indexPath.row == 3{
            
            performSegueWithIdentifier("toBecomeAGuide", sender: self)
        
      }else if indexPath.row == 4 {
    
            performSegueWithIdentifier("toAboutUs", sender: self)
    
       }else if indexPath.row == 5{
        
        userdefaults.removeObjectForKey("UserSessionJson")
        
        AppSingleton.sharedInstance.loginUserObj = nil
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let signInVc = story.instantiateViewControllerWithIdentifier("login") as! SignViewController
        presentViewController(signInVc, animated: true, completion: nil)
        
        
      }
        
    }
    
    @IBAction func editAction(sender: UIBarButtonItem) {
        
        if isEdit {
            
            self.whenSave(sender)
        
        }else{

            self.whenEdit(sender)
            
        }
        
    }
    
    
    private func whenSave(sender: UIBarButtonItem){
    
        self.nameField.resignFirstResponder()
        self.phoneNumber.resignFirstResponder()
        
        if self.nameField.text != "" && self.phoneNumber.text != "" && self.selectedImageInBase64 != nil{
            
            let params:[String:AnyObject] = ["userID": self.user.iD,
                                             "name": self.nameField.text!,
                                             "phone": self.phoneNumber.text!,
                                             "image":self.selectedImageInBase64!]
            
            Utilities.sharedInstance.disableScreen(self.view)
            
            let updateProfileS = UpdateProfileService()
                updateProfileS.execute(params, callback: { (errorMessage, message, userImageUrl) in
                    
                    
                    Utilities.sharedInstance.enableScreeen()
                    
                    if errorMessage == nil{
                        
                   //     sender.setImage(UIImage(named:"Edit-104"), forState: UIControlState.Normal)
                  
                        sender.title = "Edit"
                            
                        self.isEdit  = false
                        
                        self.nameField.enabled = false
                        self.nameField.backgroundColor = UIColor.clearColor()
                        self.phoneNumber.enabled = false
                        self.phoneNumber.backgroundColor = UIColor.clearColor()
                        
                        
                        self.editPencil.hidden = true
                        self.editImageView.userInteractionEnabled = false
                        
                        if let userDic =  AppSingleton.sharedInstance.stringToDic(self.userdefaults.valueForKey("UserSessionJson") as! NSString){
                            
                            var dic = userDic as! [String:AnyObject]
                            
                            
                            dic["name"] = self.nameField.text!
                            dic["phone"] = self.phoneNumber.text!
                            dic["imageURL"] = userImageUrl!
                            
                            self.user.name = self.nameField.text!
                            self.user.phone = self.phoneNumber.text!
                            self.user.imageURL = userImageUrl!
                            print(dic)
                            
                            if let userSessionString = AppSingleton.sharedInstance.dicToString(dic){
                                
                                print(userSessionString)
                                self.userdefaults.setValue(userSessionString, forKey: "UserSessionJson")
                            }
                        }
//
//
                    
                    }else{
                    Utilities.showAlertwithTitle("Alert", description: errorMessage!, parentViewController: self)
                        
                      //  Utilities.makeToastOfString(errorMessage!, onView: self.view)
                    
                    }
                })
        
        }else{
            
            if self.selectedImageInBase64 == nil{
                
                Utilities.showAlertwithTitle("Alert", description: Constant.SOME_THING_WENT_WRONG, parentViewController: self)
            
            }else{
            
                Utilities.showAlertwithTitle("Alert", description: Constant.someFieldsAreEmpty, parentViewController: self)

            }
        
        
        }
        
   
    
    }
    
    private func whenEdit(sender: UIBarButtonItem){
        
        
         sender.title =    "Save"
//        sender.title = "Save"
        self.isEdit  = true
        
        self.nameField.enabled = true
        self.nameField.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.4)
        
        self.phoneNumber.enabled = true
        self.phoneNumber.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.4)
        
        self.editPencil.hidden = false
        self.editImageView.userInteractionEnabled = true
    
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toUpatePAss"{
            
            
            let  filterController = segue.destinationViewController as! UpdatePassPopUp
            filterController.preferredContentSize = CGSize(width: 280 , height: 350)
            let controller = filterController.popoverPresentationController
            
            if controller != nil{
                
                controller?.delegate = self
                controller?.sourceView = self.view
                controller?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                controller?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds),0,0)
            }
            
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        
        
    }
    
    func popoverPresentationControllerShouldDismissPopover(popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            
            return true
        }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        if textField == self.nameField{
        
            return newLength <= 25
        }else{
        
             return newLength <= 14
        
        }
        
        
        
       
        
    }
    
     func changeImage() {
        
        presentUploadPictureActionSheet()
        
    }
    
    private func presentUploadPictureActionSheet() {
        
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        alertController.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { (action) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                
                let imagePicker                    = UIImagePickerController()
                imagePicker.sourceType             = .Camera
                imagePicker.cameraCaptureMode      = .Photo
                imagePicker.modalPresentationStyle = .FullScreen
                imagePicker.allowsEditing          = false
                imagePicker.delegate               = self
                
                self.presentViewController(imagePicker, animated: true, completion: nil)
                
            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Gallery", style: .Default, handler: { (action) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary) {
                
                let imagePicker             = UIImagePickerController()
                imagePicker.sourceType      = .PhotoLibrary
                imagePicker.allowsEditing   = false
                imagePicker.delegate        = self
                
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action) -> Void in
            
            
        }))
        
        
        self.presentViewController(alertController, animated: true) { () -> Void in
            
        }
        
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        
        self.editImageView.image =  info[UIImagePickerControllerOriginalImage] as? UIImage
        self.editImageView.contentMode   = .ScaleAspectFill
        //self.uploeditImageBadPicture.imageView?.clipsToBounds = true
        self.view.bringSubviewToFront(self.editImageView)
        
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let image = image{
            
            if  let compressImageData  =  image.jpeg(.lowest){
                
                self.selectedImageInBase64 = compressImageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)

                print(selectedImageInBase64!)
                
            }else{
                Utilities.makeToastOfString(Constant.SOME_THING_WENT_WRONG, onView: self.view)
                
                
            }
            
            
            
            
            //    imageData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
            
        }else{
            
            Utilities.makeToastOfString(Constant.SOME_THING_WENT_WRONG, onView: self.view)
        }
        
        
     //   self.selectedImageInBase64 = imageData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
        
        
        
        
        dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func toNotificationScreen(sender: AnyObject) {
        
        self.goToNotificationScreen()
        
    }
    
    func setNotificationCount() {
        if AppSingleton.sharedInstance.notificationCount > 0 {
            
            self.navigationItem.leftBarButtonItem?.addBadge(number: AppSingleton.sharedInstance.notificationCount)
        }else{
            
            self.navigationItem.leftBarButtonItem?.removeBadge()
        }
        
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
