//
//  CreditCardViewController.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/31/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import QuartzCore
import Braintree

class CreditCardViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate ,UITextFieldDelegate{

    @IBOutlet weak var cvvTextFieldView: TextFieldView!
   // @IBOutlet weak var cvvField: CustomTextFields!
    @IBOutlet weak var monthField: CustomTextFields!
    @IBOutlet weak var yearField: CustomTextFields!
    
    @IBOutlet weak var cardNameFieldView: TextFieldView!
    @IBOutlet weak var CardNumberFieldView: TextFieldView!
    @IBOutlet weak var titleLable: UILabel!
    
    @IBOutlet weak var rightBarButtonS: UIButton!
   // @IBOutlet weak var cvvImagV: UIImageView!
    
    var user:LoginUser?
    
    var monthPickerView = UIPickerView()
    let forFieldMonth = ["Jan","Feb","Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep","Oct","Nov","Dec"]
    let monthData = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    var yearPickerView = UIPickerView()
    let yearData = ["2017","2018","2019","2020","2021","2022"]
    
    var selectedMont:Int? = nil
    var selectedYear:Int? = nil
    
    
    var tempMonthSelect = 6
    var tempYearSelect = 0
    
    var isCreditCard = false
    
    let userdefaults = NSUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user = AppSingleton.sharedInstance.loginUserObj
        
        print(self.user?.hasCreditCard)
        
        if user!.hasCreditCard == 1 {
            
            self.isCreditCard = true
//            self.rightBarButtonS.setImage(UIImage(named:"Edit-104"), forState: UIControlState.Normal)
            
        }else{
            
            self.isCreditCard = false
           
//            self.rightBarButtonS.setImage(UIImage(named:"Save-100"), forState: UIControlState.Normal)
        }
        
        self.setTitleAndCreditBool()
        self.setInitialValue()
        self.setInitialviewRepresentation()
       

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = true

    }
    
    override func viewDidAppear(animated: Bool) {
        
        if isCreditCard{
        
            self.setPreviousData()
        }
 
    }

    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.resignResponders()
      
    }
    
    private func resignResponders(){
        
        self.cardNameFieldView.textField.resignFirstResponder()
        self.CardNumberFieldView.textField.resignFirstResponder()
        self.cvvTextFieldView.textField.resignFirstResponder()
        
          self.view.viewMoveToDown()
    
    }
    
    private func setPreviousData(){
    
        self.selectedYear = Int((self.user!.creditCardObj?.year)!)
        self.selectedMont = Int((self.user!.creditCardObj?.month)!)
        
        self.monthField.text = self.forFieldMonth[Int((self.user!.creditCardObj!.month))!-1]
        self.yearField.text = (self.user?.creditCardObj?.year)!
        
        self.cvvTextFieldView.textField.text = "XXX"
    
    }
    private func setInitialValue(){
        
        monthPickerView.delegate                        = self
        monthPickerView.dataSource                      = self
        
        yearPickerView.delegate                         = self
        yearPickerView.dataSource                       = self
        
        self.CardNumberFieldView.textField.delegate     = self
        self.cvvTextFieldView.textField.delegate = self
        
   
        
     
    }
    
    private func setTitleAndCreditBool(){
    

        
        if self.isCreditCard{
            
            self.titleLable.text = "Update a Credit Card"
            
            
        }else{
            
            self.titleLable.text = "Add a Credit Card"
        }
    
    }
    
    private func setInitialviewRepresentation(){

        self.navigationController?.navigationBarHidden = true
        
        self.makeViewsBorder()
        self.leftSpaceInField()
        
        self.setCardNumFieldView()
        self.setCardNameFieldView()
        self.setCvvFieldView()
        
        
    }
    
    private func leftSpaceInField(){
        
        self.monthField.makeStartingSpace(12)
        self.yearField.makeStartingSpace(12)
        

    }
    
    private func setCvvFieldView(){
    
    
        self.cvvTextFieldView.setTextFieldIconImage(ImageUtilities.ic_password)
        self.cvvTextFieldView.textField.secureTextEntry = true
        self.cvvTextFieldView.textField.keyboardType = UIKeyboardType.NumberPad
        self.cvvTextFieldView.setPlaceHolderOFField("CVV")
        self.cvvTextFieldView.setBorderColor()
        self.cvvTextFieldView.textField.makeStartingSpace(26)
        self.cvvTextFieldView.iconLeftSpaceCons.constant = 5
        self.cvvTextFieldView.actionIconRightMarginC.constant = 5
        self.cvvTextFieldView.layoutIfNeeded()
    }
    
    private func setCardNameFieldView(){
    
        self.cardNameFieldView.setTextFieldActionImage(nil)
       // self.cardNameFieldView.textField.makeStartingSpace(40)
        self.cardNameFieldView.setTextFieldIconImage(ImageUtilities.ic_user)
        self.cardNameFieldView.textField.delegate = self
        self.cardNameFieldView.setPlaceHolderOFField("Name")
        self.cardNameFieldView.textField.autocapitalizationType = .Words
        
        if isCreditCard{
            
              self.cardNameFieldView.textField.text =  self.user?.creditCardObj?.name!
            
        }
        
        
      
        self.cardNameFieldView.setBorderColor()
        

    }
    
    private func setCardNumFieldView(){
    
        
        self.CardNumberFieldView.setTextFieldIconImage(ImageUtilities.ic_password)
     //   self.CardNumberFieldView.textField.makeStartingSpace(40)
        self.CardNumberFieldView.textField.keyboardType = UIKeyboardType.NumberPad
        self.CardNumberFieldView.setTextFieldActionImage(nil)
        
        self.CardNumberFieldView.setPlaceHolderOFField("Card Number")
        
        if isCreditCard{
            
            print("XXXXXXXXXXXX\(self.user!.creditCardObj!.last4Digit!)")
            
             self.CardNumberFieldView.textField.text =  "XXXXXXXXXXXX\(self.user!.creditCardObj!.last4Digit!)"
            
        }
     
        self.CardNumberFieldView.setBorderColor()
    }
    
    private func makeViewsBorder(){
        
        
        self.monthField.makeViewBorder()
        self.yearField.makeViewBorder()
    
    }

    @IBAction func save(sender: AnyObject) {
        
        if self.CardNumberFieldView.textField.text?.characters.count == 16 && !(self.CardNumberFieldView.textField.text?.characters.contains("X"))! && self.cardNameFieldView.textField.text != "" &&  self.selectedYear != nil && self.selectedMont != nil && (self.cvvTextFieldView.textField.text?.characters.count == 3 || self.cvvTextFieldView.textField.text?.characters.count == 4 ) && !(self.cvvTextFieldView.textField.text?.characters.contains("X"))! {
            
            if self.isCreditCard{
                
                let alertC = UIAlertController(title: "", message: Constant.updateCreditCardText, preferredStyle: UIAlertControllerStyle.Alert)
                let yesAction  = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alertAction) in
                    
                    self.callCreditCardServices()
                    
                })
                
                let NoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in
                    
                }
                
                alertC.addAction(NoAction)
                alertC.addAction(yesAction)
                
                self.presentViewController(alertC, animated: true, completion: nil)
            
            }else{
            
                self.callCreditCardServices()
            }
            
        
        }else{
        
            checkWhoseFieldIsEmpty()
        
        }


    }
    
    
    
    private func callCreditCardServices(){
    
        self.resignResponders()
        self.CardNumberFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
        self.cardNameFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
        self.cvvTextFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
        
    
        Utilities.sharedInstance.disableScreen(self.view)
        
        let getClientApi = GetClientTokenService()
        getClientApi.execute { (errorMessage, message, token) in
            
            
            
            if errorMessage == nil{
                
                
                let braintreeClient = BTAPIClient(authorization: token!)!
                
                let cardClient = BTCardClient(APIClient: braintreeClient)
                let card = BTCard(number: "\(self.CardNumberFieldView.textField.text!.stringByReplacingOccurrencesOfString("-", withString:"") )",
                    expirationMonth: "\(self.selectedMont!)" ,
                    expirationYear: "\(self.selectedYear!)",
                    cvv: self.cvvTextFieldView.textField.text!)
                
                card.cardholderName = self.cardNameFieldView.textField.text!
                
                cardClient.tokenizeCard(card) { (tokenizedCard, error) in
                    
                    if error == nil {
                        
                        self.addOrUpdateCreditCard((tokenizedCard?.nonce)!)
                    }else{
                        
                        Utilities.sharedInstance.enableScreeen()
                        
                        Utilities.makeToastOfString("\(error!)", onView: self.view)
                        
                        
                    }
                }
                
            }else{
                
                Utilities.sharedInstance.enableScreeen()
                Utilities.makeToastOfString("\(errorMessage!)", onView: self.view)

            }
        }

        
    }
    
    private func addOrUpdateCreditCard(token: String){
    
        if self.isCreditCard{
        
            let params:[String:AnyObject] = [Constant.userID:self.user!.iD,
                                             Constant.token:token,
                                             Constant.ID:(self.user?.creditCardObj?.ID)!,
                                             Constant.customerID:(self.user?.creditCardObj?.customerID)!]
            
            print(params)
            
            let addCreditApi = UpdateCreditService()
            addCreditApi.execute(params, callback: { (errorMessage, message, serviceResponse) in
                
               
                
               
                
                Utilities.sharedInstance.enableScreeen()
                if errorMessage == nil {
                    
                     let credit = serviceResponse as! CreditCard
                    self.user?.creditCardObj = credit
                    
                    self.saveUserCreditValuesInUD(self.user!.creditCardObj!)
                    
                    Utilities.makeToastOfString("Successfully Updated", onView: self.view)
                    
                   
                    
                }else{
                    
                    Utilities.makeToastOfString("\(errorMessage!)", onView: self.view)

                }
                
            })
            
            
        }else{
            
            let params:[String:AnyObject] = [Constant.userID:self.user!.iD,
                                             Constant.token:token]
        
            let addCreditApi = AddCreditCardServic()
                addCreditApi.execute(params, callback: { (errorMessage, message, serviceResponse) in
                    
                  
                    
                    Utilities.sharedInstance.enableScreeen()
                    if errorMessage == nil {
                    
                        
                        let credit = serviceResponse as! CreditCard
                        
                        self.user?.creditCardObj = credit
                        self.user?.hasCreditCard = 1
                        

                        self.saveUserCreditValuesInUD(self.user!.creditCardObj!)
                        
                        Utilities.makeToastOfString("Successfully Added", onView: self.view)
                        self.title = "Update a Credit Card"
//                        self.rightBarButtonS.setImage(UIImage(named:"Edit-104"), forState: UIControlState.Normal)
                        
                    }else{
                    
                        Utilities.makeToastOfString("\(errorMessage!)", onView: self.view)
                    }
                    
                })
        }
    
        
    }
    
    private func checkWhoseFieldIsEmpty(){
        
        
        if self.cvvTextFieldView.textField.text! == "XXX" || (self.CardNumberFieldView.textField.text?.characters.contains("X"))! || self.selectedMont == nil || self.selectedYear == nil{
        
       
            if ((self.CardNumberFieldView.textField.text?.characters.contains("X"))!){
                
                Utilities.makeToastOfString("Credit Card number is invalid", onView: self.view)
                
                self.CardNumberFieldView.textField.text = ""
                
            }else if ((cvvTextFieldView.textField.text?.characters.contains("x"))!){
                
                Utilities.makeToastOfString("CVV number is invalid", onView: self.view)
                self.cvvTextFieldView.textField.text = ""
                
            }else if selectedMont == nil{
            
                Utilities.makeToastOfString("Month not selected", onView: self.view)
            }else if selectedYear == nil{
            
                Utilities.makeToastOfString("Year not selected", onView: self.view)
            }
            
            
            
            
        }
    
        if  (self.CardNumberFieldView.textField.text?.characters.count)! == 16 &&  !(self.CardNumberFieldView.textField.text?.characters.contains("X"))!{
            
            self.CardNumberFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
            
        }else{
            
            self.CardNumberFieldView.setTextFieldActionImage(ImageUtilities.warning)
            
        }
        
        if self.cardNameFieldView.textField.text == "" {
            
            self.cardNameFieldView.setTextFieldActionImage(ImageUtilities.warning)
            
        }else{
            
            self.cardNameFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
        }
        
        if (self.cvvTextFieldView.textField.text?.characters.count == 3 || self.cvvTextFieldView.textField.text?.characters.count == 4) && self.cvvTextFieldView.textField.text! != "XXX"{
            
            self.cvvTextFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
            
            
        }else{
            
            self.cvvTextFieldView.setTextFieldActionImage(ImageUtilities.warning)
           
        }
    
    }
    @IBAction func cancelAction(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func yearAction(sender: UITextField) {
        
        
        self.tempYearSelect = 0
        self.yearPickerView.selectRow(self.tempYearSelect, inComponent: 0, animated: true)
        
        
        let datePickerToolBar = UIToolbar()
        
        let doneButtonYear = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: "donePickerYear")
        
        let cancelButtonYear = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancelPickerYear")
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        
        doneButtonYear.tintColor = UIColor(red: 97/255, green: 207/255, blue: 233/255, alpha: 1)
        cancelButtonYear.tintColor = UIColor(red: 255/255, green: 120/255, blue: 141/255, alpha: 1)
        
        datePickerToolBar.barStyle = UIBarStyle.Default
        datePickerToolBar.translucent = true
        datePickerToolBar.backgroundColor = UIColor.whiteColor()
        //  datePickerToolBar.tintColor = UIColor(red: 0/255, green: 140/255, blue: 220/255, alpha: 1)
        datePickerToolBar.sizeToFit()
        datePickerToolBar.setItems([cancelButtonYear,spaceButton,doneButtonYear], animated: false)
        datePickerToolBar.userInteractionEnabled = true
        
        sender.inputAccessoryView = datePickerToolBar
        sender.inputView = self.yearPickerView
        
    }
    
    func cancelPickerYear(){
        
        yearField.resignFirstResponder()
        
    }
    
    func donePickerYear(){
        
    //    self.yearImageView.image = UIImage(named: "arrow_down")
        self.selectedYear = Int(self.yearData[self.tempYearSelect])
        self.yearField.text = self.yearData[self.tempYearSelect]
        yearField.resignFirstResponder()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            
            return true
        }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        if textField == self.cvvTextFieldView.textField{
            
            return newLength <= 4
            
        }else if textField == self.CardNumberFieldView.textField {
            
            return newLength <= 16
            
        }else if textField == self.cardNameFieldView.textField{
        
          return  newLength <= 25
        }
        
        
        return true
    }

 
    @IBAction func monthAction(sender: UITextField) {
   
        
        self.tempMonthSelect = 6
        self.monthPickerView.selectRow(self.tempMonthSelect, inComponent: 0, animated: true)
        
        let datePickerToolBar = UIToolbar()
        
        
        let doneButtonMonth = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: "donePickerMonth")
        
        let cancelButtonMonth = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancelPickerMonth")
        
        doneButtonMonth.tintColor = UIColor(red: 97/255, green: 207/255, blue: 233/255, alpha: 1)
        cancelButtonMonth.tintColor = UIColor(red: 255/255, green: 120/255, blue: 141/255, alpha: 1)
        
        
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        
        datePickerToolBar.barStyle = UIBarStyle.Default
        datePickerToolBar.translucent = true
        // datePickerToolBar.backgroundColor = UIColor.whiteColor()
        //datePickerToolBar.tintColor = UIColor(red: 0/255, green: 140/255, blue: 220/255, alpha: 1)
        datePickerToolBar.sizeToFit()
        datePickerToolBar.setItems([cancelButtonMonth,spaceButton,doneButtonMonth], animated: false)
        datePickerToolBar.userInteractionEnabled = true
        
        sender.inputAccessoryView = datePickerToolBar
        sender.inputView = self.monthPickerView
        //self.month(self, action: "departureDatePickerValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
    }
    
    func cancelPickerMonth(){
        
        monthField.resignFirstResponder()
        
    }
    
    func donePickerMonth(){
    
        self.selectedMont = tempMonthSelect + 1
        self.monthField.text = self.forFieldMonth[self.selectedMont! - 1]
        self.monthField.resignFirstResponder()
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == monthPickerView{
            
            tempMonthSelect = row
        }else{
            
            tempYearSelect = row
        }
        
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == monthPickerView{
            
            return monthData.count
        }else{
            return yearData.count
        }
        
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == monthPickerView{
            
            return monthData[row]
            
        }else{
            
            return yearData[row]
            
        }
    }
    
    func getClientToken(){
    
        Utilities.sharedInstance.disableScreen(self.view)
        
        let getClientApi = GetClientTokenService()
            getClientApi.execute { (errorMessage, message, token) in
                
                Utilities.sharedInstance.enableScreeen()
                
                if errorMessage == nil{
                
                    print(token)
                }else{
                    
                    print(errorMessage)
                }
        }
        
        
        
    }

    
    private func saveUserCreditValuesInUD(creditValues:CreditCard){
    
        if let userDic =  AppSingleton.sharedInstance.stringToDic(self.userdefaults.valueForKey("UserSessionJson") as! NSString){
            
            var dic = userDic 
          
            let creditDic:[String:AnyObject] = [  "ID" :creditValues.ID,
                                                  "customerID" : creditValues.customerID,
                                                  "isActive" :    creditValues.isActive,
                                                  "last4Digit" : creditValues.last4Digit ,
                                                  "month" : creditValues.month,
                                                  "name" : creditValues.name,
                                                  "year" : creditValues.year]
          
            
            dic["hasCreditCard"] = 1
            dic["creditCardObj"] = creditDic
            
            if let userSessionString = AppSingleton.sharedInstance.dicToString(dic){
                
                print(userSessionString)
                self.userdefaults.setValue(userSessionString, forKey: "UserSessionJson")
                
            }
            
    }
}

}
