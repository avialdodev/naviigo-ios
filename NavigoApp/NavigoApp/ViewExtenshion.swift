//
//  ViewExtenshion.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/16/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    func viewMoveToUp(){
        
        UIView.animateWithDuration(0.3) { () -> Void in
            
            self.bounds.origin.y = self.frame.origin.y + 200
        }
        
    }
    
    func viewMoveToDown(){
        
        UIView.animateWithDuration(0.3) { () -> Void in
            
            self.bounds.origin.y = 0
        }
    }

    func makeBorder(){
    
        self.layer.borderWidth = 1
        self.layer.borderColor = ColorUtilities.borderColor.CGColor
    }
}