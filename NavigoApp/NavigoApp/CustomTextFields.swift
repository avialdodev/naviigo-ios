//
//  CustomTextFields.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/11/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class CustomTextFields: UITextField {

    var limit:Int!
  

    func makeStartingSpace(space:Int){
        
        let textFSpace = UILabel(frame: CGRect(x: 0, y: 0, width: space, height: 40))
        textFSpace.backgroundColor = UIColor.clearColor()
        
        self.leftView = textFSpace
        
        self.leftViewMode = UITextFieldViewMode.Always
        self.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
    
    }
    
    func makeViewBorder(){
        
       
       layer.borderColor = ColorUtilities.borderColor.CGColor
        makeCorner()
      
    }
    
    func increaseFontSize(){
    
        let newFontSize = UIFont(name: (self.font?.fontName)!, size: 17)
        self.font = newFontSize
    }
    
    func makeCorner(){
    
        layer.borderWidth = 1
        layer.cornerRadius = 5
    }
    
    func setPlaceHolderOFField(text:String){
        
        self.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSForegroundColorAttributeName: ColorUtilities.placeHolderColor])
    }
    
//    func setMinimumLimit(num:Int){
//        
//        self.limit = num
//        self.delegate = self
//    
//    }
//    
//    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        
//        guard let text = textField.text else {
//            
//            return true
//        }
//        
//        let newLength = text.characters.count + string.characters.count - range.length
//        
//        return newLength <= self.limit
//            
//
//        
//        
//    }
    
}
