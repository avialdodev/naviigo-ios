//
//  GetTripDetainServic.swift
//  NavigoApp
//
//  Created by Muhammad Salman on 4/1/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//


import Foundation
import Alamofire

class GetTripDetainServic {
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: Trip?) -> Void) {
        
        print("\(Constant.SERVICE_MAIN_LINK)api/users/\(params["userID"]!)/getTrip/\(params["tripID"]!)")
        
        Alamofire.request(.GET, "\(Constant.SERVICE_MAIN_LINK)api/user/\(params["userID"]!)/getTrip/\(params["tripID"]!)", parameters: nil, encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            
            print(response)
            if response.result.isSuccess{
                
                let statusResponse = Response()
                statusResponse.parseJson(response.result.value?.valueForKey(Constant.response) as! NSDictionary)
                
                if statusResponse.status == 1{
                                        
                    if  let responseTrip = response.result.value!.valueForKey("tripObj") as? NSDictionary{
                    
                        let tripModel = Trip()
                        tripModel.parseJson((responseTrip))
                        
                        
                        
                        callback(errorMessage: nil, message: statusResponse.developerMessage, serviceResponse: tripModel)
                    
                    }else{
                        
                        callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
                    
                    }
                    
               
                        
     
                    
                }else{
                    
                    callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
                    
                }
                
                
            }else{
                
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
            }
            
        }
        
    }
}


