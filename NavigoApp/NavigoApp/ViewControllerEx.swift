//
//  ViewControllerEx.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 09/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{

    
    func goToNotificationScreen(){
    
        
        let story = UIStoryboard(name: "AppFlow", bundle: nil)
        let vc    = story.instantiateViewControllerWithIdentifier("NotificationsViewController") as! NotificationsViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    
    }

}