//
//  WebViewController.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 30/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webViewS: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

     //   webViewS.loadRequest(URLRequest(url: URL(string: "http://hardwirestudios.com")!))

        webViewS.loadRequest(NSURLRequest(URL: NSURL(string: "http://www.naviigo.com/terms.html")!))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }

}
