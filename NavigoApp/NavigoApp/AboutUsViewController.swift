//
//  AboutUsViewController.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 31/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet weak var webViewS: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Our Website"
        
        webViewS.loadRequest(NSURLRequest(URL: NSURL(string: "http://www.naviigo.com")!))

        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        let lBbItem = UIBarButtonItem(image: UIImage(named:"back-arrow1"), style: .Plain, target: self, action: #selector(AboutUsViewController.goToBack))
        self.navigationItem.leftBarButtonItem = lBbItem

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = false
    }
    @objc private func goToBack(){
    
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
