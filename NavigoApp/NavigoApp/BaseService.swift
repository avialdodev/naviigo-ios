//
//  BaseService.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/21/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation

protocol BaseService{

    func execute(params:NSDictionary,callback:(errorMessage:String?,message:String?,serviceResponse:BaseEntity?)->Void)

}