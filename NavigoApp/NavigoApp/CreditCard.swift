//
//  CreditCard.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/18/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation

class CreditCard : BaseEntity{
    
    var ID:Int!
    var customerID:String!
    var month:String!
    var year:String!
    var last4Digit:String!
    var name:String!
    var isActive:Int!
    
    
     override init(){

    }
    
//    init(id:Int,customerID:String,month:String,year:String,last4digit:String,name:String) {
//        
//        self.ID = id
//        self.customerID = customerID
//        self.month = month
//        self.year = year
//        self.last4Digit = last4digit
//        self.name = name
//        
//    }
    
    override func parseJson(response: NSDictionary?) {
        
        if let creditResponse = response{
            
            self.ID         =  creditResponse.valueForKey("ID") as! Int
            self.customerID =  creditResponse.valueForKey("customerID") as! String
            self.month      =  creditResponse.valueForKey("month") as! String
            self.year       =  creditResponse.valueForKey("year") as! String
            self.last4Digit = creditResponse.valueForKey("last4Digit") as! String
            self.name       = creditResponse.valueForKey("name") as? String ?? ""
            
            print(creditResponse.valueForKey("isActive"))
            self.isActive   =   creditResponse.valueForKey("isActive") as? Int ?? 1

        }
        
        
    }
   
}