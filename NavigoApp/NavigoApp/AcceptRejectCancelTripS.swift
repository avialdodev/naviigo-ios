//
//  AcceptRejectTripS.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 14/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//


import Foundation
import Alamofire

class AcceptRejectCancelTripS:BaseService{
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: BaseEntity?) -> Void) {
        
        print(params)
        
        Alamofire.request(.POST, params["serviceLink"] as! String, parameters: params as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            print("response\(response)")
            
            if response.result.isSuccess {
                
                let statusResponse = Response()
                statusResponse.parseJson(response.result.value as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                    callback(errorMessage: nil, message: statusResponse.developerMessage, serviceResponse: nil)
                    
                }else{
                    
                    callback(errorMessage: statusResponse.developerMessage, message: nil, serviceResponse: nil)
                    
                }
                
            }else {
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message:nil, serviceResponse: nil)
            }
            
        }
        
        
    }
    
}