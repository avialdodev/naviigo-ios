//
//  Constant.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/18/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation

class Constant {
   
    // Constans
    static let GOOGLE_API = "AIzaSyCdFf3nzAIqJ3xJExj2vpxXUTCChuJcToA"
    static let PLACE_API  = "AIzaSyDJrTQnUeOw0qFCO7oIjA0Y5bSl_cCFOmY"
    static let SERVICE_MAIN_LINK        = "http://navigo.azurewebsites.net/"
    static let image_Link        = "http://navigo.azurewebsites.net/upload/user/"
    
    
    static let addressLimitition = "Please Add Street Address"
    
    static let SOME_THING_WENT_WRONG    = "Something went wrong"
    
    
    static let passwordLimitition = 6
    static let limitForEndDate  = 60
    
    
    //Parameters
    static let email                = "email"
    static let password             = "password"
    static let pushID               = "pushID"
    
    static let response             = "response"
    static let status               = "status"
    static let developerMessage     = "developerMessage"
    static let GuideObj             = "GuideObj"
    
    static let phone                = "phone"
    static let name                 = "name"
    static let profileImage         = "profileImage"
    
    static let token                = "token"
    static let id                   = "id"
    static let city                 = "city"
    static let message              = "message"
    
    static let country              = "country"
   
    static let userID = "userID"
    
    static let newPassword = "newPassword"
    static let customerID  = "customerID"
    static let ID          = "ID"
    
    static let bio              = "bio"
    static let expertise = "expertise"
    static let lowestPrice  = "lowestPrice"
 
    
    
    // Alert And toast messages 
    
    static let updateProfileMessage     = "Your profile has been updated"
    static let someFieldsAreEmpty       = "Some fields are empty"
    static let  pleaseAddYourLocation   = "Please add your Street Address"
    static let pleaseAddExperties       = "Please add your experties"
    static let pleaseSearchByCity       = "Please search by City"
    static let successfullyDone         = "Successfully done"
    static let errorInFetchingMyRequest = "Error in fetching my request"
    static let pleaseAddCreditCard      = "Please add Credit Card"
    static let pleaseAcceptTerm         = "Please accept Terms and Conditions"
    static let paswiordValidationText   = "Password should be greater than 5 character"
    static let updateCreditCardText     = "Are you sure you want to update your credit card?"
    static let guideAddText             = "Your guide account request has been submitted and is under review. You will get an email regarding its status with in few business days."
    static let endDateGreater           = "End date should be greater then start date"

}