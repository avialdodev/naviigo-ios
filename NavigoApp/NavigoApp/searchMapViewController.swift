//
//  searchMapViewController.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/31/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces


class searchMapViewController: UIViewController, GMSMapViewDelegate,CLLocationManagerDelegate,GMSAutocompleteResultsViewControllerDelegate,GetNotificationCountDeleget,UISearchBarDelegate {
    
    
    @IBOutlet weak var guideButton: UIButton!
    @IBOutlet weak var directionButton: UIButton!
    @IBOutlet weak var myLocationB: UIButton!

    @IBOutlet weak var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    
    var myCordinates:CLLocationCoordinate2D? = nil
    var destinationCordinates:CLLocationCoordinate2D? = nil
    var route:GMSPolyline? =  nil
    var destinationMarker:GMSMarker? = nil
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    
    var city:String? = nil
    var country:String? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        
      //  self.title = "Map"
        self.navigationController?.navigationBar.barTintColor = ColorUtilities.buttonBackGroungBlueColor
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        
        
       // self.navigationItem.leftBarButtonItem?.action = #selector(searchMapViewController.goToNotificationScreen)
        
        self.mapView.delegate = self
        
        self.locationManager.requestAlwaysAuthorization()

        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.myLocationB.layer.cornerRadius = 22
        self.guideButton.layer.cornerRadius = 22
        self.directionButton.layer.cornerRadius = 22
        
        self.directionButton.hidden = true
        self.guideButton.hidden     = true
        
        
        self.setGooglePlaceSearch()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
       AppSingleton.sharedInstance.getNotificationCountDeleget = self

        let getNotificationS = GetNotificationCount()
        getNotificationS.execute(AppSingleton.sharedInstance.loginUserObj!.iD)
        
        
        self.tabBarController?.tabBar.hidden = false
        self.navigationController?.navigationBar.layer.borderColor = UIColor.whiteColor().CGColor
        
//        if AppSingleton.sharedInstance.notificationCount > 0 {
//            
//            self.navigationItem.leftBarButtonItem?.addBadge(number: AppSingleton.sharedInstance.notificationCount)
//        }else{
//        
//            self.navigationItem.leftBarButtonItem?.removeBadge()
//        }

     // self.setMyPin()
    }
    
    override func viewWillDisappear(animated: Bool) {
        
     //   AppSingleton.sharedInstance.getNotificationCountDeleget = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setGooglePlaceSearch(){
        
     
    
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchBar.translucent = true
        searchController?.searchBar.tintColor = UIColor.whiteColor()
        searchController?.searchBar.placeholder="Search Your Destination"
        searchController?.searchBar.barTintColor = UIColor.whiteColor()
        searchController?.searchResultsUpdater = resultsViewController
        searchController?.searchBar.delegate = self
        
        let subView = UIView(frame: CGRect(x: 0, y: 65.0, width: self.view.frame.size.width , height: 45.0))
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        
        self.searchController?.searchBar.barTintColor = ColorUtilities.lightBlueColor
        self.searchController?.searchBar.tintColor  = UIColor.whiteColor()
        
        
        resultsViewController?.tableCellBackgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.4)
        resultsViewController?.primaryTextColor         = ColorUtilities.lightBlueColor
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
    
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {

       self.destinationMarker?.map = nil
       self.destinationCordinates = nil
        self.guideButton.hidden = true

       
    }
    func mapView(mapView: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {

       Utilities.sharedInstance.disableScreen(self.view)
      
        let longitude :CLLocationDegrees = coordinate.longitude
        let latitude :CLLocationDegrees =  coordinate.latitude
        
        let location = CLLocation(latitude: latitude, longitude: longitude) //changed!!!
        print(location)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            print(location)
            
            Utilities.sharedInstance.enableScreeen()
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0] 
                print(pm.addressDictionary)
                
                if let address = pm.addressDictionary{
                    
                    if let country = address["Country"] as? String{
                        
                        self.country = country
                        
                        if let city = address["City"] as? String{
                        
                            self.city = city
                            self.showDestinationMarker(coordinate)
                            self.searchController?.searchBar.text = "    \(self.city!), \(self.country!)"
                            
                    
                        }else if let name =  address["Name"] as? String{
                    
                            self.city = name
                            self.showDestinationMarker(coordinate)
                            self.searchController?.searchBar.text = "    \(self.city!), \(self.country!)"
                        
                        }else{
                    
                            Utilities.makeToastOfString(Constant.SOME_THING_WENT_WRONG, onView: self.view)
                        }
                    
                     
                    
                        
                
                    }else {
                    
                        Utilities.makeToastOfString(Constant.SOME_THING_WENT_WRONG, onView: self.view)                    }
                }else {
                    
                  Utilities.makeToastOfString(Constant.SOME_THING_WENT_WRONG, onView: self.view)                }
               
            }
            else {
                
                Utilities.makeToastOfString(Constant.SOME_THING_WENT_WRONG, onView: self.view)
                
                //print("Problem with the data received from geocoder")
            }
        })
      
      

    }

    func setMyPin(){
    
             mapView.settings.myLocationButton = true
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        if self.myCordinates == nil {
            
            self.myCordinates = locValue
            let marker = GMSMarker(position: self.myCordinates!)
            marker.title = "My Location"
            marker.icon = UIImage(named: "blueCircle")
            
            marker.map = mapView
                  mapView.camera = GMSCameraPosition(target: self.myCordinates!, zoom: 12, bearing: 0, viewingAngle: 0)
        }
        locationManager.stopUpdatingLocation()

        
    }
    
    

    
    func resultsController(resultsController: GMSAutocompleteResultsViewController, didAutocompleteWithPlace place: GMSPlace) {
        
        let addressComponents = place.addressComponents
        
        for addressComponent in addressComponents! {
            
            
            if addressComponent.type == "locality" {
                
                self.city = addressComponent.name
                
            }  else if addressComponent.type == "country" {
                
                self.country = addressComponent.name
            }
            
        }
        
        self.searchController?.active = false

        if self.city != nil{
            
    
                self.searchController?.searchBar.text = "    \(city!), \(self.country!)"
                self.showDestinationMarker(place.coordinate)
                mapView.camera = GMSCameraPosition(target: place.coordinate, zoom: 12, bearing: 0, viewingAngle: 0)

        }else{
            
            Utilities.showAlertwithTitle("Alert", description: Constant.pleaseSearchByCity, parentViewController: self)
        }
    }
    
    func resultsController(resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: NSError) {
        
        print("Error: ", error.localizedDescription)
        
    }
    
    func didUpdateAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible  = false
    }
    
    func didRequestAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    private func showDestinationMarker(coordinate:CLLocationCoordinate2D){
    
        route?.map = nil
        
        destinationMarker?.map = nil
        destinationMarker = GMSMarker(position: coordinate)
        destinationMarker!.title = "Source"
        destinationMarker!.icon = UIImage(named: "destinationPin")
        destinationMarker!.map = mapView
        
        self.destinationCordinates = coordinate
        
        if self.destinationCordinates != nil && self.myCordinates != nil && self.city != nil && self.country != nil{
            
            self.directionButton.hidden = true
            self.guideButton.hidden = false
            
        }
    
    }
    
 
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "toGuideDetail"{
            
            let guidesVC            = segue.destinationViewController as! ChooseGuideVc
                guidesVC.city       = self.city!
                guidesVC.country    = self.country!
        
        }
    }
    

    @IBAction func goToGuide(sender: AnyObject) {
        
        self.performSegueWithIdentifier("toGuideDetail", sender:self )
        
    }

    @IBAction func directionAction(sender: AnyObject) {
        
        let path = GMSMutablePath()
        path.addCoordinate(self.myCordinates!)
        path.addCoordinate(self.destinationCordinates!)
        
        
        
          route = GMSPolyline(path: path)
          route?.map = mapView
        
    }
    @IBAction func myLocation(sender: AnyObject) {
        
        if self.myCordinates == nil{
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }else{
          mapView.camera = GMSCameraPosition(target: self.myCordinates!, zoom: 12, bearing: 0, viewingAngle: 0)
        }
        
    }
    
   
    @IBAction func notification(sender: AnyObject) {
        
        self.goToNotificationScreen()
    }
    
    private func myPosition(){
    
    }
    
    func setNotificationCount() {
        if AppSingleton.sharedInstance.notificationCount > 0 {
            
            self.navigationItem.leftBarButtonItem?.addBadge(number: AppSingleton.sharedInstance.notificationCount)
        }else{
            
            self.navigationItem.leftBarButtonItem?.removeBadge()
        }

    }
}

