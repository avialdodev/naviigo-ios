//
//  TextFieldView.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/17/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

@IBDesignable class TextFieldView: UIView {

    @IBOutlet weak var iconImageWidthCons: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var actionIconRightMarginC: NSLayoutConstraint!
    @IBOutlet weak var iconLeftSpaceCons: NSLayoutConstraint!
    @IBOutlet weak var textField: CustomTextFields!
    @IBOutlet weak var textFieldIcon: UIImageView!
    @IBOutlet weak var imgeVOfAction: UIImageView!
    
    var view: UIView!
    
  
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    
  
    
    func xibSetup() {
     
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "TextFieldView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        self.setInitialViewRepresentation()
        
        return view
    }
    
    private func setInitialViewRepresentation(){
    
         self.textFieldIcon.contentMode = UIViewContentMode.ScaleAspectFit
         self.layer.cornerRadius = 5
        
         self.textField.makeStartingSpace(45)
        
        
    }
    
    func increasAreaOfIcon(){
    
        self.textFieldIcon.layer.cornerRadius = 4
        self.iconLeftSpaceCons.constant = 4
        self.iconImageWidthCons.constant = 27
        self.imageViewHeightCons.constant = 27
        self.textField.increaseFontSize()
        self.textFieldIcon.layoutIfNeeded()
        self.textField.makeStartingSpace(42)
        self.textFieldIcon.clipsToBounds = true
        
        
    }
    
    func setBorderColor(){
    
        self.layer.borderColor = ColorUtilities.borderColor.CGColor
        self.layer.borderWidth = 1
    }
 
    
    func setPlaceHolderOFField(text:String){
        
        self.textField.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSForegroundColorAttributeName: ColorUtilities.placeHolderColor])
    }
    
    func setTextFieldIconImage(image:UIImage?){
    
        self.textFieldIcon.image = image
    }
    
    func setTextFieldActionImage(image:UIImage?){
    
        self.imgeVOfAction.image = image
    }
    
    
    func setimageVTintColor(){
    
     self.textFieldIcon.image = textFieldIcon.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
       textFieldIcon.tintColor = ColorUtilities.buttonBackGroungBlueColor

    }

}
