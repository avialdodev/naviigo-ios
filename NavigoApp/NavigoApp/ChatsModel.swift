//
//  ChatsModel.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC on 11/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Swift

class ChatsModel: NSObject{
    dynamic var ChatId : String = ""
    dynamic var lastMsg : String = ""
    dynamic var lastMsgTimeStemp : String = ""
    dynamic var OtherUserName : String = ""
}