//
//  UserGuide.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 09/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Foundation

class UserGuide: BaseEntity {
    
    var iD:Int!
    var userId:Int!
    var firstName:String!
    var lastName:String!
    var dob:String!
    var address:String!
    var bio:String!
    var expertise:String!
    var country:String!
    var city:String!
    var lowestPrice:Int!
    var submerchantID:String!
    var createdAt:String!
    var updatedAt:String!
    var status:Int!
    var zipCode:String!
    var region:String!
    var rating:Int!
    
    
    
    override init(){
        
    }
    override func parseJson(response: NSDictionary?) {
        
        self.iD                 =        response?.valueForKey("id") as? Int                ?? 0
        self.userId             =        response?.valueForKey("userID") as? Int
        self.firstName          =        response?.valueForKey("firstName") as? String       ?? ""
        self.lastName           =        response?.valueForKey("lastName") as? String        ?? ""
        self.dob                =        response?.valueForKey("dob") as? String             ?? ""
        self.address            =        response?.valueForKey("address") as? String         ?? ""
        self.bio                =        response?.valueForKey("bio") as? String             ?? ""
        self.expertise          =        response?.valueForKey("expertise") as? String       ?? ""
        self.country            =        response?.valueForKey("country") as? String         ?? ""
        self.city               =        response?.valueForKey("city") as? String            ?? ""
        self.lowestPrice        =        response?.valueForKey("lowestPrice") as? Int        ?? 0
        self.submerchantID      =        response?.valueForKey("submerchantID") as? String   ?? ""
        self.createdAt          =        response?.valueForKey("createdAt") as? String       ?? ""
        self.updatedAt          =        response?.valueForKey("updatedAt") as? String       ?? ""
        self.status             =        response?.valueForKey("status") as? Int             ?? 0
        self.zipCode            =        response?.valueForKey("zip") as? String       ?? ""
        self.region             =        response?.valueForKey("region") as? String       ?? ""
        self.rating             =        response?.valueForKey("rating") as? Int       ?? 0
        
        
    }
}