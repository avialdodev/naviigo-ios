//
//  UpdateProfileService.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 17/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire

class UpdateProfileService {
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, imgUrl: String?) -> Void) {
        
        print(params)
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)/api/users/updateUser", parameters: params as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            if response.result.isSuccess{
                
                let statusResponse = Response()
                statusResponse.parseJson((response.result.value as! NSDictionary).valueForKey("response") as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                    
                    if let imageUrl = (response.result.value as! NSDictionary).valueForKey("imageURL") as? String{
                        
                        callback(errorMessage: nil, message: statusResponse.developerMessage,imgUrl:imageUrl)

                    
                    }else{
                    
                         callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil,imgUrl:nil)
                    
                    }
                    
                    
                    
                }else{
                    
                    callback(errorMessage: statusResponse.developerMessage, message: nil,imgUrl:nil)
                    
                }
                
            }else{
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil,imgUrl:nil)
                
            }
            
            
        }
        
    }
    
}