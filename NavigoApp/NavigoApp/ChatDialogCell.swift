//
//  ChatDialogCell.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC on 11/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class ChatDialogCell: UITableViewCell {

    @IBOutlet weak var lastMsgTime: UILabel!
    @IBOutlet weak var lastMsgLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.userImageView.layer.cornerRadius = self.userImageView.frame.width * 0.5
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
