//
//  RegisterService.swift
//  NavigoApp
//
//  Created by Wahab Jawed on 21/01/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire

class RegisterService:BaseService{

    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: BaseEntity?) -> Void) {
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)api/users/register", parameters: params as! [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            
            print(params)
            print("response\(response)")
            
            if response.result.isSuccess {
                
                let statusResponse = Response()
                statusResponse.parseJson(response.result.value as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                    callback(errorMessage: nil, message: statusResponse.developerMessage, serviceResponse: nil)
                
                }else{
                    
                    callback(errorMessage: statusResponse.developerMessage, message: nil, serviceResponse: nil)
                
                }

            }else {
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message:nil, serviceResponse: nil)
            }
            
        }

        
    }

}