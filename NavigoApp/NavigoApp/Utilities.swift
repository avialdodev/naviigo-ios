//
//  Utilities.swift
//  CargoApp
//
//  Created by Avialdo on 20/04/2016.
//  Copyright © 2016 Avialdo. All rights reserved.
//

import Foundation

class Utilities {
    
    class var sharedInstance : Utilities{
        
        struct Static {
            
            static let instance = Utilities()
        }
        
        return Static.instance
        
    }
    
    
    
    var blurView = UIView()
    var indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
    var grayIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    
    var clearColorView = UIView()
    
    static func getToastStyle() -> CSToastStyle {
        
        let style = CSToastStyle(defaultStyle: ())
        style.messageFont = UIFont(name: "SourceSansPro-Regular", size: 12)
        style.messageAlignment = NSTextAlignment.Center
        style.backgroundColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
        
        style.cornerRadius = 3
        style.horizontalPadding = 7
        style.verticalPadding = 5
        
        return style
    }
    
    static func makeToastOfString(string:String, onView view:UIView) {
        
        view.makeToast(string, duration: 2.0, position:  CSToastPositionBottom , style: Utilities.getToastStyle())
    }
    
    static func makeToastOfStringOnCenter(string:String, onView view:UIView) {
        
        view.makeToast(string, duration: 2.0, position:  CSToastPositionBottom , style: Utilities.getToastStyle())
    }
    
    static func showAlertwithTitle(title:String? , description:String?, parentViewController:UIViewController) -> Void {
        
        let alertController = UIAlertController(title: title, message: description, preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in
            
        }
        
        alertController.addAction(okAction)
        parentViewController.presentViewController(alertController, animated: true, completion: nil)
    }
    

    
      
    func disableScreen(view:UIView) {
        
        self.blurView.backgroundColor = UIColor.blackColor()
        self.blurView.alpha = 0.4
        self.blurView.hidden = true
        
        self.clearColorView.hidden = true
        self.clearColorView.backgroundColor = UIColor.clearColor()
        
        if let mainWindow = UIApplication.sharedApplication().keyWindow {
            
            self.blurView.frame = mainWindow.bounds
            self.indicator.center = mainWindow.center
            
            mainWindow.addSubview(self.blurView)
            mainWindow.addSubview(self.indicator)
            
        }
        else {
            
            self.blurView.frame = view.bounds
            self.indicator.center = view.center
            
            view.addSubview(self.blurView)
            view.addSubview(self.indicator)
            
        }
        
        self.indicator.startAnimating()
        self.blurView.hidden = false
        
    }
    
    func enableScreeen() {
        
        self.blurView.hidden = true
        self.blurView.removeFromSuperview()
        
        self.indicator.stopAnimating()
        self.indicator.removeFromSuperview()
        
    }
    
    static func isValidEmail(testStr:String) -> Bool
        
    {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluateWithObject(testStr)
        
    }
    
}
