//
//  TabsViewController.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/31/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class TabsViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.layer.borderWidth = 1
        self.tabBar.layer.borderColor = ColorUtilities.borderColor.CGColor
        self.tabBar.barTintColor = UIColor.whiteColor()
        self.tabBar.tintColor = ColorUtilities.buttonBackGroungBlueColor
        
        print(AppSingleton.sharedInstance.notificationCount)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
