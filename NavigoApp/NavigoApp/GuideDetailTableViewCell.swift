//
//  GuideDetailTableViewCell.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 25/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import STRatingControl

class GuideDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var userFeedback: UILabel!
    @IBOutlet weak var ratingView: STRatingControl!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imageViwG: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.imageViwG.layer.cornerRadius = 30
        self.imageView?.clipsToBounds = true
        // Configure the view for the selected state
    }

}
