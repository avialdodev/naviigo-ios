//
//  GetClientTokenService.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 17/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation

import Alamofire

class GetClientTokenService {
    
    func execute( callback: (errorMessage: String?, message: String?, token: String?) -> Void) {
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)/api/creditCard/clientToken", parameters: nil , encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            if response.result.isSuccess{
                
                print(response)
                
                let statusResponse = Response()
                statusResponse.parseJson(response.result.value!.valueForKey("response") as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                    let token = response.result.value!.valueForKey("ClientToken") as! String
                    callback(errorMessage: nil, message: statusResponse.developerMessage,token:token)
                    
                    
                }else{
                    
                    callback(errorMessage: statusResponse.developerMessage, message: nil,token:nil)
                    
                }
                
            }else{
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil,token:nil)
                
            }
            
            
        }
        
    }
    
}