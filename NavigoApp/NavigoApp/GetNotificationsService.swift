//
//  GetNotificationsService.swift
//  NavigoApp
//
//  Created by Muhammad Salman on 3/12/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//



import Foundation
import Alamofire

class GetNotificationsService {
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: [Notification]?) -> Void) {
        
            print(params["userID"]!)
        
            Alamofire.request(.GET, "\(Constant.SERVICE_MAIN_LINK)api/users/notifications?userID=\(params["userID"]!)", parameters: nil, encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
                
            print(response.result.isSuccess)
            print(response.result.value)
            
            if response.result.isSuccess{
                
                
                let statusResponse = Response()
                statusResponse.parseJson(response.result.value?.valueForKey(Constant.response) as! NSDictionary)
                
                if statusResponse.status == 1{
                    
                    var notifications:[Notification] = []
                    
                     let responseNotifications = response.result.value!.valueForKey("notificationList") as? NSArray
                        
                        for responseNotification in responseNotifications!{
                            
                            let notificationModel = Notification()
                            notificationModel.parseJSon((responseNotification as! NSDictionary))
                            
                            notifications.append(notificationModel)
                            
                        }
                    
                    callback(errorMessage: nil, message: Constant.successfullyDone, serviceResponse: notifications)
                    
                }else{
                    
                    callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
                    
                }
                
                
            }else{
                
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
            }
            
        }
        
    }
}