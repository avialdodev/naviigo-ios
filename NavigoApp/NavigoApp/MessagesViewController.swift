//
//  NotificationsViewController.swift
//  UniversityApp
//
//  Created by Saad Umar on 8/27/15.
//  Copyright (c) 2015 Saad Umar. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore
import FirebaseInstanceID
import SDWebImage

class MessagesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,GetNotificationCountDeleget{
    
    //Outlets
   // @IBOutlet weak var menuButton:UIBarButtonItem!
   
    @IBOutlet weak var upperTableconstrain: NSLayoutConstraint!
    @IBOutlet weak var interConnectedLable: UILabel!
    @IBOutlet weak var interConnectedView: UIView!
    @IBOutlet weak var messagesTableView: UITableView!
    var snapshotArray = [AnyObject]()
    var threadIDArray = [AnyObject]()

    var presentingAncevarr:UIViewController?
    
    var receiversID = [String]()
    var imagesDictionary:[String:UIImage] = [:]
    var noMessages = false
    
    
    var toFriendImage = UIImage(named: "user")
 //   @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    var indicator:UIActivityIndicatorView!
    var refreshControl:UIRefreshControl!
    
    var tempImagView = UIImageView()
    //View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // self.title = "Conversation"
        
        let connectedRef = FIRDatabase.database().referenceWithPath(".info/connected")
        connectedRef.observeEventType(.Value, withBlock: { snapshot in
            if let connected = snapshot.value as? Bool where connected {
                
                self.interConnectedView.backgroundColor = UIColor.greenColor()
                self.interConnectedLable.text           = "Connected"
                
            } else {
                
                self.interConnectedView.backgroundColor = UIColor.redColor()
                self.interConnectedLable.text           = "Not Connected"
            }
        })
        
        self.navigationController?.navigationBar.barTintColor = ColorUtilities.buttonBackGroungBlueColor
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        
        setupViews()
      //  setupReveal()
        
        indicator = UIActivityIndicatorView()
        
        
        self.indicator.frame.origin.x = self.view.frame.midX
        self.indicator.frame.origin.y = self.view.frame.midY
        
      //  self.view.addSubview(self.indicator)
        
      //  self.title = "Conversations"
        
        indicator.startAnimating()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Loading....")
        refreshControl.addTarget(self, action: #selector(MessagesViewController.refreshTableView), forControlEvents: UIControlEvents.ValueChanged)
        self.messagesTableView.addSubview(refreshControl)
        
        /* Extra */
        
        
        let myID = AppSingleton.sharedInstance.loginUserObj!.iD!
        let myRootRef = FIRDatabase.database().reference()
        
        Utilities.sharedInstance.disableScreen(self.view)
        
        print(FIRDatabase.database().reference())
        // fetching all the threads of a specific user
       self.indicator.startAnimating()
        
      NSTimer.scheduledTimerWithTimeInterval(7.0, target: self, selector: #selector(MessagesViewController.stopIndicator), userInfo: nil, repeats: false)
        
        
        let threadsRef = myRootRef.child("threads").child("\(myID)")
        threadsRef.queryOrderedByKey().observeEventType(.ChildAdded, withBlock: { snapshot in
            
            
            print(snapshot.childrenCount)
            // Now we will have to get the metadata for these IDS
            let metaTagRef = myRootRef.child("conversations").child("\(myID)-\(snapshot.value!)").child("meta-data")
            //self.receiversID.append("\(snapshot.value)")
            
            
            metaTagRef.queryOrderedByKey().observeEventType(.Value, withBlock: { snapshot in
                
                self.snapshotArray.append(snapshot.value!)
                
                if(self.snapshotArray.count > 0)
                {
                    self.noMessages = false
                    for index in 0..<self.snapshotArray.count
                    {
                        //for jInd in index+1..< self.snapshotArray.count
                        if(index+1 < self.snapshotArray.count)
                        {
                            for jIndex in index+1..<self.snapshotArray.count
                            {
                                let firstString = self.snapshotArray[index].objectForKey("senderID")! as! String
                                
                                let secondString = self.snapshotArray[jIndex].objectForKey("senderID")! as! String
                                
                                if(firstString == secondString)
                                {
                                    self.snapshotArray.removeAtIndex(index)
                                }
                            }
                        }
                    }
                    
                }
                
                
//                    var found = false
//                    let myJobs = AtlasCranes.sharedinstance.myJobsArrays[0]
//                    
//                    for i in 0..<self.snapshotArray.count
//                    {
//                        found = false
//                        for k in 0..<myJobs.count
//                        {
//                            if(myJobs[k].jobEngineer.engineerId == self.snapshotArray[i].objectForKey("senderID")! as! String)
//                            {
//                                found = true
//                            }
//                        }
//                        
//                        if !found
//                        {
//                            self.snapshotArray.removeAtIndex(i)
//                        }
//                    }
//                }
                
            //    self.indicator.stopAnimating()
                self.messagesTableView.hidden = false
                self.refreshControl.endRefreshing()
                Utilities.sharedInstance.enableScreeen()
                self.messagesTableView.reloadData()
            }, withCancelBlock: { error in
             //   self.indicator.stopAnimating()
                
                Utilities.sharedInstance.enableScreeen()

                self.messagesTableView.hidden = false
                self.refreshControl.endRefreshing()
                self.messagesTableView.reloadData()
            })
            
            
        }, withCancelBlock: { error in
           // self.indicator.stopAnimating()
            Utilities.sharedInstance.enableScreeen()

            self.messagesTableView.hidden = false
            self.refreshControl.endRefreshing()
            self.messagesTableView.reloadData()
            
    })
          /* Extra */
 
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = false
        
        AppSingleton.sharedInstance.getNotificationCountDeleget = self
        
        let getNotificationS = GetNotificationCount()
        getNotificationS.execute(AppSingleton.sharedInstance.loginUserObj!.iD)
      
    }
    override func viewWillDisappear(animated: Bool) {
        
        //AppSingleton.sharedInstance.getNotificationCountDeleget = nil
    }
    //MARK:-
    //MARK: Setting up Views
    func setupViews(){
//        self.navigationController?.navigationBar.barTintColor = UIColor(red: 87/255.0, green: 185/255.0, blue: 72/255.0, alpha: 1.0 )
//        
       self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
//    func setupReveal() {
//        if self.revealViewController() != nil {
//            menuButton.target = self.revealViewController()
//            menuButton.action = "revealToggle:"
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//        }
//    }
    
    //MARK:-                      
    //MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let indexPath : NSIndexPath = self.messagesTableView.indexPathForSelectedRow!
        //NSIndexPath *indexPath = (self.mytable).indexPathForSelectedRow;
        
        
        if let destinationVC = segue.destinationViewController as? ChatViewController{
            
            destinationVC.FriendID = snapshotArray[indexPath.row].objectForKey("senderID") as! String
            destinationVC.friendName = snapshotArray[indexPath.row].objectForKey("username") as! String
            destinationVC.friendPictureName = snapshotArray[indexPath.row].objectForKey("picture") as! String
            destinationVC.myPictureName     = AppSingleton.sharedInstance.loginUserObj!.imageURL
            
            print(snapshotArray[indexPath.row].objectForKey("picture") as! String)
            
            
        }
    }
    
    //MARK:-
    //MARK: Protocols
    
    //TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if snapshotArray.count == 0
        {
            noMessages = true
            return 1
        }
        return snapshotArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if noMessages {
            let cell = UITableViewCell()
            let label = UILabel(frame: CGRectMake((self.view.frame.width/2)-100, 10, 200, 30))
            label.font = UIFont(name: "AppleSDGothicNeo-Light", size: 20.0)
            label.text = "No Messages to show"
            label.textAlignment = NSTextAlignment.Center
            cell.addSubview(label)
            return cell
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MessageCell") as! MessageCell
        if(snapshotArray.count>0) {
            cell.titleLbl!.text = snapshotArray[indexPath.row].objectForKey("username") as? String
            cell.descriptionLbl!.text = snapshotArray[indexPath.row].objectForKey("last-message") as? String
            let pictureName : String = (snapshotArray[indexPath.row].objectForKey("picture") as? String)!
            let time_stamp = snapshotArray[indexPath.row].objectForKey("timeStamp")! as! NSNumber
            //let unread_counter = snapshotArray[indexPath.row].objectForKey("unread")! as! NSNumber
            
            //print ("unread counter: \(unread_counter)")
            print("timestamp: \(time_stamp)")
            
            let myDate = NSDate(timeIntervalSince1970: time_stamp.doubleValue/1000)
            
            print("date: \(myDate)")
            //print("timeAgo: \(myDate.timeAgoSimple)")
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.stringFromDate(myDate)
            
            cell.dateLbl.text = "\(date)"
            cell.profileImg.layer.cornerRadius = cell.profileImg.bounds.size.width/2
            cell.profileImg.clipsToBounds = true
            
           
       //     cell.pictureIndicator.startAnimating()
            let imageUrl = NSURL(string: "\(Constant.image_Link)\(pictureName).jpeg")
            
            
            print(imageUrl)
            
            cell.profileImg.sd_setImageWithURL(imageUrl, placeholderImage:  UIImage(named: "user"), options: SDWebImageOptions.CacheMemoryOnly ,completed:{(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) in
                
                if image != nil{
                
                    cell.profileImg.image = image!
                    
                }else{
                
                    cell.profileImg.image = UIImage(named: "user")
                }
                
                
            //    cell.pictureIndicator.stopAnimating()
            })

        }
        
        //cell.setContents(forIndex: indexPath.row, profileImage: nil, title: "123", description: "321", dateComponents: NSDateComponents())
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if !noMessages {
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(snapshotArray[indexPath.row].objectForKey("picture") as? String, forKey: "ReceiverImage")
            defaults.setObject(snapshotArray[indexPath.row].objectForKey("senderID") as! String , forKey: "receiverId")
       
            self.performSegueWithIdentifier("toChat", sender: self)
            
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func refreshTableView()
    {
     
         NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: #selector(MessagesViewController.stopLoading), userInfo: nil, repeats: false)
    }
    
    func stopLoading() {
        refreshControl.endRefreshing()
        
    }
    
    func stopIndicator() {
        self.messagesTableView.reloadData()
        self.indicator.stopAnimating()
        Utilities.sharedInstance.enableScreeen()
    }
    @IBAction func toNotification(sender: AnyObject) {
        
        self.goToNotificationScreen()
    }
    
    func setNotificationCount() {
        if AppSingleton.sharedInstance.notificationCount > 0 {
            
            self.navigationItem.leftBarButtonItem?.addBadge(number: AppSingleton.sharedInstance.notificationCount)
        }else{
            
            self.navigationItem.leftBarButtonItem?.removeBadge()
        }
        
    }
    
}
