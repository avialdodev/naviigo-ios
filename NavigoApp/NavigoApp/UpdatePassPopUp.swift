//
//  UpdatePassPopUp.swift
//  NavigoApp
//
//  Created by Salman Raza on 2/6/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class UpdatePassPopUp: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var currentPassFieldView: TextFieldView!
    @IBOutlet weak var newPassFieldView: TextFieldView!
    @IBOutlet weak var confirmPassFieldView: TextFieldView!
    
    @IBOutlet weak var updateButton: CustomButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var user:LoginUser?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.user = AppSingleton.sharedInstance.loginUserObj
        self.setInitialViewRepresentation()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.currentPassFieldView.textField.resignFirstResponder()
        self.newPassFieldView.textField.resignFirstResponder()
        self.confirmPassFieldView.textField.resignFirstResponder()
        self.view.viewMoveToDown()
    }
    

    private func setInitialViewRepresentation(){
    
        self.setCurrentPassView()
        self.setNewPassView()
        self.setConfirmPassView()
        self.updateButton.setBacColor(ColorUtilities.buttonBackGroungBlueColor)
    }
    
    private func setCurrentPassView(){
        
        self.currentPassFieldView.setTextFieldIconImage(ImageUtilities.ic_password!)
        self.currentPassFieldView.textField.secureTextEntry = true
        self.currentPassFieldView.setTextFieldActionImage(nil)
        self.currentPassFieldView.setPlaceHolderOFField("Current password")
        self.currentPassFieldView.setBorderColor()
        self.currentPassFieldView.textField.delegate = self
    }
    
    private func setNewPassView(){
        
        self.newPassFieldView.setTextFieldIconImage(ImageUtilities.ic_password!)
        self.newPassFieldView.textField.secureTextEntry = true
        self.newPassFieldView.setTextFieldActionImage(nil)
        self.newPassFieldView.setPlaceHolderOFField("New password")
        self.newPassFieldView.setBorderColor()
        self.newPassFieldView.textField.delegate = self
    
    }
    
    private func setConfirmPassView(){
        
        self.confirmPassFieldView.setTextFieldIconImage(ImageUtilities.ic_password!)
        self.confirmPassFieldView.textField.secureTextEntry = true
        self.confirmPassFieldView.setTextFieldActionImage(nil)
        self.confirmPassFieldView.setPlaceHolderOFField("Confirm password")
        self.confirmPassFieldView.setBorderColor()
        self.confirmPassFieldView.textField.delegate = self
    }
    @IBAction func updateAction(sender: AnyObject) {
        
        if self.newPassFieldView.textField.text != self.confirmPassFieldView.textField.text || self.newPassFieldView.textField.text?.characters.count < Constant.passwordLimitition || self.currentPassFieldView.textField.text?.characters.count < Constant.passwordLimitition{
            
            self.checkWhoseFieldIsEmpty()
        
        }else{
            
            print(self.user!.iD)
            Utilities.sharedInstance.disableScreen(self.view)
            let params:[String:AnyObject] = [Constant.userID:self.user!.iD,
                                             Constant.password:self.currentPassFieldView.textField.text!,
                                             Constant.newPassword:self.newPassFieldView.textField.text!]
            let updatePass = ChangePassService()
            
                updatePass.execute(params, callback: { (errorMessage, message, serviceResponse) in
                    
                    Utilities.sharedInstance.enableScreeen()
                    if errorMessage == nil {
                        
                        self.dismissViewControllerAnimated(true, completion: nil)
                        
                    }else{
                        
                        Utilities.makeToastOfString("\(errorMessage!)", onView:self.view)
                    }
                    
                    
                })
            

        }
    }
    
    private func checkWhoseFieldIsEmpty(){
    
      if   self.newPassFieldView.textField.text != self.confirmPassFieldView.textField.text{
        
        
        self.confirmPassFieldView.setTextFieldActionImage(ImageUtilities.warning)
        self.newPassFieldView.setTextFieldActionImage(ImageUtilities.warning)

        }
        
        if self.currentPassFieldView.textField.text?.characters.count < Constant.passwordLimitition{
        
            self.currentPassFieldView.setTextFieldActionImage(ImageUtilities.warning)
        }else{
            
            self.currentPassFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)

        
        }
        if self.newPassFieldView.textField.text?.characters.count < Constant.passwordLimitition{
            
            self.newPassFieldView.setTextFieldActionImage(ImageUtilities.warning)
        }else{
            
            self.newPassFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
            
            
        }
        if self.confirmPassFieldView.textField.text?.characters.count < Constant.passwordLimitition{
            
            self.confirmPassFieldView.setTextFieldActionImage(ImageUtilities.warning)
        }else{
            
            self.confirmPassFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
            
            
        }
        
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            
            return true
        }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        
       
            
            return newLength <= 35
            
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)

    }

}
