//
//  GuideDetailViewController.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 25/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import STRatingControl

class GuideDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,STRatingControlDelegate {

    @IBOutlet weak var guideExpertiesBorderView: UIView!
    @IBOutlet weak var guideExperties: UILabel!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var feedbackView: UIView!
    @IBOutlet weak var cityCountry: UILabel!
    @IBOutlet weak var bio: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var feedBackTableView: UITableView!
    @IBOutlet weak var ratingView: STRatingControl!
    
    
    var trmpImagView = UIImageView()
    var guid:Guide!
    
    var reviews:[Review] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.guideExpertiesBorderView.backgroundColor = UIColor.clearColor()
        self.guideExpertiesBorderView.layer.borderWidth = 1
        self.guideExpertiesBorderView.layer.borderColor = UIColor.whiteColor().CGColor
        
        let url = NSURL(string : "\(Constant.image_Link)\(self.guid.imageURL!).jpeg")
        
   //    let url = NSURL(string : "http://lorempixel.com/400/200")
        
        trmpImagView.sd_setImageWithURL(url, placeholderImage: UIImage(named: "user")) { (image, error, cache, url) in
            if image != nil{
                self.imageView.image = image!
            }else{
                self.imageView.image = UIImage(named: "user")
            }
        }
        feedbackView.layer.borderWidth = 1
        feedbackView.layer.borderColor = ColorUtilities.borderColor.CGColor
        self.title = "Guide"

        self.imageView.layer.cornerRadius = 40
        self.imageView.clipsToBounds = true
        
         let lBbItem = UIBarButtonItem(image: UIImage(named:"back-arrow1"), style: .Plain, target: self, action: #selector(GuideDetailViewController.goToBack))
        self.navigationItem.leftBarButtonItem = lBbItem
        
        self.setGuideData()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = true
        print(self.guid.iD)
        let params:[String:AnyObject] = ["guideID":self.guid.guideId]
        
        Utilities.sharedInstance.disableScreen(self.view)
        
        let getFeedBacks = GetFeedbackService()
            getFeedBacks.execute(params) { (errorMessage, message, serviceResponse) in
                
                
                Utilities.sharedInstance.enableScreeen()
                
                if errorMessage == nil{
                
                    self.reviews = serviceResponse!
                    self.feedBackTableView.reloadData()
                    
                    print(self.reviews)
                    
                    if self.reviews.count == 0{
                        
                        self.backImageView.image = UIImage(named:"no_feedback")
                        self.feedBackTableView.hidden = true
                        
                        
                    }else{
                        
                        self.backImageView.image = nil
                        
                        self.feedBackTableView.hidden = false
                        
                    }
                }else{
                
                    Utilities.showAlertwithTitle("Alert", description: errorMessage!, parentViewController: self)
                }
                
                
        }
        
    }
    
    func setGuideData(){
        self.bio.text = self.guid.bio
        self.name.text = self.guid.name
        self.bio.layer.cornerRadius = 8
        self.cityCountry.text = "\(self.guid.city), \(self.guid.country)"
        self.ratingView.rating = self.guid.rating
        self.guideExperties.text = self.guid.expertise
    }
    
    func goToBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func chatAction(sender: AnyObject) {
        let story = UIStoryboard(name: "AppFlow", bundle: nil)
        let chatvc = story.instantiateViewControllerWithIdentifier("ChatViewController") as! ChatViewController
        chatvc.FriendID = "\(self.guid.iD!)"
        chatvc.friendName = "\(self.guid.name)"
    
        chatvc.friendPictureName = "\(self.guid.imageURL)"
        chatvc.myPictureName     =  AppSingleton.sharedInstance.loginUserObj!.imageURL
        
        
        self.navigationController?.pushViewController(chatvc, animated: true)
    }

    @IBAction func callAction(sender: AnyObject) {
        
        print(self.guid.phone!)
        
        
        if let url = NSURL(string: "tel://\(self.guid.phone!)") where UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().openURL(url)
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.reviews.count
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Feedback") as! GuideDetailTableViewCell
        
        cell.name.text = self.reviews[indexPath.row].name
             let url = NSURL(string : "\(Constant.image_Link)\(self.reviews[indexPath.row].profileImage).jpeg")
        
        cell.imageViwG.layer.cornerRadius = cell.imageViwG.bounds.size.width/2
        cell.imageViwG.clipsToBounds = true
        cell.imageViwG.sd_setImageWithURL(url, placeholderImage: UIImage(named: "user")) { (image, error, cache, url) in
        cell.ratingView.rating = self.reviews[indexPath.row].point
            
            if image != nil{
                
                cell.imageViwG.image = image!
                
            }else{
                
                cell.imageViwG.image = UIImage(named: "user")
            }
        }
            cell.userFeedback.text = self.reviews[indexPath.row].feedback
        
        
        return cell
    }
}
