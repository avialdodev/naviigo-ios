//
//  Review.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 09/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation


class Review: BaseEntity {
    
    var id:Int!
    var reviewer:Int!
    var name:String!
    var profileImage:String!
    var reviewee:Int!
    var tripId:Int!
    var point:Int!
    var feedback:String!
    var createdAt:String!
    
    override func parseJson(dic: NSDictionary) {
        
        self.id             = dic.valueForKey("id") as! Int
        self.reviewer       = dic.valueForKey("reviewer") as! Int
        self.name           = dic.valueForKey("name") as! String
        self.profileImage   = dic.valueForKey("profileImg") as! String
        self.reviewee       = dic.valueForKey("reviewee") as! Int
        self.tripId         = dic.valueForKey("tripID") as! Int
        self.point          = dic.valueForKey("point") as! Int
        self.feedback       = dic.valueForKey("feedback") as! String
        self.createdAt      = dic.valueForKey("createdAt") as! String
        
    }
}