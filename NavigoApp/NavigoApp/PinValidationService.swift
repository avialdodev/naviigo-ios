//
//  PinValidationAfterSignInAndSignUpService.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/22/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire

class PinValidationService:BaseService {
    
    let userdefaults = NSUserDefaults()
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: BaseEntity?) -> Void) {
        
        
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)api/users/validate", parameters: params as! [String : AnyObject], encoding: .URL, headers: nil).responseJSON { (response) -> Void in
            
            
       
            
            if response.result.isSuccess{
                
                let responseService = Response()
                responseService.parseJson( response.result.value?.valueForKey("response") as! NSDictionary)
                
                if responseService.status == 1{
                    
                    
                    let pinValidationUser = PinValidationResponse()
                    pinValidationUser.parseJson(response.result.value as! NSDictionary)
                    
                    AppSingleton.sharedInstance.loginUserObj = pinValidationUser as LoginUser

                    if let userSessionString = AppSingleton.sharedInstance.dicToString(response.result.value as! NSDictionary){
                        
                        
                        self.userdefaults.setValue(userSessionString, forKey: "UserSessionJson")
                        
                    }
                    
                    callback(errorMessage:nil , message: responseService.developerMessage,serviceResponse: pinValidationUser)

                    
                }else{
                    
                    callback(errorMessage: responseService.developerMessage, message: nil,serviceResponse: nil)
                }
                
                
            }else {
                
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil,serviceResponse: nil)
            }
            
        }

        
        
    }
}