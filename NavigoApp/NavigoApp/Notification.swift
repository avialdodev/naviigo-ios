//
//  Notification.swift
//  NavigoApp
//
//  Created by Muhammad Salman on 3/12/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation


class Notification{

    var id:Int!
    var userId:Int!
    var createdAt:String!
    var title:String!
    var description:String!
    var isRead:Int!
    var type:Int!
    var tripId:String!
    
    init(){
    
    }
    
    func parseJSon(response: NSDictionary){
        
        self.id             = response.valueForKey("id") as! Int
        self.userId         = response.valueForKey("userID") as! Int
        self.createdAt      = (response.valueForKey("createdAt") as? String) ?? ""
        self.title          = (response.valueForKey("title") as? String) ?? ""
        self.description    = (response.valueForKey("description") as? String) ?? ""
        self.isRead         = response.valueForKey("isRead") as! Int
        self.type           = response.valueForKey("type") as! Int
        
        print(response.valueForKey("data"))
        self.tripId           = response.valueForKey("data") as? String ?? "0"


        
    
    }

}