//
//  TransactionViewController.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 09/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import SDWebImage

class TransactionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,GetNotificationCountDeleget  {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var belowMyRequestV: UIView!
    @IBOutlet weak var belowGuideRequestView: UIView!
    
    @IBOutlet weak var notificationButton: UIBarButtonItem!
    
    @IBOutlet weak var backImagev: UIImageView!
    
    var isFirstTime:Bool = true
   
    
    var isMyRequest:Bool = true
    
    var myTripRequest:[Trip]    = []
    var guideTripRequest:[Trip] = []
    
    var myTripServiceLink:String!
    var guideTripServiceLink:String!
    
    var toPaymentRT:Trip!
    
    var user:LoginUser!
    
    let dateFormatter = NSDateFormatter()
    let calendar = NSCalendar.currentCalendar()
    
    var imagview = UIImageView()
    
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.user = AppSingleton.sharedInstance.loginUserObj!
        
        self.dateFormatter.dateFormat = "yyyy-MM-dd h:mm a"

        self.setInitialViewRepresentation()
        self.setServiceLink()
        self.setRefreshController()
 
        
    }
    
    
    private func setServiceLink(){
        
        if self.user.isGuide == 1 {
            
            print()
            
            self.myTripServiceLink    = "\(Constant.SERVICE_MAIN_LINK)api/trip/getMyTripReq?userID=\(self.user.asGuide!.iD)"
            
        }else{
            
            self.myTripServiceLink    = "\(Constant.SERVICE_MAIN_LINK)api/trip/getMyTripReq?userID=0"
        }
        //
        
        
        
        self.guideTripServiceLink = "\(Constant.SERVICE_MAIN_LINK)api/trip/getGuideTripReq?userID=\(self.user.iD)"
        
    
    }
    
    private func setRefreshController(){
        

        
        refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        tableview.backgroundView = refreshControl
       
    
    }
    
    func refresh(_ refreshControl: UIRefreshControl) {
        
        self.callServices()
      
    }
    
    private func setInitialViewRepresentation(){
        

    
        self.tableview.tableFooterView = UIView()

        
        
        
        self.belowMyRequestV.backgroundColor = ColorUtilities.buttonBackGroungBlueColor
        
        self.navigationController?.navigationBar.barTintColor = ColorUtilities.buttonBackGroungBlueColor
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        AppSingleton.sharedInstance.getNotificationCountDeleget = self
        
        let getNotificationS = GetNotificationCount()
        getNotificationS.execute(AppSingleton.sharedInstance.loginUserObj!.iD)
        
        self.tabBarController?.tabBar.hidden = false
        self.callServices()
        

        print(self.isMyRequest)
        
      
    }
    
    override func viewWillDisappear(animated: Bool) {
        
    //    AppSingleton.sharedInstance.getNotificationCountDeleget = nil
    }
    
    private func callServices(){
    
        Utilities.sharedInstance.disableScreen(self.view)
        
        let getTripService = GetTripRequestService()
        getTripService.execute(["serviceLink":myTripServiceLink]) { (errorMessage, message, serviceResponse) in
            
            if errorMessage == nil {
                
                
                self.myTripRequest = serviceResponse!
                
                
                
                let getTripServiceForGuide = GetTripRequestService()
                getTripServiceForGuide.execute(["serviceLink":self.guideTripServiceLink]) { (errorMessage, message, serviceResponse) in
                    
                    Utilities.sharedInstance.enableScreeen()
                    self.refreshControl.endRefreshing()
                    
                    if errorMessage == nil {
                        
                        print(serviceResponse!)
                        
                        self.guideTripRequest = serviceResponse!
                        
                        
                    }else{
                        
                        Utilities.showAlertwithTitle("", description: errorMessage!, parentViewController: self)
                        
                    }
                    
                    self.checkAndSetNoDataImage()
                    
                    self.tableview.reloadData()
                    
                    
                }
                
            }else{
                
                Utilities.sharedInstance.enableScreeen()
                self.refreshControl.endRefreshing()
                
                Utilities.showAlertwithTitle("", description: errorMessage!, parentViewController: self)
                
            }
            
            
            
            
        }
        
        
        

    }
    
    private func checkAndSetNoDataImage(){
    
        
        if self.isMyRequest{
            
            if self.myTripRequest.count == 0{
                
                self.isShowNoDataImage(true)
                
            }else{
                
                self.isShowNoDataImage(false)
                
            }
            
        }else{
            
            
            if self.guideTripRequest.count == 0{
                
                self.isShowNoDataImage(true)
                
            }else{
                
                self.isShowNoDataImage(false)
                
            }
        }

    
    }
    
    @IBAction func myRequestAction(sender: AnyObject) {
        
        self.belowGuideRequestView.backgroundColor = UIColor.clearColor()
        self.belowMyRequestV.backgroundColor = ColorUtilities.buttonBackGroungBlueColor
        
         self.isMyRequest = true
       self.checkAndSetNoDataImage()
        
       

        self.tableview.reloadData()
    }
    @IBAction func guideRequestAction(sender: AnyObject) {

        
        self.belowGuideRequestView.backgroundColor = ColorUtilities.buttonBackGroungBlueColor
        self.belowMyRequestV.backgroundColor = UIColor.clearColor()
        self.isMyRequest = false
       self.checkAndSetNoDataImage()
        
        
        self.tableview.reloadData()
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        
        
        if isMyRequest{
            
            self.toPaymentRT = self.myTripRequest[indexPath.row]
            self.goToPaymentRequest(true)

        
        }else{
            
            self.toPaymentRT = self.guideTripRequest[indexPath.row]
                
            self.goToPaymentRequest(false)
            
        
        }
        
        tableview.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    private func goToPaymentRequest(isGuideGo:Bool){
    
                let story = UIStoryboard(name: "AppFlow", bundle: nil)
                let vc    = story.instantiateViewControllerWithIdentifier("PaymentRequestVC") as! PaymentRequestVC
        
                    vc.isGuide = isGuideGo
                    vc.trip = self.toPaymentRT!
                    
        
        
              self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        var returnCount:Int = 0
        
        if isMyRequest{
            
            returnCount = self.myTripRequest.count
  
        }else{
            
            returnCount = self.guideTripRequest.count
        
        }
        
//        if self.isFirstTime{
//        
//            self.isFirstTime = false
//            
//        }else{
//        
//            if returnCount == 0{
//            
//                self.isShowNoDataImage(true)
//                
//            }else{
//            
//                self.isShowNoDataImage(false)
//            
//            }
//        }
        
        return returnCount
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 95
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("TransactionTableViewCell") as! TransactionTableViewCell
        if isMyRequest{
            self.setCellAccorndiToRole(cell, indexPath: indexPath, dic: self.myTripRequest)
        }else{
            self.setCellAccorndiToRole(cell, indexPath: indexPath, dic: self.guideTripRequest)
        }
        return cell
    }
    
    private func setCellAccorndiToRole(cell:TransactionTableViewCell,indexPath:NSIndexPath,dic:[Trip]){
    
        print("\(indexPath.row)=\(dic[indexPath.row].id)")
        cell.amount.text = "$\(dic[indexPath.row].amount)"
        cell.date.text   = dic[indexPath.row].createdAt
        cell.name.text   = dic[indexPath.row].name
        
        
        let startDate = self.dateFormatter.dateFromString(dic[indexPath.row].startDate)
        let endDate   = self.dateFormatter.dateFromString(dic[indexPath.row].endDate)
        
        let date1 = calendar.startOfDayForDate(startDate!)
        let date2 = calendar.startOfDayForDate(endDate!)
        
        let flags = NSCalendarUnit.Day
        let components = calendar.components(flags, fromDate: date1, toDate: date2, options: [])
        
        cell.days.text = "\(components.day)"
        
      
        
        
        
        if dic[indexPath.row].status == 0{
            
            cell.status.text = "Pending"
            cell.status.backgroundColor = ColorUtilities.status_Pending_Color
            
            
        }else if  dic[indexPath.row].status == 1{
            
            cell.status.text = "Accepted"
            cell.status.backgroundColor = ColorUtilities.status_Accept_Color
            
        }else if  dic[indexPath.row].status == 2{
            
            cell.status.text = "Declined"
            cell.status.backgroundColor = ColorUtilities.status_Cancel_Color
            
        }else if  dic[indexPath.row].status == 3{
            
            cell.status.text = "Completed"
            cell.status.backgroundColor = ColorUtilities.status_Complete_Color
            
        }else if  dic[indexPath.row].status == 4{
            
            cell.status.text = "Cancelled"
            cell.status.backgroundColor = ColorUtilities.status_Cancel_Color
            
        }
        
         let url = NSURL(string : "\(Constant.image_Link)\(dic[indexPath.row].imageName!).jpeg")
        
       // let url = NSURL(string : "http://lorempixel.com/400/200")
        
       // cell.imageV.setImageWithURL(url, placeholderImage: <#T##UIImage!#>)
        
        cell.imageV.sd_setImageWithURL(url, placeholderImage: UIImage(named: "user")) { (image, error, cache, url) in
            
            
            if image != nil{
                
                cell.imageV.image = image!
                
            }else{
                
                cell.imageV.image = UIImage(named: "user")
            }
        }
        

    }
    
    func isShowNoDataImage(bool:Bool){
    
        if bool{
        
            self.tableview.hidden = true
            self.backImagev.hidden = false
            self.backImagev.image = UIImage(named: "no_transaction")
            self.view.bringSubviewToFront(self.backImagev)
            
        }else{
        
            self.backImagev.image = nil
            self.tableview.hidden = false
            self.backImagev.hidden = true
        
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation

    
        

    func setNotificationCount() {
        if AppSingleton.sharedInstance.notificationCount > 0 {
            
            self.navigationItem.leftBarButtonItem?.addBadge(number: AppSingleton.sharedInstance.notificationCount)
        }else{
            
            self.navigationItem.leftBarButtonItem?.removeBadge()
        }
        
    }
 
    @IBAction func toNotificationScreen(sender: AnyObject) {
        
        self.goToNotificationScreen()
    }

}
