//
//  LoginUser.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/18/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
class LoginUser:User {
      
    var creditCardObj:CreditCard?

    
   override func parseJson(response:NSDictionary) {
            super.parseJson(response)
             creditCardObj = CreditCard()
                creditCardObj?.parseJson(response.valueForKey("creditCardObj") as? NSDictionary)
        
        
    }
    
    
}