//
//  GetNotificationCount.swift
//  NavigoApp
//
//  Created by Muhammad Salman on 4/1/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//


import Foundation
import Alamofire

class GetNotificationCount {
    
    func execute(userID:Int) {
        
        
        Alamofire.request(.GET, "\(Constant.SERVICE_MAIN_LINK)api/users/getNotificationCount/\(userID)", parameters: nil, encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            if response.result.isSuccess{
                
                
                AppSingleton.sharedInstance.notificationCount = response.result.value as! Int
                
                AppSingleton.sharedInstance.getNotificationCountDeleget?.setNotificationCount()
            }
                
            
            
        }
    
    }
}
