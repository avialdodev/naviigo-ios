//
//  BecomeAGuideVC.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/31/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import HTagView
import GooglePlaces

class BecomeAGuideVC: UIViewController,UITextFieldDelegate,HTagViewDataSource,HTagViewDelegate,GMSAutocompleteViewControllerDelegate,UITextViewDelegate {
    

    
    @IBOutlet weak var adressForArrowImagV: UIImageView!
    @IBOutlet var tagView: HTagView!
  
    @IBOutlet weak var addLocationB: CustomButton!
    
    let userdefaults = NSUserDefaults()
    
//    @IBOutlet weak var topSpaceViewHeight: NSLayoutConstraint!

    @IBOutlet weak var countryImagV: UIImageView!
    @IBOutlet weak var cityImageV: UIImageView!
    @IBOutlet weak var regionFieldView: TextFieldView!
    @IBOutlet weak var zipCodeFieldView: TextFieldView!
    @IBOutlet weak var lowestPrizeFieldView: TextFieldView!
    @IBOutlet weak var doBFieldView: TextFieldView!
    @IBOutlet weak var emailFieldview: TextFieldView!
    @IBOutlet weak var transportationAndAreaView: UIView!
    @IBOutlet weak var aboutMe: UITextView!
    @IBOutlet weak var areaExpertiesView: TextFieldView!
   // @IBOutlet weak var lastNameView: TextFieldView!
    @IBOutlet weak var firstNameView: TextFieldView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var country: UILabel!
    
    
    var user:LoginUser?
    
    var tags:[String] = []
    
    var cityName:String? = nil
    var countryName:String? = nil
    var experties = ""
    
    var address:String = ""
    var tempDateOfBirth:String? = nil
    var selectedDateOfBirth:String = ""
    
    let dateFormatter = NSDateFormatter()
    
    
    let errorImage = UIImage(named: "warning")
    let textBox_Tick = UIImage(named: "textbox_tick")
    
    var currentWindow = UIApplication.sharedApplication().keyWindow
    var backView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backView.frame = self.view.frame
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        
        user = AppSingleton.sharedInstance.loginUserObj!
        
        if user!.isGuide == 1{
            
            self.setPreviousValue()
            self.title = "Update Guide Profile"
            
        }else{
        
            self.title = "Become a Guide"
        }
       
        
//        let rightBarButton = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.action, target: nil, action: <#T##Selector#>)
      

        self.setFirstNameView()
       // self.setLastNameView()
        self.setAreaexpertiesView()
        self.setAboutMeView()
        self.setEmailFieldView()
        self.setLowestPrizeFieldV()
        self.setDobFieldView()
        self.setRegionFieldV()
        self.setZipCodeFieldV()
        
        self.city.backgroundColor = ColorUtilities.taxtFBackColor
        self.country.backgroundColor = ColorUtilities.taxtFBackColor
        self.addLocationB.backgroundColor = ColorUtilities.taxtFBackColor
        self.addLocationB.layer.borderWidth = 1
        self.addLocationB.layer.borderColor = ColorUtilities.borderColor.CGColor
        
        self.tagView.backgroundColor = ColorUtilities.taxtFBackColor
        
        self.city.layer.borderWidth = 1
        self.city.layer.borderColor = ColorUtilities.borderColor.CGColor
        self.city.layer.cornerRadius = 4
        
        
        self.country.layer.borderWidth = 1
        self.country.layer.borderColor = ColorUtilities.borderColor.CGColor
        self.country.layer.cornerRadius = 4
        
        

      //  self.setSpaceAccordingToScreen()
            self.transportationAndAreaView.layer.cornerRadius = 5
        self.transportationAndAreaView.layer.borderWidth = 1
        self.transportationAndAreaView.layer.borderColor = ColorUtilities.borderColor.CGColor
        
        setTagView()
        
        self.cityImageV.image = cityImageV.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        cityImageV.tintColor = ColorUtilities.buttonBackGroungBlueColor
        
        self.adressForArrowImagV.image = adressForArrowImagV.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        adressForArrowImagV.tintColor = ColorUtilities.buttonBackGroungBlueColor
        
        
        self.countryImagV.image = countryImagV.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        countryImagV.tintColor = ColorUtilities.buttonBackGroungBlueColor
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = true
          }
    
    
    private func setPreviousValue(){
    
        
      
            
            self.aboutMe.text                   = self.user!.asGuide!.bio
            self.city.text = "         \(self.user!.asGuide!.city)"
            self.country.text = "         \(self.user!.asGuide!.country)"
        
        
            self.cityName = self.user!.asGuide!.city
            self.countryName = self.user!.asGuide!.country
            self.firstNameView.textField.text = "\(self.user!.asGuide!.firstName!) \(self.user!.asGuide!.lastName!)"
         //   self.lastNameView.textField.text  = self.user!.asGuide!.lastName!
            self.lowestPrizeFieldView.textField.text = "\(self.user!.asGuide!.lowestPrice!)"
            self.doBFieldView.textField.text         =   self.user?.asGuide?.dob!
            
            self.city.textColor = UIColor.blackColor()
            self.country.textColor = UIColor.blackColor()
            
            // "  \(self.user!.asGuide!.city),\(self.user!.asGuide!.country)"
            self.cityName = self.user!.asGuide!.city
            self.countryName = self.user!.asGuide!.country
            self.address = (self.user!.asGuide!.address
                )!
            self.selectedDateOfBirth = self.user!.asGuide!.dob
            self.zipCodeFieldView.textField.text = self.user!.asGuide!.zipCode!
            self.regionFieldView.textField.text    = self.user!.asGuide!.region
            
            var guideExperties = self.user!.asGuide!.expertise
        
        if guideExperties != ""{
            
            self.tags   = guideExperties!.componentsSeparatedByString(",")
            
            self.tagView.reloadData()
        
        }
        
       
        
        
        if self.user!.asGuide!.address! != ""{
            
              self.addLocationB.setTitle("  \(self.user!.asGuide!.address!)", forState: UIControlState.Normal)
        
        }else{
            
            self.addLocationB.setTitle("   Add Street Address", forState: UIControlState.Normal)
        
        }
        
        
            
       
        

    }
    
    private func setTagView(){
    
    
        tagView.delegate = self
        tagView.dataSource = self
        tagView.marg = 8
        tagView.btwTags = 8
        tagView.btwLines =  8
        tagView.fontSize = 12

        tagView.tagMainBackColor = ColorUtilities.buttonBackGroungBlueColor
        tagView.tagSecondBackColor = ColorUtilities.buttonBackGroungBlueColor
        tagView.tagSecondTextColor = UIColor.darkTextColor()
        tagView.tagContentEdgeInsets = UIEdgeInsets(top: 5, left: 12, bottom: 5, right: 12)
        
    
        
    }
    
    
    private func setEmailFieldView(){
        
        self.emailFieldview.setTextFieldIconImage(ImageUtilities.whiteEmail)
        self.emailFieldview.increasAreaOfIcon()
        self.emailFieldview.setimageVTintColor()
        self.emailFieldview.setPlaceHolderOFField("Email")
        self.emailFieldview.textField.keyboardType = UIKeyboardType.EmailAddress
        self.emailFieldview.setTextFieldActionImage(nil)
        self.emailFieldview.setBorderColor()
        self.emailFieldview.textField.userInteractionEnabled = false
        self.emailFieldview.textField.text = "\(self.user!.email!)"
        self.emailFieldview.textField.delegate = self
        self.emailFieldview.textField.backgroundColor = ColorUtilities.taxtFBackColor
    
    }
    
    private func setDobFieldView(){
        
        self.doBFieldView.setPlaceHolderOFField("DOB")
        self.doBFieldView.setTextFieldIconImage(ImageUtilities.date)
        self.doBFieldView.setimageVTintColor()
        self.doBFieldView.increasAreaOfIcon()
        self.doBFieldView.setTextFieldActionImage(nil)
        self.doBFieldView.setBorderColor()
        self.doBFieldView.textField.delegate = self
        self.doBFieldView.textField.backgroundColor = ColorUtilities.taxtFBackColor
    }
    
    private func setLowestPrizeFieldV(){
        
        self.lowestPrizeFieldView.setTextFieldIconImage(ImageUtilities.usDollor)
        self.lowestPrizeFieldView.increasAreaOfIcon()
        self.lowestPrizeFieldView.setPlaceHolderOFField("Avg Price")
        self.lowestPrizeFieldView.setimageVTintColor()
        self.lowestPrizeFieldView.textField.keyboardType = UIKeyboardType.NumberPad
        self.lowestPrizeFieldView.setTextFieldActionImage(nil)
        self.lowestPrizeFieldView.setBorderColor()
        self.lowestPrizeFieldView.textField.delegate = self
        self.addReturnButtonOnKeyboard(self.lowestPrizeFieldView.textField)
          self.lowestPrizeFieldView.textField.backgroundColor = ColorUtilities.taxtFBackColor
    
    }
    private func setZipCodeFieldV(){
        
       // self.zipCodeFieldView.textField.makeStartingSpace(12)
        self.zipCodeFieldView.setTextFieldIconImage(ImageUtilities.blueLocation)
        self.zipCodeFieldView.increasAreaOfIcon()
        self.zipCodeFieldView.setimageVTintColor()
        self.zipCodeFieldView.setPlaceHolderOFField("Zip Code")
        self.zipCodeFieldView.setTextFieldActionImage(nil)
        self.zipCodeFieldView.setBorderColor()
        self.zipCodeFieldView.textField.delegate = self
        self.zipCodeFieldView.textField.keyboardType = UIKeyboardType.NumberPad
        self.addReturnButtonOnKeyboard(self.zipCodeFieldView.textField)
        self.zipCodeFieldView.textField.delegate = self
          self.zipCodeFieldView.textField.backgroundColor = ColorUtilities.taxtFBackColor
    
    }
    
    private func setRegionFieldV(){
    
        self.regionFieldView.setTextFieldIconImage(ImageUtilities.world)
        self.regionFieldView.increasAreaOfIcon()
        self.regionFieldView.setPlaceHolderOFField("State/Region")
        self.regionFieldView.setimageVTintColor()
        self.regionFieldView.setTextFieldActionImage(nil)
        self.regionFieldView.setBorderColor()
        self.regionFieldView.textField.delegate = self
          self.regionFieldView.textField.backgroundColor = ColorUtilities.taxtFBackColor
    
    }
    
    
    private func setFirstNameView(){
        self.firstNameView.setPlaceHolderOFField("Full Name")
        self.firstNameView.setTextFieldIconImage(ImageUtilities.fullName)
        self.firstNameView.textField.autocapitalizationType = .Words
        self.firstNameView.setBorderColor()
        self.firstNameView.textField.delegate = self
          self.firstNameView.textField.backgroundColor = ColorUtilities.taxtFBackColor
        self.firstNameView.setimageVTintColor()
        self.firstNameView.increasAreaOfIcon()
        
    
    }
    

    private func setAreaexpertiesView(){
        self.areaExpertiesView.setPlaceHolderOFField("Areas of expertise")
        self.areaExpertiesView.setTextFieldIconImage(nil)
        self.areaExpertiesView.textField.makeStartingSpace(12)
        self.areaExpertiesView.setTextFieldActionImage(nil)
        self.areaExpertiesView.textField.delegate = self
        self.areaExpertiesView.textField.makeViewBorder()
        self.areaExpertiesView.textField.backgroundColor = ColorUtilities.taxtFBackColor
    }
 
    
    private func setAboutMeView(){
        self.aboutMe.layer.cornerRadius = 5
        self.aboutMe.layer.borderWidth = 1
        self.aboutMe.layer.borderColor = ColorUtilities.borderColor.CGColor
        self.aboutMe.delegate = self
        self.aboutMe.backgroundColor = ColorUtilities.taxtFBackColor
        
    
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        resignResponder()
            }
    
    private func resignResponder(){
    
        self.firstNameView.textField.resignFirstResponder()
     //   self.lastNameView.textField.resignFirstResponder()
      //  self.cityCountryView.textField.resignFirstResponder()
        self.areaExpertiesView.textField.resignFirstResponder()
        self.aboutMe.resignFirstResponder()
        self.zipCodeFieldView.textField.resignFirstResponder()
        self.regionFieldView.textField.resignFirstResponder()
        self.emailFieldview.textField.resignFirstResponder()
        
        self.view.viewMoveToDown()

    
    }
    

    
    @IBAction func backAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == self.areaExpertiesView.textField{
        
            self.view.viewMoveToUp()
            
        }else if textField == self.doBFieldView.textField{
        
                    dateOfBirthAction(textField)
        }else if textField == self.zipCodeFieldView.textField {
            
            self.view.viewMoveToUp()
        
        
        }else if textField == self.regionFieldView.textField{
        
             self.view.viewMoveToUp()
        }
        
        
    }
    

//    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
//        

//        
//        return true
//    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            
            return true
        }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        if textField == lowestPrizeFieldView.textField{
            
            return newLength <= 4
            
        }else if textField == self.zipCodeFieldView.textField{
        
            return newLength <= 5
        }else if textField == self.regionFieldView.textField {
        
            return newLength <= 2
        }else if textField == self.firstNameView.textField{
        
            return newLength <= 50
        }else if textField == areaExpertiesView.textField{
        
            return newLength <= 12
        
        }
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        
        self.view.viewMoveToDown()
        self.resignResponder()
        
        if textField == self.areaExpertiesView.textField{
            
            if textField.text != ""{
                if self.tags.count < 3{
                    
                    if (textField.text!.characters.contains(",")){
                        
                        var comasTags = textField.text!.componentsSeparatedByString(",")
                        var filterComasTags:[String] = []
                        
                        for tag in comasTags{
                        
                            if tag != ""{
                            
                                filterComasTags.append(tag)
                            }
                        }
                        
                        
                        if (self.tags.count + filterComasTags.count) <= 3{
                            
                            for tag in filterComasTags{
                                self.tags.append(tag)
                            }
                            tagView.reloadData()
                        }else{
                            Utilities.showAlertwithTitle("Alert", description: "Maximum Tags are Three", parentViewController: self)
                        }
                    }else{
                        self.tags.append(textField.text!)
                        tagView.reloadData()
                    }

                    
                }else{
                    
                    Utilities.showAlertwithTitle("Alert", description: "Maximum Tags are Three", parentViewController: self)
                    
                }
                
            }
            textField.text = nil

        
        }
        
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
       
        if(text == "\n") {
            aboutMe.resignFirstResponder()
            self.view.viewMoveToDown()
            return false
        }
        
        let newText = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        return numberOfChars <= 200
            

    }
    

    
    func numberOfTags(tagView: HTagView) -> Int {
        
        return tags.count
    }
    func tagView(tagView: HTagView, titleOfTagAtIndex index: Int) -> String {
        
        return tags[index]
    }
    
    func tagView(tagView: HTagView, tagTypeAtIndex index: Int) -> HTagType {
        return .Cancel
    }
    

    
    func tagView(tagView: HTagView, tagSelectionDidChange selectedIndices: [Int]) {
        
        
    }
    
    func tagView(tagView: HTagView, didCancelTagAtIndex index: Int) {
        
               tags.removeAtIndex(index)
              tagView.reloadData()
        
    }
    
    
    private func dateOfBirthAction(sender:UITextField){

        let datePickerView = UIDatePicker()
        
        let datePickerToolBar = UIToolbar()
        let doneButtonDate = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(BecomeAGuideVC.departureDonePickerDate))
        
        let cancelButtonDate = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(BecomeAGuideVC.departureCancelPickerDate))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        
        datePickerToolBar.barStyle = UIBarStyle.Default
        //datePickerToolBar.translucent = true
        datePickerToolBar.backgroundColor = UIColor.whiteColor()
        datePickerToolBar.tintColor = UIColor(red: 0/255, green: 140/255, blue: 220/255, alpha: 1)
        datePickerToolBar.sizeToFit()
        datePickerToolBar.setItems([cancelButtonDate,spaceButton,doneButtonDate], animated: false)
        datePickerToolBar.userInteractionEnabled = true
        
        
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView .addTarget(self, action: #selector(BecomeAGuideVC.departureDatePickerValueChanged), forControlEvents: UIControlEvents.ValueChanged)
        
        
        
        sender.inputView = datePickerView
        sender.inputAccessoryView = datePickerToolBar
        
        self.tempDateOfBirth = dateFormatter.stringFromDate(datePickerView.date)
        self.doBFieldView.textField.text = self.tempDateOfBirth
        
        currentWindow?.addSubview(self.backView)
    
    }
    
    
    func departureDatePickerValueChanged(sender:UIDatePicker) {
        
        let date = dateFormatter.stringFromDate(sender.date)
        self.tempDateOfBirth = date
        self.doBFieldView.textField.text = self.tempDateOfBirth!
        
    }
    
    func departureDonePickerDate(){
        
        if self.tempDateOfBirth != nil{
        
            self.selectedDateOfBirth = self.tempDateOfBirth!
            self.doBFieldView.textField.text = self.selectedDateOfBirth
        
        }
        
     self.doBFieldView.textField.resignFirstResponder()
     self.backView.removeFromSuperview()
        
    }
    
    func departureCancelPickerDate(){

        self.doBFieldView.textField.text = self.selectedDateOfBirth
        self.doBFieldView.textField.resignFirstResponder()
         self.backView.removeFromSuperview()
        
    }

    
    func viewController(viewController: GMSAutocompleteViewController, didAutocompleteWithPlace place: GMSPlace) {
        
        
        self.cityName = nil
        self.countryName = nil
        
    
        self.city.text = ""
        self.country.text = ""
        self.zipCodeFieldView.textField.text = ""
        self.address = ""
        
        var zipcode:String? = nil
        
        address =  place.formattedAddress!.componentsSeparatedByString(",")[0]
        print(address)
        
        print("Place attributions: \(place.attributions)")
        
       let addressComponents = place.addressComponents
        
        for addressComponent in addressComponents! {
            
            print( "\(addressComponent.type)    \(addressComponent.name)")
            
            
            
            
            if addressComponent.type == "locality" {
                
                self.cityName = addressComponent.name
                
            }  else if addressComponent.type == "country" {
                
                self.countryName = addressComponent.name
                
                
            }else if addressComponent.type == "postal_code"{
            
                zipcode = addressComponent.name
    

            }
            
            
        }
        
        
        if zipcode != nil{
        
            print(zipcode)
            print(self.zipCodeFieldView.textField.text)
            self.zipCodeFieldView.textField.text = zipcode!
        }else{
        
            self.zipCodeFieldView.textField.text = ""
        }
        

        
        self.addLocationB.setTitle("  \(place.formattedAddress!)", forState: UIControlState.Normal)
        
        if self.cityName != nil && self.countryName != nil{
        
            self.city.text = "         \(self.cityName!)"
            self.country.text = "         \(self.countryName!)"
            
        }else{
            
            Utilities.showAlertwithTitle("", description: Constant.addressLimitition, parentViewController: self)
        
        }

        
        self.city.textColor    = UIColor.blackColor()
        self.country.textColor = UIColor.blackColor()
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        
        
    }
    
    func wasCancelled(viewController: GMSAutocompleteViewController) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    func didUpdateAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible  = false
    }
    
    func didRequestAutocompletePredictionsForResultsController(resultsController: GMSAutocompleteResultsViewController) {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    @IBAction func addLocation(sender: AnyObject) {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        presentViewController(autocompleteController, animated: true, completion: nil)
        
        
    }
    private func addReturnButtonOnKeyboard(sender:UITextField){
        
        let toolBar = UIToolbar()
        let returnButton = UIBarButtonItem(title: "Return", style: UIBarButtonItemStyle.Plain, target: self, action:  #selector(PaymentRequestVC.resignOnReturn))
        
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        
        toolBar.barStyle = UIBarStyle.Default
        //datePickerToolBar.translucent = true
        toolBar.backgroundColor = UIColor.whiteColor()
        toolBar.tintColor = UIColor(red: 0/255, green: 140/255, blue: 220/255, alpha: 1)
        toolBar.sizeToFit()
        toolBar.setItems([spaceButton,returnButton], animated: false)
        toolBar.userInteractionEnabled = true
        
        
        sender.inputAccessoryView = toolBar
        
    }
    
    func resignOnReturn(){
        
        self.lowestPrizeFieldView.textField.resignFirstResponder()
        self.zipCodeFieldView.textField.resignFirstResponder()
        
        self.view.viewMoveToDown()
    }
    
    
    @IBAction func saveAction(sender: AnyObject) {
        
        self.resignResponder()
        
        if self.firstNameView.textField.text != "" &&  self.aboutMe.text != "" && self.cityName != nil && self.countryName != nil && self.tags.count != 0 && self.address != "" && self.emailFieldview.textField.text != "" && self.doBFieldView.textField.text != "" && self.lowestPrizeFieldView.textField.text != "" && self.zipCodeFieldView.textField.text != "" && self.regionFieldView.textField.text != ""{
            
            self.setExpertiesVariable()
            
            if  let fullNameArray = self.firstNameView.textField.text?.componentsSeparatedByString(" "){
                
                if fullNameArray.count > 1 {
                    
                    
                    
                    if self.user!.isGuide != 1 {
                        
                        self.insertguideCall()
                        
                    }else{
                        
                        self.updateGuideCall()
                    }
                    
                    
                    
                }else{
                    
                      self.firstNameView.setTextFieldActionImage(errorImage)
                      Utilities.makeToastOfString("Please enter full name", onView: self.view)
                }
                
            
            }else{
            
                self.firstNameView.setTextFieldActionImage(errorImage)
            }
            
            
  
        
        }else{
        
            self.checkWhoseFieldIsEmpty()
        }
        
    }
    
    private func insertguideCall(){
        
        var params:[String:AnyObject] = [:]
        var link = ""
        
        var firstName = ""
        var lastName  = ""
        
        if let fullNameArray = self.firstNameView.textField.text?.componentsSeparatedByString(" "){
        
            for i in 0..<fullNameArray.count{
            
                if i == fullNameArray.count - 1{
                
                    lastName += fullNameArray[i]
                }else{
                    
                    if i < fullNameArray.count - 2{
                        
                        firstName += fullNameArray[i] + " "
                    }else {
                        
                        firstName += fullNameArray[i]
                    }
                    
                }
            }
            
            
            params = [Constant.userID:self.user!.iD!,
                      "firstName":firstName,
                      "lastName":lastName,
                      "dob":self.selectedDateOfBirth,
                      "address":self.address,
                      "zip":self.zipCodeFieldView.textField.text!,
                      "region":self.regionFieldView.textField.text!,
                      "locality":self.cityName!,
                      
                      Constant.bio:self.aboutMe.text!,
                      
                      Constant.country:self.countryName!,
                      Constant.expertise:self.experties,
                      Constant.lowestPrice:Int(self.lowestPrizeFieldView.textField.text!)!]
            
            link = "\(Constant.SERVICE_MAIN_LINK)api/users/insertGuide"
            
            Utilities.sharedInstance.disableScreen(self.view)
            
            
            let insertAUpdateGuide = BecomeAUpdateGuideS()
            insertAUpdateGuide.execute(params,link: link, callback: { (errorMessage, message, serviceResponse) in
                
                Utilities.sharedInstance.enableScreeen()
                
                if errorMessage == nil {
                    
                    
                    
                    if let userDic =  AppSingleton.sharedInstance.stringToDic(self.userdefaults.valueForKey("UserSessionJson") as! NSString){
                        
                        var dic = userDic as! [String:AnyObject]
                        
                        params["submerchantID"] = (serviceResponse as! UserGuide).submerchantID
                        params["rating"] = (serviceResponse as! UserGuide).rating
                        params["id"] = (serviceResponse as! UserGuide).iD
                        params["status"] = (serviceResponse as! UserGuide).status
                        params["city"]   = (serviceResponse as! UserGuide).city
                        
                        dic["guideObj"] = params
                        dic["isGuide"] = 1
                        print(dic)
                        
                        if let userSessionString = AppSingleton.sharedInstance.dicToString(dic){
                            
                            print(userSessionString)
                            self.userdefaults.setValue(userSessionString, forKey: "UserSessionJson")
                            
                        }
                        
                    }
                    
                    self.user!.isGuide = 1
                    let userGuide = UserGuide()
                    userGuide.parseJson(params)
                    self.user!.asGuide = userGuide
                    
                    
                    let alertC = UIAlertController(title: "", message: Constant.guideAddText, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in
                        
                        self.navigationController?.popViewControllerAnimated(true)
                        
                        
                        
                    }
                    
                    alertC.addAction(okAction)
                    
                    
                    self.presentViewController(alertC, animated: true, completion: nil)
                    
                    
                    
                }else{
                    
                    Utilities.showAlertwithTitle("Alert", description: errorMessage!, parentViewController: self)
                    
                }
                
            })
            
            
        }
    
    }
    
    
    private func updateGuideCall(){
    
        
        var params:[String:AnyObject] = [:]
        var link = ""
        
        var firstName = ""
        var lastName  = ""
        
        if let fullNameArray = self.firstNameView.textField.text?.componentsSeparatedByString(" "){
            
            for i in 0..<fullNameArray.count{
                
                if i == fullNameArray.count - 1{
                    
                    lastName += fullNameArray[i]
                }else{
                    
                    if i < fullNameArray.count - 2{
                        
                        firstName += fullNameArray[i] + " "
                    }else {
                    
                          firstName += fullNameArray[i]
                    }
                    
                  
                }
            }
            

            
            params = [  Constant.id:self.user!.asGuide!.iD,
                        Constant.userID:self.user!.iD!,
                        "firstName":firstName,
                        "lastName":lastName,
                        "dob":self.selectedDateOfBirth,
                        "address":self.address,
                        "zip":self.zipCodeFieldView.textField.text!,
                        "region":self.regionFieldView.textField.text!,
                        "locality":self.cityName!,
                        
                        Constant.bio:self.aboutMe.text!,
                        
                        Constant.country:self.countryName!,
                        Constant.expertise:self.experties,
                        Constant.lowestPrice:Int(self.lowestPrizeFieldView.textField.text!)!]
            
            link = "\(Constant.SERVICE_MAIN_LINK)/api/users/updateGuide"
            
            print(params)
            
            Utilities.sharedInstance.disableScreen(self.view)
            
            
            let insertAUpdateGuide = BecomeAUpdateGuideS()
            insertAUpdateGuide.execute(params,link: link, callback: { (errorMessage, message, serviceResponse) in
                
                Utilities.sharedInstance.enableScreeen()
                
                if errorMessage == "1" || errorMessage=="5" {
                    
                    if let userDic =  AppSingleton.sharedInstance.stringToDic(self.userdefaults.valueForKey("UserSessionJson") as! NSString){
                        
                        var dic = userDic as! [String:AnyObject]
                        
                        params["submerchantID"] = (serviceResponse as! UserGuide).submerchantID
                        params["rating"] = (serviceResponse as! UserGuide).rating
                        params["id"] = (serviceResponse as! UserGuide).iD
                        params["status"] = (serviceResponse as! UserGuide).status
                        params["city"]   = (serviceResponse as! UserGuide).city
                        
                        dic["guideObj"] = params
                        dic["isGuide"] = 1
                        print(dic)
                        
                        if let userSessionString = AppSingleton.sharedInstance.dicToString(dic){
                            
                            print(userSessionString)
                            self.userdefaults.setValue(userSessionString, forKey: "UserSessionJson")
                            
                        }
                        
                    }
                    
                    self.user!.isGuide = 1
                    let userGuide = UserGuide()
                    userGuide.parseJson(params)
                    self.user!.asGuide = userGuide
                    
                    
                    if(errorMessage=="1"){
                        
                        let alertC = UIAlertController(title: "", message: Constant.guideAddText, preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in
                            
                            self.navigationController?.popViewControllerAnimated(true)
                            
                        }
                        alertC.addAction(okAction)
                        self.presentViewController(alertC, animated: true, completion: nil)
                        
                        
                    }else{
                        
                        Utilities.showAlertwithTitle("Alert", description: message!, parentViewController: self)
                        
                    }
                    
                    
                    
                }else{
                    
                    Utilities.showAlertwithTitle("Alert", description: errorMessage!, parentViewController: self)
                    
                }
                
            })
            

        
        }
            
        
       // "lastName":self.lastNameView.textField.text!,
        }
    
    private func setFieldActionImage(){
    
        self.checkTextFieldASetImage(self.firstNameView)
      //  self.checkTextFieldASetImage(self.lastNameView)
        self.checkTextFieldASetImage(self.emailFieldview)
        self.checkTextFieldASetImage(self.doBFieldView)
        self.checkTextFieldASetImage(self.lowestPrizeFieldView)
        self.checkTextFieldASetImage(self.zipCodeFieldView)
        self.checkTextFieldASetImage(self.regionFieldView)
    }
    
    
    private func setTagsActionImage(){
        
   
        
        
        if self.tags.count == 0{
            
            self.areaExpertiesView.setTextFieldActionImage(errorImage)
            
        }else{
            
            self.areaExpertiesView.setTextFieldActionImage(textBox_Tick)
        }
    
    }
    
    private func checkWhoseFieldIsEmpty(){
        
        
        self.setFieldActionImage()
        self.setTagsActionImage()

        if self.aboutMe.text == ""{
            
                Utilities.showAlertwithTitle("Alert", description: "About me is empty", parentViewController: self)
        
        }else if self.cityName == nil || self.countryName == nil{
            
            Utilities.showAlertwithTitle("Alert", description: Constant.pleaseAddYourLocation, parentViewController: self)
            
        }

    }
    
    private func checkTextFieldASetImage(textFieldV:TextFieldView){
    
        if textFieldV.textField.text == ""{
            
            textFieldV.setTextFieldActionImage(errorImage)
        
        }else{
        
            textFieldV.setTextFieldActionImage(textBox_Tick)
        }
    
    }
    
    private func setExpertiesVariable(){
    
        
        var i = 1
        
        for tag in tags{
        
            if tags.count == i {
                
                self.experties = "\(self.experties)" + "\(tag)"

            
            }else{
            
                self.experties = "\(self.experties)" + "\(tag),"

            }
            
            i = i + 1
        
        }
    
    }
    
    func goToProfile() {
        
        self.navigationController?.popViewControllerAnimated(true)
    }

    
}
