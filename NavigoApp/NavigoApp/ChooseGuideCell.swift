//
//  ChooseGuideCell.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 13/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import STRatingControl

class ChooseGuideCell: UITableViewCell {

 //   @IBOutlet weak var bio: UITextView!
    @IBOutlet weak var ratingView: STRatingControl!
    @IBOutlet weak var imageViewG: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var cityAndCountry: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imageViewG.layer.cornerRadius = 35
        self.imageViewG.clipsToBounds = true
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
