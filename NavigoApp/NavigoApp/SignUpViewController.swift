//
//  SignUpViewController.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/11/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UIPopoverPresentationControllerDelegate,getOkActionOfEmailSendDeleget {


    @IBOutlet weak var nameFieldView: TextFieldView!
    @IBOutlet weak var emailFieldView: TextFieldView!
    @IBOutlet weak var phoneFieldView: TextFieldView!
    @IBOutlet weak var passFieldView: TextFieldView!
    @IBOutlet weak var uploadPicImgV: UIImageView!
  //  @IBOutlet weak var uploadPicture: UIButton!
    @IBOutlet weak var cameraBlurView: UIView!
    @IBOutlet weak var createButton: CustomButton!
    
    var selectedImageInBase64:String? = nil
    
    var isCheck = false
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setInitialValueSet()
        initialValuesForRepresentation()
      
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.changeImage))
        self.uploadPicImgV.userInteractionEnabled = true
        self.uploadPicImgV.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        
      
        
    }

    override func viewDidAppear(animated: Bool) {
        
     
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {

        self.resignResponders()

    }
    
    
    private func resignResponders(){
    
    
        self.nameFieldView.textField.resignFirstResponder()
        self.emailFieldView.textField.resignFirstResponder()
        self.phoneFieldView.textField.resignFirstResponder()
        self.passFieldView.textField.resignFirstResponder()
        
        self.view.viewMoveToDown()
    }
    
    private func initialValuesForRepresentation(){
        
          self.uploadPicImgV.layer.cornerRadius = self.uploadPicImgV.bounds.size.width/2
          self.cameraBlurView.layer.cornerRadius = self.cameraBlurView.bounds.size.width/2
          self.setFieldsXibsViews()
          self.createButton.setBacColor(ColorUtilities.buttonBackGroungBlueColor)
    }
    
    private func setFieldsXibsViews(){
    
        self.setNameViewXib()
        self.setEmailViewXib()
        self.setPhoneNumXibView()
        self.setPassXibView()

    }
    
    private func setNameViewXib(){
        
        self.nameFieldView.setTextFieldActionImage(nil)
        self.nameFieldView.setTextFieldIconImage(ImageUtilities.ic_user!)
        self.nameFieldView.setPlaceHolderOFField("Name")
        self.nameFieldView.textField.delegate = self
        self.nameFieldView.textField.autocapitalizationType = .Words
        
    }
    private func setEmailViewXib(){
        
        self.emailFieldView.setTextFieldActionImage(nil)
        self.emailFieldView.setTextFieldIconImage(ImageUtilities.ic_email!)
        self.emailFieldView.textField.keyboardType = UIKeyboardType.EmailAddress
        self.emailFieldView.setPlaceHolderOFField("Email")
        self.emailFieldView.textField.delegate = self

        

        
    }
    private func setPhoneNumXibView(){
        
        self.phoneFieldView.setTextFieldActionImage(nil)
        self.phoneFieldView.setTextFieldIconImage(ImageUtilities.ic_phone!)
        self.phoneFieldView.textField.keyboardType = UIKeyboardType.PhonePad
        self.phoneFieldView.setPlaceHolderOFField("Phone")
         self.phoneFieldView.textField.delegate = self
        
        
    }
    private func setPassXibView(){
        
        self.passFieldView.setTextFieldActionImage(nil)
        self.passFieldView.setTextFieldIconImage(ImageUtilities.ic_password!)
        self.passFieldView.textField.secureTextEntry = true
        self.passFieldView.setPlaceHolderOFField("Password")
        self.passFieldView.textField.delegate = self
        
    }
    
    private func setInitialValueSet(){
    
        self.nameFieldView.textField.delegate       = self
        self.emailFieldView.textField.delegate      = self
        self.phoneFieldView.textField.delegate      = self
        self.passFieldView.textField.delegate       = self
        
        AppSingleton.sharedInstance.okBtnClickOnEmailPopUp = self
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        self.view.viewMoveToUp()
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
    
         self.uploadPicImgV.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.uploadPicImgV.contentMode   = .ScaleAspectFill
        self.uploadPicImgV.clipsToBounds = true
        self.view.bringSubviewToFront(self.uploadPicImgV)
        
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        if let image = image{
        
            if  let compressImageData  =  image.jpeg(.lowest){
            
                self.selectedImageInBase64 = compressImageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                
//                self.selectedImageInBase64 =  "\(compressImageData.base //(NSDataBase64EncodingOptions.Encoding64CharacterLineLength))"
                print(selectedImageInBase64!)
            
            }else{
                Utilities.makeToastOfString(Constant.SOME_THING_WENT_WRONG, onView: self.view)

            
            }
            
           
       
            
            //    imageData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        }else{
        
            Utilities.makeToastOfString(Constant.SOME_THING_WENT_WRONG, onView: self.view)
        }
     
 
        

       
        
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            
            return true
        }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        
        if textField == self.nameFieldView.textField{
            
            return newLength <= 25
            
        }else if textField == self.emailFieldView.textField{
            
            return newLength <= 50
            
        }else if textField == self.phoneFieldView.textField{
        
        
            return newLength <= 14
        }else{
        
            return newLength <= 35
        }
        
                
    }

    @IBAction func backAction(sender: AnyObject) {
        
        let stb = UIStoryboard(name: "Main", bundle: nil)
        let vc = stb.instantiateViewControllerWithIdentifier("login") as! SignViewController
        presentViewController(vc, animated: true, completion: nil)    }
   
     func changeImage() {
        
        self.presentUploadPictureActionSheet()
    }
    private func presentUploadPictureActionSheet() {
        
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        alertController.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { (action) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                
                let imagePicker                    = UIImagePickerController()
                imagePicker.sourceType             = .Camera
                imagePicker.cameraCaptureMode      = .Photo
                imagePicker.modalPresentationStyle = .FullScreen
                imagePicker.allowsEditing          = false
                imagePicker.delegate               = self
                
                self.presentViewController(imagePicker, animated: true, completion: nil)
                
            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Gallery", style: .Default, handler: { (action) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary) {
                
                let imagePicker             = UIImagePickerController()
                imagePicker.sourceType      = .PhotoLibrary
                imagePicker.allowsEditing   = false
            
                imagePicker.delegate        = self
                
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action) -> Void in
            
            
        }))
        
        
        self.presentViewController(alertController, animated: true) { () -> Void in
            
        }
        
    }
    private func setActionImageOnAllTextFields(image:UIImage){
        
           self.nameFieldView.setTextFieldActionImage(image)
           self.emailFieldView.setTextFieldActionImage(image)
           self.passFieldView.setTextFieldActionImage(image)
          self.phoneFieldView.setTextFieldActionImage(image)
    }

    @IBAction func createAction(sender: AnyObject) {
        
        self.resignResponders()
        
        if self.nameFieldView.textField.text != "" && self.phoneFieldView.textField.text != "" && self.passFieldView.textField.text?.characters.count >= Constant.passwordLimitition && self.emailFieldView.textField.text != "" && self.selectedImageInBase64 != nil && self.isCheck != false && Utilities.isValidEmail(self.emailFieldView.textField.text!) == true{
            
   
            
                self.setActionImageOnAllTextFields(ImageUtilities.textbox_tick!)
            
                Utilities.sharedInstance.disableScreen(self.view)
                
                let params:[String:AnyObject] = [Constant.email:emailFieldView.textField.text!,
                                                 Constant.phone:phoneFieldView.textField.text!,
                                                 Constant.password:passFieldView.textField.text!,
                                                 Constant.name:nameFieldView.textField.text!,
                                                 Constant.profileImage:self.selectedImageInBase64!]
                
                
                let registerService = RegisterService()
                registerService.execute(params, callback: { (errorMessage, message, serviceResponse) in
                    
                    Utilities.sharedInstance.enableScreeen()
                    
                    if errorMessage == nil{
                        
                        self.performSegueWithIdentifier("toCheckEmail", sender: self)
                        
                    }else{
                        
                        Utilities.makeToastOfString("\(errorMessage!)", onView: self.view)
                    }

                    
                })

        }else{
            
          
            self.checkWhoseFieldIsEmpty()
            
        }
    
    }
    
    private func checkWhoseFieldIsEmpty(){
        
        if self.nameFieldView.textField.text == ""{
            
            self.nameFieldView.setTextFieldActionImage(ImageUtilities.warning)
            
        }else{
        
             self.nameFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
        }
        
        if Utilities.isValidEmail(self.emailFieldView.textField.text!) == false{
            
            self.emailFieldView.setTextFieldActionImage(ImageUtilities.warning)
            
        }else{
            
            self.emailFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
        }
        
        if self.passFieldView.textField.text?.characters.count < Constant.passwordLimitition{
            
            self.passFieldView.setTextFieldActionImage(ImageUtilities.warning)
            
        }else{
            
            self.passFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
        }
        if self.phoneFieldView.textField.text == ""{
            
            self.phoneFieldView.setTextFieldActionImage(ImageUtilities.warning)
            
        }else{
            
            self.phoneFieldView.setTextFieldActionImage(ImageUtilities.textbox_tick)
        }
        
        if self.selectedImageInBase64 == nil{
            
            Utilities.makeToastOfString("Image not seleted", onView: self.view)
            
        }else if passFieldView.textField.text?.characters.count < Constant.passwordLimitition{
            
            Utilities.makeToastOfString(Constant.paswiordValidationText, onView: self.view)
            
        }else if isCheck == false{
            
            Utilities.makeToastOfString(Constant.pleaseAcceptTerm, onView: self.view)
            
        }
    
    }
    

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toPinValidation"{
        
            var vc = segue.destinationViewController as! PinValidationAfterSignUpOrSignIn
                vc.email = self.emailFieldView.textField.text
            
            
        }else    if segue.identifier == "toCheckEmail"{
            
            
            let  filterController = segue.destinationViewController as! EmailSendPopUpVC
            filterController.preferredContentSize = CGSize(width: 280 , height: 240)
            let controller = filterController.popoverPresentationController
            
            if controller != nil{
                
                controller?.delegate = self
                controller?.sourceView = self.view
                controller?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                controller?.sourceRect = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds),0,0)
            }
            
            
            
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        

    }
    
    func popoverPresentationControllerShouldDismissPopover(popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    
    func okBtnClick() {
        
        self.performSegueWithIdentifier("toPinValidation", sender: self)

    }
    
    @IBAction func checkBoxAction(sender: UIButton) {
        
        let checked = UIImage(named: "checked")
        let uncheck = UIImage(named: "Unchecked")
        
        if isCheck{
            
            sender.setImage(uncheck, forState: UIControlState.Normal)
            self.isCheck = false
            
        }else{
        
            sender.setImage(checked, forState: UIControlState.Normal)
            self.isCheck = true
        }
        
    }
    
    

}


extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in PNG format
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the PNG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    var png: NSData? { return UIImagePNGRepresentation(self) }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(quality: JPEGQuality) -> NSData? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}