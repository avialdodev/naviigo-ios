//
//  RatingViewController.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 18/03/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit
import STRatingControl

class RatingViewController: UIViewController,STRatingControlDelegate,UITextViewDelegate {

    @IBOutlet weak var descriptionView: UITextView!
    
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var doneButton: CustomButton!
    var trip:Trip!
    var user:LoginUser!
    
    var selectedRating = 0
    
    var previousStars:Int!
    var previousFeedback:String!
  
    @IBOutlet weak var ratingView: STRatingControl!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        print(self.navigationController)
        self.user = AppSingleton.sharedInstance.loginUserObj!
        
        self.descriptionView.layer.borderWidth = 1
        self.descriptionView.layer.borderColor = ColorUtilities.borderColor.CGColor
        self.descriptionView.layer.cornerRadius = 5
        self.descriptionView.delegate = self
        
        
        ratingView.sizeToFit()
        
        self.ratingView.delegate = self
        
        
        
         //ratingView.spacing = 0
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if self.previousFeedback != nil && self.previousStars != nil{
        
            print(self.previousStars)
            self.ratingView.rating = self.previousStars
            self.descriptionView.text = self.previousFeedback
            
            self.ratingView.userInteractionEnabled = false
            self.descriptionView.userInteractionEnabled = false
            
            self.crossButton.hidden = true
            self.doneButton.setTitle("Cancel", forState: UIControlState.Normal)
            self.doneButton.backgroundColor = ColorUtilities.redColor
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.descriptionView.resignFirstResponder()
        self.view.viewMoveToDown()
    }
    
//    func textViewDidBeginEditing(textView: UITextView) {
//        
//        self.view.viewMoveToUp()
//    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
 
        
        let newText = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        return numberOfChars <= 100
        
        
    }
    
    
    func didSelectRating(control: STRatingControl, rating: Int) {
        
        self.selectedRating = rating
        print(rating)
    }
    @IBAction func crossAction(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func doneAction(sender: AnyObject) {
   
        if self.previousFeedback != nil && self.previousStars != nil{
        
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }else{
            
            leaveFeedback()
        
        }
        
    }
    
    
    private func leaveFeedback(){
        
        if self.descriptionView.text != ""{
            
            Utilities.sharedInstance.disableScreen(self.view)
            
            let params:[String:AnyObject] = [  "tripID": self.trip.id,
                                               "point": self.selectedRating,
                                               "feedback": self.descriptionView.text!,
                                               ]
            
            print(params)
            
            let leaveFeedbackS = LeaveFeedbackService()
            leaveFeedbackS.execute(params) { (errorMessage, message, serviceResponse) in
                
                
                Utilities.sharedInstance.enableScreeen()
                
                if errorMessage == nil{
                    
                    self.trip.status = 3
                    
                    
                    
                    self.dismissViewControllerAnimated(true, completion: nil)
                    
                    AppSingleton.sharedInstance.isComingFromFDeleget?.isComingFromFeedback(true)
                
                }else{
                
                    Utilities.showAlertwithTitle("Alert", description: errorMessage!, parentViewController: self)
                }
                
            }

        
        }else{
        
        
            Utilities.showAlertwithTitle("Alert", description: Constant.someFieldsAreEmpty, parentViewController: self)
        
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
