//
//  ResetPassAndValidationViewController.swift
//  NavigoApp
//
//  Created by Salman Raza on 1/16/17.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import UIKit

class ResetPassAndValidationViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var pinCodeView: TextFieldView!
    @IBOutlet weak var newPassView: TextFieldView!
    @IBOutlet weak var confirmPassView: TextFieldView!
    @IBOutlet weak var DoneButton: CustomButton!
    
    var serviceAction = false
    
    var email:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setInitialViewRepresentation()
        self.setInitialValues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.resignResponders()
    }
    
    private func resignResponders(){
    
        self.pinCodeView.textField.resignFirstResponder()
        self.newPassView.textField.resignFirstResponder()
        self.confirmPassView.textField.resignFirstResponder()
        
        self.view.viewMoveToDown()
    }
    
    private func setInitialValues(){
    
        self.newPassView.textField.delegate     = self
        self.confirmPassView.textField.delegate = self
    
    }
    
    private func setInitialViewRepresentation(){
    
        self.setXibsViews()
        self.DoneButton.setBacColor(ColorUtilities.buttonBackGroungBlueColor)
        
        self.newPassView.alpha     = 0
        self.confirmPassView.alpha = 0
        
        // for animation
        self.DoneButton.transform  = CGAffineTransformMakeTranslation(0, -180)
        self.pinCodeView.transform = CGAffineTransformMakeTranslation(0, -50)
        
    }
    
    private func setXibsViews(){
    
        self.setPinCodeXib()
        self.setNewPassXib()
        self.setConfirmPassXib()

    }
    
    private func setPinCodeXib(){
    
        self.pinCodeView.setTextFieldIconImage(ImageUtilities.ic_gender!)
        self.pinCodeView.setPlaceHolderOFField("Enter Pin Code")
        self.pinCodeView.setTextFieldActionImage(nil)
        self.pinCodeView.textField.delegate = self
    
    }
    
    private func setNewPassXib(){
        
        self.newPassView.setTextFieldIconImage(ImageUtilities.ic_password!)
        self.newPassView.textField.secureTextEntry = true
        self.newPassView.setPlaceHolderOFField("New Password")
        self.newPassView.setTextFieldActionImage(nil)
        self.newPassView.textField.delegate = self
        
    }
    private func setConfirmPassXib(){
        
        self.confirmPassView.setTextFieldIconImage(ImageUtilities.ic_password!)
        self.confirmPassView.setPlaceHolderOFField("Confirm Password")
        self.confirmPassView.textField.secureTextEntry = true
        self.confirmPassView.setTextFieldActionImage(nil)
        self.confirmPassView.textField.delegate = self
    
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == self.newPassView.textField || textField == self.confirmPassView.textField{
        
            self.view.viewMoveToUp()
        }
    }
    
    private func doAnomation(){
        
        UIView.animateWithDuration(3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .TransitionNone, animations: { () -> Void in
            
            self.newPassView.alpha     = 1
            self.confirmPassView.alpha = 1
            
            self.pinCodeView.transform = CGAffineTransformMakeTranslation(0, 0)
            self.DoneButton.transform = CGAffineTransformMakeTranslation(0,0)
            
            self.DoneButton.setTitle("Done", forState: UIControlState.Normal)
            
            }, completion: nil)
        //
    
    
    }
    
    @IBAction func backAction(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func ConfirmOrDoneAction(sender: AnyObject) {
        
        self.resignResponders()
        
        if serviceAction {
            
            if pinCodeView.textField.text != "" && self.newPassView.textField.text != "" &&
                self.confirmPassView.textField.text != ""{
                
                    
                    
                    if self.newPassView.textField.text == self.confirmPassView.textField.text{
                        
                        if self.newPassView.textField.text?.characters.count >= Constant.passwordLimitition {
                        
                            self.newPassView.setTextFieldActionImage(ImageUtilities.textbox_tick)
                            self.confirmPassView.setTextFieldActionImage(ImageUtilities.textbox_tick)
                            
                            
                            Utilities.sharedInstance.disableScreen(self.view)
                            
                            
                            let params:[String:AnyObject] = [
                                Constant.email:self.email!,
                                Constant.token:self.pinCodeView.textField.text!,
                                Constant.password:self.newPassView.textField.text!
                            ]
                            
                            let pinValidationService = ForgetPinService()
                            
                            pinValidationService.execute(params, callback: { (errorMessage, message, serviceResponse) -> Void in
                                
                                Utilities.sharedInstance.enableScreeen()
                                
                                if errorMessage == nil{
                                    
                                    let stb = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = stb.instantiateViewControllerWithIdentifier("login") as! SignViewController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                }else{
                                    
                                    Utilities.makeToastOfString("\(errorMessage!)", onView: self.view)
                                    
                                }
                                
                                
                            })
                            

                        
                        }else{
                        
                             self.newPassView.setTextFieldActionImage(ImageUtilities.warning)
                            self.confirmPassView.setTextFieldActionImage(ImageUtilities.warning)
                        
                        }
                        
                        
                        
                        
                    }else{
                           self.newPassView.setTextFieldActionImage(ImageUtilities.warning)
                          self.confirmPassView.setTextFieldActionImage(ImageUtilities.warning)

                    }
                    
            
            }else{
            
                if pinCodeView.textField.text == ""{
                
                    self.pinCodeView.setTextFieldActionImage(ImageUtilities.warning)
                }
                
                if self.newPassView.textField.text == ""{
                
                     self.newPassView.setTextFieldActionImage(ImageUtilities.warning)
                }
                
                if self.confirmPassView.textField.text == ""{
                
                     self.confirmPassView.setTextFieldActionImage(ImageUtilities.warning)
                }
                
            }
        
        }else{
            self.doAnomation()
            self.serviceAction = true
        
        }

    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            
            return true
        }
        
        let newLength = text.characters.count + string.characters.count - range.length
        
        
        if textField == self.pinCodeView.textField{
            
            return newLength <= 10
            
        }else {
            
            return newLength <= 35
            
        }

        
    }

}
