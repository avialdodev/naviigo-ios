//
//  Response.swift
//  NavigoApp
//
//  Created by Wahab Jawed on 21/01/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation


class Response:BaseEntity {
    
    var developerMessage:String!
    var status:Int!
    override func parseJson(dic: NSDictionary) {
        

        
      self.developerMessage =   (dic.valueForKey(Constant.developerMessage) as? String) ?? ""
      self.status           =  dic.valueForKey(Constant.status) as! Int
        
        
    }

}