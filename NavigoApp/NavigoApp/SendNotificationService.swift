//
//  SendNotificationService.swift
//  NavigoApp
//
//  Created by Wahab Jawed on 19/08/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire

class SendNotificationService{
    
    func execute(params: NSDictionary) {
        
        Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)api/users/sendNotification", parameters: params as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
            
            print("response\(response)")
            
        }
        
        
    }
    
}
