//
//  GetGuideService.swift
//  NavigoApp
//
//  Created by Abdul Wahab MAC 2 on 25/02/2017.
//  Copyright © 2017 Salman Raza. All rights reserved.
//

import Foundation
import Alamofire


class GetGuideService{
    
    
    var guides:[Guide] = []
    
    
    func execute(params: NSDictionary, callback: (errorMessage: String?, message: String?, serviceResponse: [Guide]?) -> Void) {
        
     Alamofire.request(.POST, "\(Constant.SERVICE_MAIN_LINK)api/guide/search", parameters: params as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON { (response) -> Void in
        
            if response.result.isSuccess{
                
                
                if let responseResult = response.result.value as? NSDictionary{
                    
                    let guideResponseArray = responseResult.valueForKey("guideList") as! NSArray
                    
                    for guideResponse in guideResponseArray{
                    
                        let guide = Guide()
                            guide.parseJson(guideResponse as! NSDictionary)
                            print(guide.rating)
                        
                        self.guides.append(guide)
                    }
                
                    callback(errorMessage: nil, message: nil, serviceResponse: self.guides)
                
                }else{
                
                    callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)

                }
            
            }else{
            
                callback(errorMessage: Constant.SOME_THING_WENT_WRONG, message: nil, serviceResponse: nil)
                
            }
            
        
        
        }
    }
}